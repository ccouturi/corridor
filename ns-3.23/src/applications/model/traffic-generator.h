/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Telecom Bretagne
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Couturier <christophe.couturier@telecom-bretagne.eu>
 */
 

#ifndef TRAFFIC_GENERATOR_H
#define TRAFFIC_GENERATOR_H

#include "ns3/application.h"
#include "ns3/socket.h"
#include <ns3/data-rate.h>
#include <ns3/packet-sink.h>
#include <ns3/output-stream-wrapper.h>
#include "ns3/traced-value.h"

#include "ns3/attribute.h"
#include "ns3/sequence-number.h"



//#include "ns3/applications-module.h"

namespace ns3 {
/**
 * \ingroup applications
 * \defgroup trafficgenerator TrafficGenerator
 */

/**
 * \ingroup trafficgenerator
 * \class TrafficGenerator
 * \brief An easy to use UDP or TCP traffic generator for IPv4 and IPv6.
 *
 * Specify Tx and Rx nodes, destination address and port and protocol 
 * type (UDP or TCP).
 * The class provides facilities to periodically record traffic stats in 
 * a file in order to make goodput and delay plots.
 */ 
 
class TrafficGenerator : public Application 
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  TypeId GetInstanceTypeId (void) const;
  TrafficGenerator ();
  virtual ~TrafficGenerator();

  /**
   * \brief Initialise an IPv4 socket on the source node and a sink on
   * the destination node. And configure them according to the 
   * parameters.
   * \param protocol The protocol used to establish the communication.
   * This string identifies the socket factory type used to create 
   * sockets for the applications. Typical values would be 
   * "ns3::TcpSocketFactory" or "ns3::UdpSocketFactory"
   * \param srcNode Pointer to the node to attach socket source to.
   * \param destNode Pointer to the node to attach destination sink to.
   * \param destAddress IPv4 address of destination node.
   * \param port Destination port number of UDP or TCP traffic.
   * \param packetSize Size of application packets to be generated. The 
   * period of generation is defined according to packetSize and 
   * dataRate parameters.
   * \param dataRate Rate of data production by the Tx application.
   */
  void Setup (std::string protocol, 
              Ptr<Node> srcNode, Ptr<Node> destNode,
              Ipv4Address destAddress, uint16_t port,
              uint32_t packetSize, DataRate dataRate);
void SetupBulk (std::string protocol, 
            Ptr<Node> srcNode, Ptr<Node> destNode,
            Ipv4Address destAddress, uint16_t port,
            uint32_t packetSize, uint32_t dataSize);              


//~ void Setup (std::string protocol, 
            //~ Ptr<Node> srcNode, Ptr<Node> destNode,
            //~ Address destAddress, uint16_t port,
            //~ uint32_t packetSize, DataRate dataRate);
//~ void SetupBulk (std::string protocol, 
            //~ Ptr<Node> srcNode, Ptr<Node> destNode,
            //~ Address destAddress, uint16_t port,
            //~ uint32_t packetSize, uint32_t dataSize);              
              
  /**
   * \brief Initialise an IPv6 socket on the source node and a sink on
   * the destination node. And configure them according to the 
   * parameters.
   * \param protocol The protocol used to establish the communication.
   * This string identifies the socket factory type used to create 
   * sockets for the applications. Typical values would be 
   * "ns3::TcpSocketFactory" or "ns3::UdpSocketFactory"
   * \param srcNode Pointer to the node to attach socket source to.
   * \param destNode Pointer to the node to attach destination sink to.
   * \param destAddress IPv6 address of destination node.
   * \param port Destination port number of UDP or TCP traffic.
   * \param packetSize Size of application packets to be generated. The 
   * period of generation is defined according to packetSize and 
   * dataRate parameters.
   * \param dataRate Rate of data production by the Tx application.
   */
  void Setup (std::string protocol, 
              Ptr<Node> srcNode, Ptr<Node> destNode,
              Ipv6Address destAddress, uint16_t port,
              uint32_t packetSize, DataRate dataRate);
void SetupBulk (std::string protocol, 
            Ptr<Node> srcNode, Ptr<Node> destNode,
            Ipv6Address destAddress, uint16_t port,
            uint32_t packetSize, uint32_t dataSize);              
              
 
  /**
   * \brief Enable to periodically record in a file information about 
   * recived traffic (goodput and delay).
   * The file has the following structure:
   *    - 1st column: time (in s)
   *    - 2nd column: average throughput on the 100ms period of time (in Mb/s)
   *    - 3rd column: nb Bytes received during last 100 period of time
   *    - 4th column: nb packets received during last 100 period of time
   *    - 5th column: minimum packet delay measured during last 100 period of time (in s)
   *    - 6th column: maximum packet delay measured during last 100 period of time (in s)
   *    - 7th column: average packet delay during last 100 period of time (in s)
   * \param filePath Full path to the record file.
   * \param samplePeriod Period of records into the file.
   * \return true if the file was successfully created; false otherwise.
   */  
  bool RecordTraffic (std::string filePath, Time samplePeriod);

  /**
   * \brief Return a pointer to the listening soket of the sink App
   * \return Pointer to the listening socket.
   */  
  Ptr<Socket> GetSinkListeningSocket (void);
  /**
   * \brief Return a pointer to the the socket used to send data to the 
   * nbth connected node.
   * \return Pointer to the socket.
   */  
  Ptr<Socket> GetSinkConnectedSocket (uint16_t nb);
  /**
   * \brief Return the number of sockets oppened to connect with remote 
   * hosts.
   * \return Number of connected hosts.
   */  
  uint16_t GetNbConnectionsToSink ();
    

  /**
   * \brief Helper to record contention window change in a file using a
   * simple one line call.
   * Can be used in the the case of TCP protocol.
   * \param filePath Full path to the record file.
   */  
  void TraceCwnd (std::string filePath);
  /**
  * \brief Helper to record SlowStart Threshold change in a file using a
  * simple one line call.
  * Can only be used in the the case of TCP protocol supporting
  * SSThreshold. tracing
  * \param filePath Full path to the record file.
  */  
  void TraceSSThreshold (std::string filePath);
  /**
   * \brief Helper to record round trip time (RTT) in a file using a
   * simple one line call.
   * Can be used in the the case of TCP protocol.
   * \param filePath Full path to the record file.
   */  
  void TraceRtt (std::string filePath);
  /**
   * \brief Helper to record retransmit timeout (RTO) in a file using a
   * simple one line call.
   * Can be used in the the case of TCP protocol.
   * \param filePath Full path to the record file.
   */  
  void TraceRto (std::string filePath);
  /**
   * \brief Helper to record advertised window (RWND) in a file using a
   * simple one line call.
   * Can be used in the the case of TCP protocol.
   * \param filePath Full path to the record file.
   */  
  void TraceRwnd (std::string filePath);

void TraceTxSequence (std::string filePath);
void TraceHighestTxSequence (std::string filePath);
//~ void TraceRxSequence (std::string filePath);
void TraceActualWnd (std::string filePath);
void TraceInFlight (std::string filePath);

  /**
   * \brief Helper to record the Rx message number in a file using a
   * simple one line call.
   * \param filePath Full path to the record file.
   */  
  void TraceRxMsg (std::string filePath);


private:
  virtual void StartApplication (void);
  virtual void StopApplication (void);
  
  virtual void ScheduleTx (void);
  virtual void SendPacket (void);

  virtual void WriteRecordInFile (Time samplePeriod);
  virtual void RxPacketSinkCallback (Ptr<const Packet> p, const Address& add);

  virtual void CwndChangeCallback (uint32_t oldCwnd, uint32_t newCwnd);
  virtual void SSThresholdChangeCallback (uint32_t oldSst, uint32_t newSst);
  virtual void RttChangeCallback (Time oldRtt, Time newRtt);
  virtual void RtoChangeCallback (Time oldRtt, Time newRtt);
  virtual void RwndChangeCallback (uint32_t oldRwnd, uint32_t newRwnd);
virtual void NextTxSeqCallback (SequenceNumber32 oldSeq, SequenceNumber32 newSeq);
virtual void HighestTxSeqCallback (SequenceNumber32 oldSeq, SequenceNumber32 newSeq);
//~ virtual void NextRxSeqCallback (SequenceNumber32 oldSeq, SequenceNumber32 newSeq);
virtual void ActualWndChangeCallback (uint32_t oldWnd, uint32_t newWnd);  
virtual void ActualInFlightCallback (uint32_t oldVal, uint32_t newVal);  

  virtual void RxMsgChangeCallback (uint32_t oldNb, uint32_t newNb);
 
  void CommonIpv4Ipv6Setup (Ptr<Node> srcNode, uint32_t packetSize, DataRate dataRate);

  
  Ptr<Socket>       m_socket;         // The Tx socket on the sender side
  Ptr<PacketSink>   m_sink;           // The sink on Rx side
  Address           m_destAddress;    // Couple of IPaddress and port for Socket setup
  uint32_t          m_packetSize;     // Size of SDU to be generated by the Tx application 
  DataRate          m_dataRate;       // Rate of data production by the application
  uint32_t          m_bulkSize;       // Size of the data to send (in bulk mode only)
  uint32_t          m_bulkBytesSent;  // Nb bytes od data sent so far in bulk mode
  EventId           m_sendEvent;
  bool              m_running;
  
  uint32_t          m_txPacketCounter;// Number of packets sent
  TracedValue<uint32_t> m_lasRxPacket;// Number of the last packet received
  uint8_t*          m_currentMsg;     // Buffer to store Rx Data in order to decode the message number
  uint32_t          m_currentMsgPos;  // Position of filling of the Rx data buffer 
  
  uint32_t          m_rxPacketsLast;  // Number of Rx packets since last write in the record file 
  uint32_t          m_rxBytesLast;    // Number of Rx bytes since last write in the record file 
  Time              m_rxDelayTotal;   // Cumulative delay of Rx packets since last write in the record file 
  Time              m_rxDelayMin;     // Delay of quickest received packet since last write in the record file 
  Time              m_rxDelayMax;     // Delay of slowest received packet since last write in the record file 
  Ptr<OutputStreamWrapper> m_recordFile;  // Stream where to record gooput and delay stats
  Ptr<OutputStreamWrapper> m_cwndStream;  // Stream where to record contention window values
  Ptr<OutputStreamWrapper> m_ssThresholdStream;  // Stream where to record Slow Start Threshold values
  Ptr<OutputStreamWrapper> m_rttStream;   // Stream where to record RTT values
  Ptr<OutputStreamWrapper> m_rtoStream;   // Stream where to record RTO values
  Ptr<OutputStreamWrapper> m_rwndStream;  // Stream where to record RWND values
Ptr<OutputStreamWrapper> m_txSeqStream; 
Ptr<OutputStreamWrapper> m_txHighestSeqStream;
//~ Ptr<OutputStreamWrapper> m_rxSeqStream; 
Ptr<OutputStreamWrapper> m_actualWndStream; 
Ptr<OutputStreamWrapper> m_inFlightStream; 
  Ptr<OutputStreamWrapper> m_rxNbStream;  // Stream where to record Rx message number values
  
  enum ipTypes {ipv4 = 1, ipv6 = 2, ipUnset = 0};
  ipTypes           m_iptype;         // version of IP (v4 or v6)
  
  enum trafficMode {cbr = 1, bulk = 2, modeUnset = 0};
  trafficMode       m_mode;
  void DataSentCallback (Ptr<Socket>, uint32_t);
  void SendBulkData (void);
};


} // namespace ns3

#endif /* TRAFFIC_GENERATOR_H */
