
#ifndef LTE_COGNITIVE_MANAGER_H
#define LTE_COGNITIVE_MANAGER_H

#include "ns3/core-module.h"
#include "ns3/object.h"
//#include "ns3/ptr.h"
//#include "ns3/event-id.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
//#include "ns3/config-store-module.h"

//#include <ns3/radio-environment-map-helper.h>

//#include <iomanip>

#include <stdio.h> // Modif RP : to read trace files

#include "ns3/lte-module.h"
#include "ns3/radio-bearer-stats-calculator.h"

namespace ns3{

class Socket;
class Address;

class CognitiveManager : public Application 
{
public:
  static TypeId GetTypeId(void);
  CognitiveManager ();
  virtual ~CognitiveManager();

//  void Setup (Ptr<MobilityModel> mrPosition, Ptr<Socket> socket, Address address, bool second_trip_case, float futureExtent_seconds, FILE * pFile, Ptr<LteHelper> lteHelper, Time databasePeriod, FILE * pMacFile);
  void Setup (Ptr<MobilityModel> mrPosition, Ptr<Socket> socket, Address address, bool second_trip_case, float futureExtent_seconds, char* dataBasePath, Ptr<LteHelper> lteHelper, Time databasePeriod, char* macDataBasePath);
  void ReceivePacket (Ptr<Socket> socket);
  void test(Ptr<Socket> socket);
  void HandleRead (Ptr<Socket> socket);
  void HandleAccept (Ptr<Socket> s, const Address& from);
  void WarnTCP (float current_rsrq, float future_rsrq, float future_rsrq_best_neighbour);

  
private:
  virtual void StartApplication (void); // Called at time specified by Start
  virtual void StopApplication (void); // Called at time specified by Stop
  unsigned long GetIndexOfposition (float pos, unsigned long baseIndex);
  unsigned long GetMacIndexOfposition (float pos, unsigned long baseIndex);
  
  void CheckFutureLink(void);
  void ScheduleCheck(void);
  
  struct ueMeasurements
  {
    float time;
    float posX;
    float speedX;
    int rnti;
    int cID;
    float rsrp;
    float rsrq;
    int servCellBool;    
  };

  struct ueMacMeasurements
  {
    float time;
    float posX;
    float speedX;
    int frameNum;
    int subFrameNum;
    int rnti;
    int mcs1;
    int sizeTb1;
    int mcs2;
    int sizeTb2;
  };



  Ptr<MobilityModel> m_mrPosition;
  Ptr<Socket>       m_server_socket;
  Ptr<Socket>       m_accepted_socket;
  Address           m_bind_address;
  uint32_t          m_nbrPackets;
  bool              m_second_trip_case;
  ueMeasurements *  m_ueDatabase;
  float   m_futureExtent_seconds;
  unsigned long     m_currentTabIndex;
  unsigned long     m_nbr_of_measures;
  Time    m_databasePeriod;

  ueMacMeasurements *  m_ueMacDatabase;
  unsigned long     m_nbMacMeasures;
  int               m_currentMinMcs;
  int               m_currentMaxMcs;
  unsigned long     m_currentMacWndStart;
};

} // namespace

#endif
