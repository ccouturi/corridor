#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
//#include "ns3/config-store-module.h"

/*#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/socket.h"
#include "ns3/address.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"*/

#include <math.h>

#include "cognitive-manager.h"

#include <iomanip>

#include <stdio.h> // Modif RP : to read trace files

#include "ns3/lte-module.h"
#include "ns3/radio-bearer-stats-calculator.h"


#define DELTA_RSRQ_TUNNEL   30  // Hard coded for the moment. Should ask this information to the MIIS
#define ENB_HANDOVER_THRESHOLD  -12.5   // Hard coded for the moment. Should ask this information to the MIIS

namespace ns3{
  
NS_LOG_COMPONENT_DEFINE ("CognitiveManager");
NS_OBJECT_ENSURE_REGISTERED (CognitiveManager);
 
//~ void handler (float period, Ptr<LteHelper> lteHelper)
//~ {
    //~ //Ptr< RadioBearerStatsCalculator >  statCalc =  lteHelper->GetPdcpStats ();
    //~ Ptr< RadioBearerStatsCalculator >  statCalc =  lteHelper->GetRlcStats ();
    //~ uint32_t cellID = statCalc->GetDlCellId (1, 0); //IMSI=1; LCID=??
//~ 
//~ //GetNoiseFigure () const
    //~ 
//~ //    std::cout << Simulator::Now().GetSeconds() << " handler called with argument arg0=" << period << " CellID: "<< cellID << std::endl;
    //~ Simulator::Schedule(Seconds(period), &handler, period, lteHelper);
//~ }
//~ 

  
TypeId CognitiveManager::GetTypeId(void)
{
  static TypeId tid = TypeId ("ns3::CognitiveManager")
    .SetParent<Application> ()
    .AddConstructor<CognitiveManager> ()
 /*   .AddAttribute ("Local", "The Address on which to Bind the rx socket.",
                   AddressValue (),
                   MakeAddressAccessor (&PacketSink::m_local),
                   MakeAddressChecker ())
    .AddAttribute ("Protocol", "The type id of the protocol to use for the rx socket.",
                   TypeIdValue (UdpSocketFactory::GetTypeId ()),
                   MakeTypeIdAccessor (&PacketSink::m_tid),
                   MakeTypeIdChecker ())
    .AddTraceSource ("Rx", "A packet has been received",
                     MakeTraceSourceAccessor (&PacketSink::m_rxTrace))*/
  ;
  return tid;
}

CognitiveManager::CognitiveManager ()
  : m_server_socket (0),
    m_accepted_socket (0),
    m_bind_address(),
    m_nbrPackets (0),
    m_second_trip_case(0),
    m_ueDatabase(0),
    m_futureExtent_seconds(0.0),
    m_currentTabIndex(0),
    m_nbr_of_measures(0),
    m_databasePeriod(MilliSeconds (200)),
    m_ueMacDatabase(0),
    m_nbMacMeasures(0),
    m_currentMinMcs(999999),
    m_currentMaxMcs(0),
    m_currentMacWndStart(0)
{
}

CognitiveManager::~CognitiveManager()
{
  m_server_socket = 0;

  if (m_ueDatabase)
    std::free(m_ueDatabase);

  if (m_ueMacDatabase)
    std::free(m_ueMacDatabase);
}

unsigned long CognitiveManager::GetIndexOfposition (float pos, unsigned long baseIndex)
{
  unsigned long l;

  if (baseIndex > m_nbr_of_measures){
    //std::cout << "\n baseIndex > m_nbMeasures" << std::endl;
    return m_nbr_of_measures;
  }

  if (pos > m_ueDatabase[m_nbr_of_measures-1].posX){
    //std::cout << "\n pos > m_ueDatabase[m_nbMeasures-1].posX ---------pos = " << pos << "  m_ueDatabase[m_nbMeasures].posX =" <<  m_ueDatabase[m_nbr_of_measures].posX << std::endl;
    return m_nbr_of_measures;
  }
  
  for (l=baseIndex; (l<m_nbr_of_measures) && (m_ueDatabase[l].posX < pos); l++)
  {
  }
  
  return l;
}


unsigned long CognitiveManager::GetMacIndexOfposition (float pos, unsigned long baseIndex)
{
  unsigned long l;

  if (baseIndex > m_nbMacMeasures){
    //std::cout << "\n baseIndex > m_nbMacMeasures ---------------------------" << std::endl;
    return m_nbMacMeasures;
  }

  if (pos > m_ueMacDatabase[m_nbMacMeasures-1].posX){
    //std::cout << "\n pos > m_ueMacDatabase[m_nbMacMeasures].posX ---------pos = " << pos << "  m_ueMacDatabase[m_nbMacMeasures].posX =" <<  m_ueMacDatabase[m_nbMacMeasures].posX << std::endl;
    return m_nbMacMeasures;
  }
  
  for (l=baseIndex; (l<m_nbMacMeasures) && (m_ueMacDatabase[l].posX < pos); l++)
  {
  }
  
  return l;
}


void CognitiveManager::Setup (Ptr<MobilityModel> mrPosition, Ptr<Socket> socket, Address address, bool second_trip_case, float futureExtent_seconds, char* dataBasePath, Ptr<LteHelper> lteHelper, Time databasePeriod, char* macDataBasePath)
{
  FILE * pFile;
  FILE * pMacFile;
  float r_time, r_rsrp, r_rsrq;
  int r_rnti, r_cID;
  int r_servCellBool;
  float r_posX, r_speedX;
  int r_frameNum, r_subFrameNum, r_mcs1, r_sizeTb1, r_mcs2, r_sizeTb2;
  int i;
    
  m_mrPosition = mrPosition;  
  m_server_socket = socket;
  m_bind_address = address; 
  m_second_trip_case = second_trip_case;
  m_databasePeriod = databasePeriod; 

    
  // if LTE
  // floor round to the periodicity of LTE internal measurements = m_ueMeasurementsFilterPeriod of LteUePhy class
  //m_futureExtent_seconds = futureExtent_seconds - fmod(futureExtent_seconds, databasePeriod.GetSeconds ()); 
  //->  seems fmod does not like float to douvble conversion => rewrite it by hand
  float quot = futureExtent_seconds / databasePeriod.GetSeconds ();
  float tquot = ceil (quot);
  float rest = futureExtent_seconds - tquot * databasePeriod.GetSeconds ();
  m_futureExtent_seconds = futureExtent_seconds - rest;
  
  NS_LOG_DEBUG ("---------> Cognitive Manager future extent : " << m_futureExtent_seconds);      
  // else if Wifi ...
  
  if (m_second_trip_case == 0)
  {
        //~ Simulator::Schedule (databasePeriod, &handler, period, lteHelper);
  }
  else // ==> if (m_second_trip_case != 0)
  {
    //_______________cId/RSRP/RSRQ Database______________
    pFile = fopen (dataBasePath, "r");
     
    if (pFile!=NULL)
    {
      NS_LOG_DEBUG ("Database File opening successful");      
      
      // Determine the number of lines of the file
      m_nbr_of_measures = 0;
      while (!feof (pFile))
      {
        fscanf(pFile,"%*f\t%*f\t%*f\t%*f\t%*f\t%*f\t%*f\t%*i\t%*f\n");
        m_nbr_of_measures++;
      }
      NS_LOG_DEBUG ("Number of measurements in the file :" << "\t" << m_nbr_of_measures);
      fseek(pFile, 0, SEEK_SET); // rewind
     
      
      // Create a measurements Tab of the corresponding size
      m_ueDatabase = static_cast<ueMeasurements *>(std::malloc(m_nbr_of_measures*sizeof(ueMeasurements)));
      
      // Parse the file and fill the measurements tab
      i = 0;
      while ( !feof (pFile))
      {
            fscanf(pFile,"%f\t%f\t%f\t%d\t%d\t%f\t%f\t%d\n", &r_time, &r_posX, &r_speedX, &r_rnti, &r_cID, &r_rsrp, &r_rsrq, &r_servCellBool);
            m_ueDatabase[i].time        = round(r_time * 10) /10; // round to one digit
            m_ueDatabase[i].posX        = r_posX;
            m_ueDatabase[i].speedX      = r_speedX;
            m_ueDatabase[i].rnti        = r_rnti;
            m_ueDatabase[i].cID         = r_cID; // Serving CellID
            m_ueDatabase[i].rsrp        = r_rsrp;
            m_ueDatabase[i].rsrq        = r_rsrq;
            m_ueDatabase[i].servCellBool= r_servCellBool; // Current CellID = Serving CellID ?
            
            i++;
            
            NS_LOG_INFO (r_time << "\t" << r_posX << "\t"  << r_speedX << "\t" << r_rnti << "\t" << r_cID << "\t" << r_rsrp << "\t" << r_rsrq << "\t" << r_servCellBool);
      }//END while ( !feof (pFile)) (file reading)

      fclose (pFile);
    
    }//END if (pFile!=NULL)
    else
    {
      NS_LOG_ERROR (Simulator::Now ().GetSeconds () << "\t" << " !!! File opening failed !!! ");
    }
    
    
    
    //_________________MAC Database________________
    pMacFile = fopen (macDataBasePath, "r");
    
    if (pMacFile != NULL)
    {
      NS_LOG_DEBUG ("Mac File opening successful");      
      
      // Determine the number of lines of the file
      m_nbMacMeasures = 0;
      while (!feof (pMacFile))
      {
        fscanf (pMacFile, "%*f\t%*f\t%*f\t%*d\t%*d\t%*d\t%*d\t%*d\t%*d\t%*d\n");
        m_nbMacMeasures++;
      }
      NS_LOG_DEBUG ("Number of measurements in the MAC database file :" << "\t" << m_nbMacMeasures);

      fseek(pMacFile, 0, SEEK_SET); // rewind
      
      
      // Create a measurements Tab of the corresponding size
      m_ueMacDatabase = static_cast<ueMacMeasurements *>(std::malloc(m_nbMacMeasures*sizeof(ueMacMeasurements)));
      
      // Parse the file and fill the measurements tab
      i = 0;
      while ( !feof (pMacFile))
      {
            fscanf(pMacFile,"%f\t%f\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", &r_time, &r_posX, &r_speedX, &r_frameNum, &r_subFrameNum, &r_rnti, &r_mcs1, &r_sizeTb1, &r_mcs2, &r_sizeTb2);

            m_ueMacDatabase[i].time        = r_time;
            m_ueMacDatabase[i].speedX        = r_speedX;
            m_ueMacDatabase[i].posX        = r_posX;
            m_ueMacDatabase[i].frameNum    = r_frameNum;
            m_ueMacDatabase[i].subFrameNum = r_subFrameNum;
            m_ueMacDatabase[i].rnti        = r_rnti;
            m_ueMacDatabase[i].mcs1        = r_mcs1;
            m_ueMacDatabase[i].sizeTb1     = r_sizeTb1;
            m_ueMacDatabase[i].mcs2        = r_mcs2;
            m_ueMacDatabase[i].sizeTb2     = r_sizeTb2;
            
            i++;
            
            NS_LOG_INFO (r_time << "\t" << r_posX << "\t"  << r_speedX << "\t" 
                    << r_frameNum << "\t" << r_subFrameNum << "\t" 
                    << r_rnti << "\t"
                    << r_mcs1<< "\t" << r_sizeTb1 << "\t"
                    << r_mcs2<< "\t" << r_sizeTb2
                    );
      }//END while ( !feof (pMacFile)) (file reading)
      
      fclose (pMacFile);
      
    }//END if (pMacFile!=NULL)
    else
    {
      NS_LOG_ERROR (Simulator::Now ().GetSeconds () << "\t" << " !!! MAC File opening failed !!! ");
    }
    
    //____________Start CM scheduling______________
    Time tNext (Seconds (m_ueDatabase[0].time));
    Simulator::Schedule (tNext, &CognitiveManager::CheckFutureLink, this);     
    
  } //END if/else (m_second_trip_case == 0)
  
}

void CognitiveManager::StartApplication ()
{
  //m_socket->SetIpRecvTos(true);
  //m_socket->SetRecvCallback(MakeCallback (&CognitiveManager::test,this));
  m_server_socket->SetAcceptCallback ( MakeNullCallback<bool, Ptr<Socket>, const Address &> (),
                                MakeCallback (&CognitiveManager::HandleAccept, this));
  
  m_server_socket->Bind(m_bind_address);
  m_server_socket->Listen();
}

void CognitiveManager::StopApplication (void)
{
  if (m_server_socket)
  {
    m_server_socket->Close ();    
  }
}

void CognitiveManager::HandleAccept (Ptr<Socket> s, const Address& from)
{
  m_accepted_socket = s;
  s->SetRecvCallback (MakeCallback (&CognitiveManager::ReceivePacket, this));
  s->SetIpRecvTos(true);
  //m_socketList.push_back (s);
}

void CognitiveManager::ReceivePacket (Ptr<Socket> socket)
{
  Ptr<Node> noeud = socket->GetNode();
  m_nbrPackets++;
  //std::cout << Simulator::Now ().GetSeconds () << " : _________________________ "<< m_nbrPackets <<" PAQUETS RECUS au noeud "<< noeud->GetId() <<" ________________________" << std::endl;
  //std::cout << "DEBUT du traitement de reception du paquet sur le noeud " << noeud->GetId() << std::endl;
  Ptr<Packet> packet = socket->Recv ();
  
  /*std::cout << "FIN du traitement de reception du paquet sur le noeud " << noeud->GetId() << std::endl;
  packet->PrintPacketTags(std::cout);
  SocketIpTosTag tosTag;
  bool tagbool = packet->RemovePacketTag (tosTag);
  std::cout << "PRESENCE DE TAG TOS = " << tagbool << std::endl;
  if (tagbool)
    {
      std::cout << " TOS = " << (uint32_t)tosTag.GetTos () << std::endl;
    }*/
}

void CognitiveManager::CheckFutureLink(void)
{
  int   cell_index, future_index,
        nbr_cell_current, nbr_cell_future, 
        current_serving_CellId, future_serving_CellId;
  unsigned long      i;  
  float future_rsrp_serving, current_rsrp_serving,
        future_rsrq_serving, current_rsrq_serving, 
        future_rsrq_best_neighbour, future_rsrp_best_neighbour;
  int   minMcs, maxMcs;
  float currentPos, endPos;
  
  NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : ---- Checking future link state ----");
  
  
  
  
  // Determine the current and future positions 
  currentPos = round(m_mrPosition->GetPosition ().x*1000000.0f)/1000000.0f;
  endPos = currentPos + m_futureExtent_seconds * (m_mrPosition->GetVelocity ().x);
    
  NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Current Position  = " << currentPos << "\t window span till : " << endPos);


  // ______________________________________________________________
  // ______________________LEGACY DATABASE_________________________
  // ______________________________________________________________
  // retrieve index of current position
  m_currentTabIndex = GetIndexOfposition (currentPos, m_currentTabIndex);
    
  // Determine the number of cells at the current time (WE COULD MONITOR THIS NUMBER, AND REACT IF IT CHANGES (+ or -))
  nbr_cell_current=0;
  while (m_ueDatabase[m_currentTabIndex + nbr_cell_current].posX == currentPos)
  {
    nbr_cell_current++;
  }
  NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : " << nbr_cell_current << " cells in sight for the moment");
    
  // ____________________Get current state_________________________
  // Look for the current serving cell
  // (This information might be accessible from the node itself by GetTargetEnb() and GetCellId())
  for (cell_index=0; cell_index<nbr_cell_current ; cell_index++)
  {
    if (m_ueDatabase[m_currentTabIndex + cell_index].servCellBool == 1) // serving cell
    {
      current_serving_CellId = m_ueDatabase[m_currentTabIndex + cell_index].cID;
      current_rsrq_serving = m_ueDatabase[m_currentTabIndex + cell_index].rsrq;
      current_rsrp_serving = m_ueDatabase[m_currentTabIndex + cell_index].rsrp;

      NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Current serving cell number : " << current_serving_CellId);
      NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Current RSRQ of the serving cell = " << current_rsrq_serving);
      NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Current RSRP of the serving cell = " << current_rsrp_serving);
    }
  } 
  
  // ____________________Get the future state______________________
  // retrieve index of current position
  future_index = GetIndexOfposition (endPos, m_currentTabIndex);  
  NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Index of current: " << m_currentTabIndex << "\tindex of future: " << future_index);    
    
    
  // Determine the number of cells at the future moment (WE COULD MONITOR THIS NUMBER, AND REACT IF IT CHANGES (+ or -))
  nbr_cell_future=0;
  while(m_ueDatabase[future_index + nbr_cell_future].posX <= endPos)
  {
    nbr_cell_future++;
  }
  NS_LOG_INFO ( Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : " << nbr_cell_future << " cells in sight at future time ");
    
//TODO: WHAT HAPPENS WHEN THERE IS NO CELL CURRENTLY IN SIGHT/REPORTED ???
    
  // Look for the future RSRP and RSRQ of the serving cell
  for (cell_index=0; cell_index<nbr_cell_future ; cell_index++)
  {
    //if (m_ueDatabase[future_index + cell_index].currentCID == current_serving_CellId) // serving cell
    if (m_ueDatabase[future_index + cell_index].servCellBool == 1) // find future serving cell
    {
        future_serving_CellId = m_ueDatabase[future_index + cell_index].cID;
        future_rsrq_serving = m_ueDatabase[future_index + cell_index].rsrq;
        future_rsrp_serving = m_ueDatabase[future_index + cell_index].rsrp;

        NS_LOG_INFO ( Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Future serving cell = " << future_serving_CellId);
        NS_LOG_INFO ( Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Future RSRQ of the serving cell = " << future_rsrq_serving);
        NS_LOG_INFO ( Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : Future RSRP of the serving cell = " << future_rsrp_serving);

        break;
    } // END if (servCellBool == 1)
  } // END for (cell_index = 0-> nbr_cell_future)

  // Look for the best future neighbouring cell
  future_rsrq_best_neighbour = -1000;
  future_rsrp_best_neighbour = -1000;
  for (cell_index=0; cell_index<nbr_cell_future ; cell_index++)
  {
    if (m_ueDatabase[future_index + cell_index].servCellBool == 0) // neighbouring cell
    {
      if (m_ueDatabase[future_index + cell_index].rsrq >= future_rsrq_best_neighbour) // RSRQ
      {
        future_rsrq_best_neighbour = m_ueDatabase[future_index + cell_index].rsrq;
      }
      if (m_ueDatabase[future_index + cell_index].rsrp >= future_rsrp_best_neighbour) //RSRP
      {
        future_rsrp_best_neighbour = m_ueDatabase[future_index + cell_index].rsrp;
      }
    } // END if (servCellBool == 0)
  } // END for (cell_index = 0-> nbr_cell_future)
    
    
  
  
  
    
  // ______________________________________________________________
  // _______________________MCS EVOLUTION__________________________
  // ______________________________________________________________
  m_currentMacWndStart = GetMacIndexOfposition (currentPos, m_currentMacWndStart);

  //~ std::cout << "\n\n" << Simulator::Now().GetSeconds() << "\t"  << "Nouvelle Iterration         \n  "
          //~ << "  currentPos: " << currentPos << "\t"  << "  endPos: " << endPos << "\n"
          //~ << "  m_currentMacWndStart: " << m_currentMacWndStart << "\t"  << "(Max= " << m_nbMacMeasures << ")"  << "\n"
          //~ << std::endl;

          
  // Look for the Min and Max in the furure window
  minMcs = 9999999;
  maxMcs = 0;
  
  //~ for (i=m_currentMacWndStart; (i<m_nbMacMeasures) && (m_ueMacDatabase[i].time < futureTime); i++)
  for (i=m_currentMacWndStart; (i<m_nbMacMeasures) && (m_ueMacDatabase[i].posX < endPos); i++)
  {
    if ( m_ueMacDatabase[i].mcs1 < minMcs )
        minMcs = m_ueMacDatabase[i].mcs1;
    
    if ( m_ueMacDatabase[i].mcs1 > maxMcs )
        maxMcs = m_ueMacDatabase[i].mcs1;
  }
  
  
  // ______________________________________________________________
  // _____________________WARN IF NECESSARY________________________
  // ______________________________________________________________

  // Warn TCP Freeze if required
  WarnTCP(current_rsrq_serving, future_rsrq_serving, future_rsrq_best_neighbour);
 
  // Warn if Hand over is going to happend
  if (current_serving_CellId != future_serving_CellId)
  {
    NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" 
            << "Handover pending from cell " 
            << current_serving_CellId << " to " << future_serving_CellId
            );
//TODO:  WarnHO(current_serving_CellId, future_serving_CellId);
  }


  // Warn if QOS parameter are to change
  if ( (m_currentMinMcs != minMcs) || (m_currentMaxMcs != maxMcs) )
  {
    NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" 
                << "MCS gonna change: MCSmin: " << m_currentMinMcs << " -> " << minMcs << "\n\t"
                << "                  MCSmax: " << m_currentMaxMcs << " -> " << maxMcs
                );
//TODO:  WarnQOS(m_currentMinMcs, minMcs, m_currentMaxMcs, maxMcs);
  }


  
    
    
  // ______________________________________________________________
  // _________________SCHEDULE NEXT ITERATION______________________
  // ______________________________________________________________

  // Memorise MCS params for next iteration
  m_currentMinMcs = minMcs;
  m_currentMaxMcs = maxMcs;

  m_currentTabIndex += nbr_cell_current;

  ScheduleCheck();

}

void CognitiveManager::ScheduleCheck (void)
{
  // if LTE
  // periodicity of LTE internal measurements = m_ueMeasurementsFilterPeriod of LteUePhy class
//  Time tNext (Seconds (0.2)); 
  // else if ...
  
  Simulator::Schedule (m_databasePeriod, &CognitiveManager::CheckFutureLink, this);

}

void CognitiveManager::WarnTCP (float current_rsrq, float future_rsrq, float future_rsrq_best_neighbour)
{
  float delta_rsrq_time;
  float delta_rsrq_cell;
  
  // if LTE
  delta_rsrq_time = future_rsrq - current_rsrq; //(RSRQ always negative)
  delta_rsrq_cell = future_rsrq - future_rsrq_best_neighbour;
  
  // ______________________________________________________________
  // Link Going Down
  // ______________________________________________________________
  if (delta_rsrq_time <= -DELTA_RSRQ_TUNNEL)
  {
    NS_LOG_DEBUG ( Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
    NS_LOG_DEBUG ( Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : LINK GOING DOWN (SHADOWING, DELTA_RSRQ = " << delta_rsrq_time << ")");
    NS_LOG_DEBUG ( Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- " );
  }
  else if (delta_rsrq_time >= DELTA_RSRQ_TUNNEL)
  {
    NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
    NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : LINK GOING UP (SHADOWING, DELTA_RSRQ = " << delta_rsrq_time << ")");
    NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
  }
  else
  {
    NS_LOG_INFO (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : LINK OK");
  }
  
  // ______________________________________________________________
  // Handover
  // ______________________________________________________________
  if (future_rsrq < ENB_HANDOVER_THRESHOLD)
  {
    if (delta_rsrq_cell <= -1)
    {
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : PENDING ENB HANDOVER DECISION, FUTURE SERVING RSRQ = " << future_rsrq);
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : PENDING ENB HANDOVER DECISION, FUTURE BEST NEIGHBOURING RSRQ = " << future_rsrq_best_neighbour);
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
    }
    else if (delta_rsrq_cell == 0)
    {
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : CURRENTLY HANDING OVER");
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
    }
    else
    {
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : NEED HANDOVER DECISION, FUTURE SERVING RSRQ = " << future_rsrq);
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> Cognitive Manager : TCP WARNING : BUT NO BETTER NEIGHBOURING CELL, FUTURE BEST NEIGHBOURING RSRQ = " << future_rsrq_best_neighbour);
      NS_LOG_DEBUG (Simulator::Now().GetSeconds() << "\t" << "---------> -------------------------------------------------------------------------------------- ");
    }
  }
  
  
  // else if ...
  
}


} // namespace
