/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Telecom Bretagne
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Couturier <christophe.couturier@telecom-bretagne.eu>
 */

#include "traffic-generator.h"
#include "ns3/log.h"
#include "ns3/applications-module.h"


namespace ns3 {
///////////// Count Header Class //////////////////////////////////
class CountHeader : public Header
{
public:

  CountHeader (void);

  // new methods
  void SetData (uint32_t data);
  uint32_t GetData (void);
  // new method needed
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  // overridden from Header
  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual void Print (std::ostream &os) const;

private:
  uint32_t m_data;
};

NS_OBJECT_ENSURE_REGISTERED (CountHeader);


TypeId
CountHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::CountHeader")
    .SetParent<Header> ()
    .AddConstructor<CountHeader> ()
    ;
  return tid;
}

TypeId
CountHeader::GetInstanceTypeId (void) const
{
  return CountHeader::GetTypeId ();
}

CountHeader::CountHeader (void)
{
}

void 
CountHeader::SetData (uint32_t data)
{
  m_data = data;
}
uint32_t
CountHeader::GetData (void)
{
  return m_data;
}


uint32_t 
CountHeader::GetSerializedSize (void) const
{
  return sizeof (m_data);
}
void 
CountHeader::Serialize (Buffer::Iterator start) const
{
  start.WriteHtonU32 (m_data);
}
uint32_t 
CountHeader::Deserialize (Buffer::Iterator start)
{
  m_data = start.ReadNtohU32 ();
  return sizeof (m_data);
}
void 
CountHeader::Print (std::ostream &os) const
{
  os << m_data;
}

///////////////////////////////////////////////////////////////////





NS_LOG_COMPONENT_DEFINE ("TrafficGenerator");
NS_OBJECT_ENSURE_REGISTERED (TrafficGenerator);

TypeId
TrafficGenerator::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::TrafficGenerator")
    .SetParent<Application> ()
    .AddConstructor<TrafficGenerator> ()
    .AddTraceSource ("RxMessageNumber", "Number of the last Rx message",
                     MakeTraceSourceAccessor (&TrafficGenerator::m_lasRxPacket))
  ;
  return tid;
}
TypeId
TrafficGenerator::GetInstanceTypeId (void) const
{
  return TrafficGenerator::GetTypeId ();
}

TrafficGenerator::TrafficGenerator ()
  : m_socket (0),
    m_sink(0),
    m_packetSize (0), 
    m_dataRate (0), 
    m_bulkBytesSent (0),
    m_sendEvent (), 
    m_running (false), 
    m_txPacketCounter (0),
    m_lasRxPacket (0),
    m_currentMsg (0),
    m_currentMsgPos (0),  
    m_rxPacketsLast (0),
    m_rxBytesLast (0),
    m_rxDelayTotal (Seconds(0)),
    m_rxDelayMin (Seconds(999999)),
    m_rxDelayMax (Seconds(0)),
    m_iptype (ipUnset),
    m_mode (modeUnset)
{
}

TrafficGenerator::~TrafficGenerator()
{
  m_socket = 0;
  
  if ( m_currentMsg )
  {
    free (m_currentMsg);
  }
}

void 
TrafficGenerator::RxPacketSinkCallback (Ptr<const Packet> p, const Address& add)
{
  //NS_LOG_INFO ("RxPacketSinkCallback\n");            
  
  // Counter update (packets, bytes and delay)
  m_rxPacketsLast ++;
  m_rxBytesLast   += p->GetSize();

  DelayJitterEstimation delay;
  delay.RecordRx(p);
  Time d = delay.GetLastDelay(); 
  m_rxDelayTotal += d; 
  if (d < m_rxDelayMin) m_rxDelayMin = d;
  if (d > m_rxDelayMax) m_rxDelayMax = d;
  //std::cout << Simulator::Now ().GetSeconds () << "\t :" << d.GetSeconds () << "\t" << m_rxDelayTotal.GetSeconds () << "\t" << m_rxDelayMin.GetSeconds () << "\t" << m_rxDelayMax.GetSeconds () << std::endl;
  
  // Extract the message number
  Packet p1 = Packet (*p);
  uint32_t remaining = p1.GetSize();  // we need to copy the packet to be allowed to remove portions we have already read
  //~ NS_LOG_DEBUG ("Entering extract loop: pos=" << m_currentMsgPos << ", remaining=" << remaining );
  while (remaining > 0)
  {
    uint32_t nbToRead =  ( (m_currentMsgPos + remaining) <= m_packetSize ) 
                          ? remaining : (m_packetSize - m_currentMsgPos);
    
    //~ NS_LOG_DEBUG ("  pos=" << m_currentMsgPos << ", remaining=" << remaining <<  ", nbToRead=" << nbToRead );
    
    p1.CopyData ( &m_currentMsg[m_currentMsgPos], nbToRead);
    m_currentMsgPos += nbToRead;
    
    if ( m_currentMsgPos == m_packetSize ) // i.e. new message to decode
      {
        // Decode data to get message number
        Packet p2 = Packet (m_currentMsg, m_packetSize);
        CountHeader h;
        p2.RemoveHeader (h);
        m_lasRxPacket = h.GetData ();

        //~ NS_LOG_DEBUG ("  ==> Received message #" << m_lasRxPacket); 

        // reset Rx buffer to 0
        m_currentMsgPos = 0;
        
      } // END if if ( m_currentMsgPos == m_packetSize )

    p1.RemoveAtStart (nbToRead);
    remaining -= nbToRead;
    
  } // END while (remaining > 0)

//~ Packet p2 = Packet (m_currentMsg, m_packetSize);
//~ CountHeader h2;
//~ p2.RemoveHeader (h2);
//~ uint32_t tmp  = h2.GetData ();
//~ NS_LOG_DEBUG ("  sortie: remaining=" << remaining << ",  head=" << tmp); 
}


void
TrafficGenerator::CommonIpv4Ipv6Setup (Ptr<Node> srcNode, uint32_t packetSize, DataRate dataRate)
{
  NS_ASSERT_MSG (packetSize >= sizeof (m_txPacketCounter),
                 "Traffic Generator expect packetSize of at least " << sizeof (m_txPacketCounter) << " bytes" <<
                 "  (given: " << packetSize << ")");
  
  m_mode = cbr;
  m_packetSize = packetSize;
  m_dataRate = dataRate;
  
  // allocation of the Rx buffer that will be use to decode message number during Rx process
  m_currentMsg = (uint8_t*) malloc (m_packetSize);
  
  m_sink->TraceConnectWithoutContext ("Rx", MakeCallback (&TrafficGenerator::RxPacketSinkCallback, this));

  srcNode->AddApplication (this);
}



void
TrafficGenerator::Setup (std::string protocol, Ptr<Node> srcNode, Ptr<Node> destNode, Ipv4Address destAddress, uint16_t port,
                       uint32_t packetSize, DataRate dataRate)
{
  NS_LOG_INFO ("Creating IPv4 Traffic Generator for: " << protocol <<"  port: " << port <<
               "\tdestAddress: " << destAddress << "\n");

  m_iptype = ipv4;
  
  PacketSinkHelper packetSinkHelper (protocol, InetSocketAddress (Ipv4Address::GetAny (), port)); 
  ApplicationContainer sinkApp = packetSinkHelper.Install (destNode);
  m_sink = sinkApp.Get (0)->GetObject <PacketSink>();
  
  TypeId tid = TypeId::LookupByName (protocol);
  m_socket = Socket::CreateSocket (srcNode, tid);
  m_destAddress = InetSocketAddress (destAddress, port);
  
  CommonIpv4Ipv6Setup (srcNode, packetSize, dataRate);
}
//~ void
//~ TrafficGenerator::Setup (std::string protocol, Ptr<Node> srcNode, Ptr<Node> destNode, Address destAddress, uint16_t port,
                       //~ uint32_t packetSize, DataRate dataRate)
//~ {
  //~ uint8_t   addBuff[16];
  //~ destAddress.CopyTo (addBuff);
  //~ 
  //~ if (Ipv4Address::IsMatchingType (destAddress))
    //~ {
      //~ 
      //~ m_iptype = ipv4;
//~ m_destAddress = InetSocketAddress (destAddress, port);
//~ 
      //~ NS_LOG_INFO ("Creating IPv4 Traffic Generator for: " << protocol <<"  port: " << port <<
               //~ "\tdestAddress: " << destAddress << "\tm_destAddress: " << m_destAddress << "\n");
//~ NS_LOG_INFO ("\taddBuff: " << addBuff << "\tipv4Add: " << ipv4Add << "\n");
//~ 
      //~ PacketSinkHelper packetSinkHelper (protocol, InetSocketAddress (Ipv4Address::GetAny (), port)); 
      //~ ApplicationContainer sinkApp = packetSinkHelper.Install (destNode);
      //~ m_sink = sinkApp.Get (0)->GetObject <PacketSink>();
    //~ }
  //~ else if (Ipv6Address::IsMatchingType (destAddress))
    //~ {
//~ uint8_t   addBuff[16];
//~ destAddress.CopyTo (addBuff);      
      //~ 
      //~ NS_LOG_INFO ("Creating IPv6 Traffic Generator for: " << protocol <<"  port: " << port <<
                   //~ "\tdestAddress: " << destAddress << "\n");
      //~ m_iptype = ipv6;
      //~ Ipv6Address ipv6Add ( (char*)addBuff );
      //~ m_destAddress = Inet6SocketAddress (ipv6Add, port);
//~ 
      //~ PacketSinkHelper packetSinkHelper (protocol, Inet6SocketAddress (Ipv6Address::GetAny (), port)); 
      //~ ApplicationContainer sinkApp = packetSinkHelper.Install (destNode);
      //~ m_sink = sinkApp.Get (0)->GetObject <PacketSink>();
    //~ }
  //~ else
    //~ {
      //~ NS_FATAL_ERROR  ("Tryed to initialise Traffic Generator with bad IP address (neither IPv4 nor IPv6)");
      //~ return;
    //~ }
  //~ 
  //~ TypeId tid = TypeId::LookupByName (protocol);
  //~ m_socket = Socket::CreateSocket (srcNode, tid);
  //~ 
  //~ CommonIpv4Ipv6Setup (srcNode, packetSize, dataRate);
//~ }

void 
TrafficGenerator::SetupBulk (std::string protocol,  Ptr<Node> srcNode, Ptr<Node> destNode,
            Ipv4Address destAddress, uint16_t port, uint32_t packetSize, uint32_t dataSize)
{
  Setup (protocol, srcNode, destNode, destAddress, port, packetSize, 0); // will initialise Tx socket and Rx sink in the same maner as for cbr

  m_mode = bulk;
  m_bulkSize = dataSize;
}
//~ void 
//~ TrafficGenerator::SetupBulk (std::string protocol,  Ptr<Node> srcNode, Ptr<Node> destNode,
            //~ Address destAddress, uint16_t port, uint32_t packetSize, uint32_t dataSize)
//~ {
  //~ Setup (protocol, srcNode, destNode, destAddress, port, packetSize, 0); // will initialise Tx socket and Rx sink in the same maner as for cbr
//~ 
  //~ m_mode = bulk;
  //~ m_bulkSize = dataSize;
//~ }

void
TrafficGenerator::Setup (std::string protocol, Ptr<Node> srcNode, Ptr<Node> destNode, Ipv6Address destAddress, uint16_t port,
                       uint32_t packetSize, DataRate dataRate)
{
  NS_LOG_INFO ("Creating IPv6 Traffic Generator for: " << protocol <<"  port: " << port <<
               "\tdestAddress: " << destAddress << "\n");

  m_iptype = ipv6;

  PacketSinkHelper packetSinkHelper (protocol, Inet6SocketAddress (Ipv6Address::GetAny (), port));
  ApplicationContainer sinkApp = packetSinkHelper.Install (destNode);
  m_sink = sinkApp.Get (0)->GetObject <PacketSink>();
  
  TypeId tid = TypeId::LookupByName (protocol);
  m_socket = Socket::CreateSocket (srcNode, tid);
  m_destAddress = Inet6SocketAddress (destAddress, port);
  
  CommonIpv4Ipv6Setup (srcNode, packetSize, dataRate);
}
void
TrafficGenerator::SetupBulk (std::string protocol,  Ptr<Node> srcNode, Ptr<Node> destNode,
            Ipv6Address destAddress, uint16_t port, uint32_t packetSize, uint32_t dataSize)
{
  Setup (protocol, srcNode, destNode, destAddress, port, packetSize, 0); // will initialise Tx socket and Rx sink in the same maner as for cbr

  m_mode = bulk;
  m_bulkSize = dataSize;
}


bool
TrafficGenerator::RecordTraffic (std::string filePath, Time samplePeriod)
{
  AsciiTraceHelper asciiTraceHelper;
  m_recordFile = asciiTraceHelper.CreateFileStream (filePath);
  
  if (m_recordFile == 0) return false;
  
  WriteRecordInFile (samplePeriod);
  return true;
}

void 
TrafficGenerator::WriteRecordInFile (Time samplePeriod)
{
  double throughput = double(m_rxBytesLast) * 8.0d / (samplePeriod.GetSeconds () * 1024.0d * 1024.0d);
 
  *m_recordFile->GetStream() << Simulator::Now ().GetSeconds () << "\t" << throughput <<  "\t" << m_rxBytesLast << "\t" << m_rxPacketsLast << "\t";
  if (m_rxPacketsLast!=0)
    {
       *m_recordFile->GetStream() << m_rxDelayMin.GetSeconds () << "\t" << m_rxDelayMax.GetSeconds () << "\t" << m_rxDelayTotal.GetSeconds () / m_rxPacketsLast;
    }
  else
    {
       *m_recordFile->GetStream() << "-" << "\t" << "-" << "\t" << "-";
    }
  *m_recordFile->GetStream() << std::endl;
                                
  m_rxBytesLast   = 0;
  m_rxPacketsLast = 0;
  m_rxDelayTotal  = Seconds(0);
  m_rxDelayMin    = Seconds(999999);
  m_rxDelayMax    = Seconds(0);
  Simulator::Schedule (samplePeriod, &TrafficGenerator::WriteRecordInFile, this, samplePeriod);  
}

void
TrafficGenerator::StartApplication (void)
{
  m_running = true;
  switch (m_iptype)
    {
      case ipv4:
        m_socket->Bind ();
        NS_LOG_INFO ("TrafficGenerator binded in IPv4\n");
        break;
      case ipv6:
        m_socket->Bind6 ();
        NS_LOG_INFO ("TrafficGenerator binded in IPv6\n");
        break;
      default:
        NS_LOG_ERROR ("TrafficGenerator started with unrecognized IP version\n");
    }

  m_socket->Connect (m_destAddress);
  
  if (m_mode == cbr)
    {
      SendPacket ();
    }
    else if (m_mode == bulk)
    {
      m_socket->SetSendCallback ( MakeCallback (&TrafficGenerator::DataSentCallback, this));
    }
}

void 
TrafficGenerator::StopApplication (void)
{
  m_running = false;

  if (m_sendEvent.IsRunning ())
    {
      Simulator::Cancel (m_sendEvent);
    }

  if (m_socket)
    {
      m_socket->Close ();
    }
}

void 
TrafficGenerator::SendPacket (void)
{
  // header to store message number
  CountHeader header;
  header.SetData (++m_txPacketCounter);

  // Create packet and add header (total size of m_packetSize)
  uint32_t size = m_packetSize - sizeof(m_txPacketCounter);
  Ptr<Packet> packet = Create<Packet> (size);
  packet->AddHeader (header);

  // Add Jitter information
  DelayJitterEstimation delay;
  delay.PrepareTx (packet); 

  // Send packet if enough space in Tx buffer of the socket
  if ( (m_packetSize) > m_socket->GetTxAvailable ()  )
    {
      NS_LOG_INFO ("TrafficGenerator: Not enough space in Tx buffer, Droping packet (size of packet=" << m_packetSize << ", availble in socket buffer:" << m_socket->GetTxAvailable () << ")" );  
    }
  else
    {
      NS_LOG_INFO ("m_socket->Send (packet);  " << *packet);  
      int tst = m_socket->Send (packet);
      if (tst < 0 )
        {
          NS_LOG_INFO ("TrafficGenerator: Tx error (overflow or something like this) err=" << tst);
        }
    }

  // Schedule next packet send
  ScheduleTx ();
}

void 
TrafficGenerator::ScheduleTx (void)
{
  if (m_running)
    {
      Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
      m_sendEvent = Simulator::Schedule (tNext, &TrafficGenerator::SendPacket, this);
    }
}

Ptr<Socket>
TrafficGenerator::GetSinkListeningSocket (void)
{
  NS_ASSERT_MSG (m_sink!=0, "Trying to access an non initialised sink (No connection established for the moment)");
  Ptr<Socket> p = m_sink->GetListeningSocket ()->GetObject <Socket>();
  return p;
}

Ptr<Socket>
TrafficGenerator::GetSinkConnectedSocket (uint16_t target)
{
  NS_ASSERT_MSG (m_sink!=0, "Trying to access an non initialised sink (No connection established for the moment)");

  std::list< Ptr<Socket> >  lst = m_sink->GetAcceptedSockets ();
  std::list< Ptr<Socket> >::iterator it; 
  uint16_t  count = 0;
  
  it = lst.begin();
  while (it != lst.end() && count < target)
    {
      it++;
      count++;
    }
  if (count == target)
    {
      return *it;
    }
  else
    {
      NS_LOG_ERROR ("ERROR: No Connected Sink soket found !!!");
      return 0;
    }
}
  
  
uint16_t
TrafficGenerator::GetNbConnectionsToSink ()
{
  NS_ASSERT_MSG (m_sink!=0, "Trying to access an non initialised sink (No connection established for the moment)");
  std::list< Ptr<Socket> >  lst = m_sink->GetAcceptedSockets ();
  std::list< Ptr<Socket> >::iterator it; 
  uint16_t  count = 0;

  it = lst.begin();
  while (it != lst.end())
    {
      it++;
      count++;
    }
  return count;
}


void 
TrafficGenerator::DataSentCallback (Ptr<Socket>, uint32_t)
{
  NS_LOG_FUNCTION (this);
  if (m_running)
    { // Only send new data if the connection has completed
      Simulator::ScheduleNow (&TrafficGenerator::SendBulkData, this);
    }
}

void 
TrafficGenerator::SendBulkData (void)
{
  NS_LOG_FUNCTION (this);


  while (m_bulkSize == 0 || m_bulkBytesSent < m_bulkSize)
    { // Time to send more
      uint32_t toSend = m_packetSize;
      // Make sure we don't send too many
      if (m_bulkSize > 0)
        {
          toSend = std::min (m_packetSize, m_bulkSize - m_bulkBytesSent);
        }
      
    
      // header to store message number
      CountHeader header;
      header.SetData (++m_txPacketCounter);
 
      // Create packet and add header (total size of m_packetSize)
      uint32_t size;
      if ( toSend > sizeof(m_txPacketCounter) )
        {
          size = toSend - sizeof(m_txPacketCounter);
        }
      else
        {
          size = 0;
        }
      Ptr<Packet> packet = Create<Packet> (size);
      packet->AddHeader (header);

      // Add Jitter information
      DelayJitterEstimation delay;
      delay.PrepareTx (packet); 

      NS_LOG_LOGIC ("sending packet of " << size + sizeof(m_txPacketCounter) << " at " << Simulator::Now () );
      int actual = m_socket->Send (packet);
      
      if (actual > 0)
        {
          m_bulkBytesSent += actual;
        }

      // We exit this loop when actual < toSend as the send side
      // buffer is full. The "DataSent" callback will pop when
      // some buffer space has freed ip.
      if ((unsigned)actual != toSend)
        {
          break;
        }
    } // END of while bytes to send

  // Check if time to close (all sent)
  if (m_bulkSize !=0 && m_bulkBytesSent >= m_bulkSize && m_running)
    {
      m_socket->Close ();
      m_running = false;
    }
}



void 
TrafficGenerator::TraceCwnd (std::string filePath)
{
  //TODO: Assert the protocol is TCP and supporting CWND tracing
  AsciiTraceHelper asciiTraceHelper;
  m_cwndStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("CongestionWindow", MakeCallback (&TrafficGenerator::CwndChangeCallback, this));
}

void 
TrafficGenerator::TraceSSThreshold (std::string filePath)
{
  //TODO: Assert the protocol is TCP and support SSThresh tracing
  AsciiTraceHelper asciiTraceHelper;
  m_ssThresholdStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("SlowStartThreshold", MakeCallback (&TrafficGenerator::SSThresholdChangeCallback, this));
}

void 
TrafficGenerator::TraceRtt (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_rttStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("RTT", MakeCallback (&TrafficGenerator::RttChangeCallback, this));
}

void 
TrafficGenerator::TraceRto (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_rtoStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("RTO", MakeCallback (&TrafficGenerator::RtoChangeCallback, this));
}

void 
TrafficGenerator::TraceRwnd (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_rwndStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("RWND", MakeCallback (&TrafficGenerator::RwndChangeCallback, this));
}

void 
TrafficGenerator::TraceTxSequence (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_txSeqStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("NextTxSequence", MakeCallback (&TrafficGenerator::NextTxSeqCallback, this));
}

void 
TrafficGenerator::TraceHighestTxSequence (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_txHighestSeqStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("HighestSequence", MakeCallback (&TrafficGenerator::HighestTxSeqCallback, this));
}

//~ void 
//~ TrafficGenerator::TraceRxSequence (std::string filePath)
//~ {
  //~ //TODO: Assert the protocol is TCP
  //~ AsciiTraceHelper asciiTraceHelper;
  //~ m_rxSeqStream = asciiTraceHelper.CreateFileStream (filePath);
  //~ m_sink->TraceConnectWithoutContext ("StatLastRxSeq", MakeCallback (&TrafficGenerator::NextRxSeqCallback, this));
//~ }


void 
TrafficGenerator::TraceActualWnd (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_actualWndStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("StatActualWindow", MakeCallback (&TrafficGenerator::ActualWndChangeCallback, this));
}

void 
TrafficGenerator::TraceInFlight (std::string filePath)
{
  //TODO: Assert the protocol is TCP
  AsciiTraceHelper asciiTraceHelper;
  m_inFlightStream = asciiTraceHelper.CreateFileStream (filePath);
  m_socket->TraceConnectWithoutContext ("StatInFlight", MakeCallback (&TrafficGenerator::ActualInFlightCallback, this));
}


void 
TrafficGenerator::TraceRxMsg (std::string filePath)
{
  AsciiTraceHelper asciiTraceHelper;
  m_rxNbStream = asciiTraceHelper.CreateFileStream (filePath);
  TraceConnectWithoutContext ("RxMessageNumber", MakeCallback (&TrafficGenerator::RxMsgChangeCallback, this));  
}

void
TrafficGenerator::CwndChangeCallback (uint32_t oldCwnd, uint32_t newCwnd)
{
  *m_cwndStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldCwnd << std::endl;
  *m_cwndStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newCwnd << std::endl;
}

void
TrafficGenerator::SSThresholdChangeCallback (uint32_t oldSst, uint32_t newSst)
{
  *m_ssThresholdStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldSst << std::endl;
  *m_ssThresholdStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newSst << std::endl;
}

void
TrafficGenerator::RttChangeCallback (Time oldRtt, Time newRtt)
{
  *m_rttStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldRtt.GetSeconds() << std::endl;
  *m_rttStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newRtt.GetSeconds() << std::endl;
}

void
TrafficGenerator::RtoChangeCallback (Time oldRto, Time newRto)
{
  *m_rtoStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldRto.GetSeconds() << std::endl;
  *m_rtoStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newRto.GetSeconds() << std::endl;
}

void
TrafficGenerator::RwndChangeCallback (uint32_t oldRwnd, uint32_t newRwnd)
{
  *m_rwndStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldRwnd << std::endl;
  *m_rwndStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newRwnd << std::endl;
}

void
TrafficGenerator::NextTxSeqCallback (SequenceNumber32 oldSeq, SequenceNumber32 newSeq)
{
  *m_txSeqStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldSeq << std::endl;
  *m_txSeqStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newSeq << std::endl;
}

void
TrafficGenerator::HighestTxSeqCallback (SequenceNumber32 oldSeq, SequenceNumber32 newSeq)
{
  *m_txHighestSeqStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldSeq << std::endl;
  *m_txHighestSeqStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newSeq << std::endl;
}

//~ void
//~ TrafficGenerator::NextRxSeqCallback (SequenceNumber32 oldSeq, SequenceNumber32 newSeq)
//~ {
  //~ *m_rxSeqStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldSeq << std::endl;
  //~ *m_rxSeqStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newSeq << std::endl;
//~ }

void
TrafficGenerator::ActualWndChangeCallback (uint32_t oldWnd, uint32_t newWnd)
{
  *m_actualWndStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldWnd << std::endl;
  *m_actualWndStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newWnd << std::endl;
}

void
TrafficGenerator::ActualInFlightCallback (uint32_t oldVal, uint32_t newVal)
{
  *m_inFlightStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldVal << std::endl;
  *m_inFlightStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newVal << std::endl;
}

void
TrafficGenerator::RxMsgChangeCallback (uint32_t oldNb, uint32_t newNb)
{
  //~ NS_LOG_DEBUG ("RxMsgChangeCallback: old=" << oldNb << "  new=" << newNb); 
  *m_rxNbStream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newNb << std::endl;
}


} // namespace ns3
