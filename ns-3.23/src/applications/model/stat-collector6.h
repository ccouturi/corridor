/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2008 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 
 */
#ifndef STATCOLLECTOR6_H
#define STATCOLLECTOR6_H
#include "ns3/object.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv6-address.h"
#include "ns3/tag.h"
#include "ns3/network-module.h"
//#include "ns3/seq-id-tag.h"
//#include "ns3/seq-id-queue-tag.h"
#include "ns3/packet.h"
#include <vector>
#include <fstream>
#include "ns3/simulator.h"
#include "ns3/nstime.h"
#include <iomanip>
#include <string>

using namespace std;


namespace ns3 {


enum{ undefined = 999};

struct PacketStatistics
{
int sequenceNumber;
float txTime;
float rxTime;
float delay;
float delayVariation;
float jitter;
bool  loss;
float packetSize;
};


struct FourTuple{

Ipv6Address scrAddress;
Ipv6Address destAddress;
int scrPort;
int destPort;
};

struct PacketStatisticsQueue
{
 int sequenceNumber;
 float enqueueTime;
 float dequeueTime;
 float queuedTime;
 bool  drop;
 int currentQueueSize;
float packetSize;
};


class StatCollector6 : public Object
{
public:
  static TypeId GetTypeId (void);
  
  StatCollector6 ();

  virtual ~StatCollector6();

static void PrepareTx(Ptr<Packet> packet, Ipv6Address scrAddress, Ipv6Address destAddress , int scrPort, int destPort);
static void RecordRx(Ptr<Packet> packet);
static void ClassifyPacket(Ipv6Address scrAddress, Ipv6Address destAddress , int scrPort, int destPort);
static uint32_t AllocateFlowId (void);
static void SetDelayLossThreshold(float thresholdTime);
static void SetOuputDirectory(std::string outputDirectory);
static void EnableEndToEndStatCollection(void);
static void EnableQueueStatCollection(void);

static void OutputEndToEndStats(void);
static void OutputQueueStats(void);

private:

static float m_delayLossThreshold;
static std::string m_ouputDirectory;
static bool m_collectStats;
static bool m_collectStatsQueue;


//Tx
static vector<FourTuple>  m_flowVector;
static int m_flowId;
static int m_seqId;
static float m_txTime;
static float m_rxTime;
static float m_delay;
static float m_delayVariation;
static float m_jitter;
static bool m_reordered; 
static vector <int> m_sequenceVector; 

//Rx
static vector< vector<PacketStatistics> > m_statVector;






//Queues
public:

static uint32_t AllocateQueueId (void);
static void Enqueue(int queueId , int pHBQueueId, Ptr<Packet> packet, bool drop , int currentQueueSize);
static void Dequeue(int queueId , int pHBQueueId, Ptr<Packet> packet, int currentQueueSize);

static int m_queueId;
static int m_pHBQueueId;
static int m_seqIdQueue;
static int m_currentQueueSize;

static float m_enqueueTime;
static float m_dequeueTime;
static float m_queuedTime;
static vector< vector<  vector< PacketStatisticsQueue> >  > m_statVectorQueue;
static vector<  vector< int> >  m_sequenceVectorQueue;

static vector< vector< float> > averageQueueLengthVector;
static vector< vector< float> > lastEnqueueDequeueTime;



//Sorting

static void ShellSort(vector <float> &num);
static float Get50THPercentile(vector <float> &num);

static bool m_summaryOnly;
};

} // namespace ns3

#endif /* STATCOLLECTOR6_H */
