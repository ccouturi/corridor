
//#include "ns3/simulator.h"
#include "tcp-freeze.h"

#include "ns3/packet.h"
#include "ns3/log.h"
/*
#include "ns3/abort.h"
#include "ns3/node.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/log.h"
#include "ns3/ipv4.h"
#include "ns3/ipv6.h"
#include "ns3/ipv4-interface-address.h"
#include "ns3/ipv4-route.h"
#include "ns3/ipv6-route.h"
#include "ns3/ipv4-routing-protocol.h"
#include "ns3/ipv6-routing-protocol.h"
#include "ns3/simulation-singleton.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/double.h"
#include "ns3/trace-source-accessor.h"
#include "tcp-socket-base.h"
#include "tcp-l4-protocol.h"
#include "ipv4-end-point.h"
#include "ipv6-end-point.h"
#include "ipv6-l3-protocol.h"
#include "tcp-header.h"
#include "tcp-option-winscale.h"
#include "tcp-option-ts.h"
#include "rtt-estimator.h"
*/

NS_LOG_COMPONENT_DEFINE ("TcpFreeze");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (TcpFreeze);

TypeId
TcpFreeze::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::TcpFreeze")
    .SetParent<TcpNewReno> ()
    //~ .SetParent<TcpWestwood> ()
    .AddConstructor<TcpFreeze> ()
    .AddAttribute ("MaxAdvertisedWindowSize", "The maximum advertised window size (EXPERIMENTAL).",
                   UintegerValue (0),
                   MakeUintegerAccessor (&TcpFreeze::m_maxAdvertisedWindowSize),
                   MakeUintegerChecker<uint32_t> ())
    ;
  return tid;
}

//constructor
TcpFreeze::TcpFreeze (void) : 
  m_freezeActivated (false),
  m_firstAfterFreeze (false),
  m_freezeCommanded (false),
  m_maxAdvertisedWindowSize (0)
{
 
}

Ptr<TcpSocketBase> 
TcpFreeze::Fork (void)
{
  return CopyObject<TcpFreeze> (this);
}

void 
TcpFreeze::Freeze(void)
{
  std::cout<<"enter freeze"<< std::endl;
  NS_LOG_LOGIC ("Enter Freeze mode");
//TODO: Add a parameter to freeze indicating at what time the disconnection is planned
//      and schedule ZWA so that it is sent actually sent RTT before that time (RTT is local to the socket)
//      note: will have to handle the case of several successive call to Freeze () before the actual ZWA is sent (so have to reschedule ZWA emission)

  m_freezeActivated = true;  
  SendEmptyPacket (TcpHeader::ACK); // Send ZWA (Zero Advertisement Window) Ack
}

void 
TcpFreeze::EndFreeze(void)
{
  NS_LOG_LOGIC ("Exit Freeze ode");
  m_freezeActivated = false;
  m_firstAfterFreeze = true;

  // Send Tiplicate ACK to immediately release the sender. Otherwise, 
  // it might remain iddle until next ZWP answer is received
  SendEmptyPacket (TcpHeader::ACK);
  SendEmptyPacket (TcpHeader::ACK);
  SendEmptyPacket (TcpHeader::ACK);  
}

void 
TcpFreeze::ReceivedData (Ptr<Packet> packet, const TcpHeader& tcpHeader)
{
  TcpSocketBase::ReceivedData (packet,tcpHeader);
 
  // At Rx side: Immediately send an ack fot the first received packet
  //             This is to disable the cumulative ack scheme wich would
  //             make us wait to receive other segments befor sending the ack
  if (m_firstAfterFreeze)
    {
      SendEmptyPacket (TcpHeader::ACK);
      m_firstAfterFreeze = false;
    }
}


uint16_t 
TcpFreeze::AdvertisedWindowSize (void)
{
  if (m_freezeActivated)   // Return 0 during freeze mode
    {
      return 0;
    }
    
  if (m_maxAdvertisedWindowSize==0)
    {
      return TcpSocketBase::AdvertisedWindowSize ();  
    }
  else
    {
      return std::min (TcpSocketBase::AdvertisedWindowSize (), (uint16_t)m_maxAdvertisedWindowSize);
    }
}



//~ void
//~ TcpFreeze::ReceivedAck (Ptr<Packet> packet, const TcpHeader& tcpHeader)
//~ {
  //~ TcpSocketBase::ReceivedAck (packet,tcpHeader);     
//~ }


void TcpFreeze::ReceivedAck (Ptr<Packet> packet, const TcpHeader& tcpHeader)
{
  // It is simpler to discard received ACKs during freeze than to prevent from sending them
  // so Ignore Ack if in freeze mode, execept if advertised window !=0 (wich means end of freeze)
/*  
  uint32_t advWnd = tcpHeader.GetWindowSize ();
  
  // if not in freeze mode or if this is an unfreeze packet
  if (!m_freezeCommanded
      || (m_freezeCommanded && advWnd!=0) )
    {
      TcpSocketBase::ReceivedAck (packet, tcpHeader);
    }
  else
    {
      NS_LOG_LOGIC ("Discarding received Ack because in Freeze Mode");
    }

  // If this is a ZWA packet
  if (advWnd==0)
    {
      NS_LOG_LOGIC ("Received a freeze command from the receiver");    
      m_freezeCommanded = true;
    }
*/
TcpSocketBase::ReceivedAck (packet, tcpHeader);
}


//~ void
//~ TcpFreeze::NewAck (const SequenceNumber32& seq)
//~ {
  //~ TcpNewReno::NewAck (seq);
//~ }


int
TcpFreeze::Send (Ptr<Packet> p, uint32_t flags)
{
  if (m_freezeActivated)
    {
      NS_LOG_LOGIC ("FreezeTCP blocked Packet sending because in Freeze mode");    
      return 0;
    }
  
  return TcpSocketBase::Send (p, flags);
}

}//namespace ns3

