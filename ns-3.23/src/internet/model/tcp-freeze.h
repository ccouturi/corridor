#ifndef TCP_FREEZE_H
#define TCP_FREEZE_H


#include "tcp-socket-base.h"

//~ #include "tcp-reno.h"
#include "tcp-newreno.h"
//~ #include "tcp-westwood.h"

//~ #include "ns3/packet.h"

namespace ns3 {

//~ class TcpFreeze : public TcpReno
class TcpFreeze : public TcpNewReno
//~ class TcpFreeze : public TcpWestwood
{
public:
  static TypeId GetTypeId (void);
  TcpFreeze (void);       // Constructor
  
  void Freeze(void);      // Freeze command
  void EndFreeze(void);   // Stop Freeze command
  
  int Send (Ptr<Packet> p, uint32_t flags); // Redifined Send function to prevent receiver to transmit while in freeze mode

protected:
  bool m_freezeActivated;   // on receiver: true when in freeze mode
  bool m_firstAfterFreeze;  // memorize exit of freeze mode in order to force to ACK the first received data
  

  virtual uint16_t AdvertisedWindowSize (void); // redifined to force sending ZWA
  
  virtual Ptr<TcpSocketBase> Fork (void); // Call CopyObject<TcpFreeze>
  virtual void ReceivedAck (Ptr<Packet> packet, const TcpHeader& tcpHeader); //on the sender side: redefined to ignore ACK received when in freeze mode
  virtual void ReceivedData (Ptr<Packet> packet, const TcpHeader& tcpHeader);
  
  //~ virtual void NewAck (const SequenceNumber32& seq);

  

  //~ virtual void DupAck (const TcpHeader& t, uint32_t count);
  //virtual void Retransmit (void);

bool m_freezeCommanded;
uint32_t m_maxAdvertisedWindowSize;


};

}//Namespace NS3


#endif
