# commandes pour generer les fichiers fusionnés:
#  paste SimulationPrototype/efUdpUl.txt SimulationPrototype/af4UdpUl.txt  SimulationPrototype/af3UdpUl.txt SimulationPrototype/beUdpUl.txt > SimulationPrototype/paste_UDP_UL_QOS.txt


load '../00.styles/gnuplot-palettes/set1.pal'



# xyborder.cfg
set style line 101 lc rgb '#000000' lt 1 lw 1
set border 3 front ls 101
set tics nomirror out scale 0.75
set format '%g'

set xtics scale 2 1

set ylabel "Applicative throughput (Mb/s)"
set xlabel "Time (s)"
set y2label " "
set y2tics 
set grid xtics ytics
set key default


#~ set multiplot layout 2, 1
#~ set title "Witout QoS" font ",14"
file = "paste_UDP_UL_NO-QOS.txt"
plot  "LTEsimulation-no_zero.txt"  using ($1+0.1):2 with filledcurves x1 lc rgb "gray" title "Available BW"
      
#~ unset multiplot      


