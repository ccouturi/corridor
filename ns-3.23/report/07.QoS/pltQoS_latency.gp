# commandes pour generer les fichiers fusionnés:
#  paste SimulationPrototype/efUdpUl.txt SimulationPrototype/af4UdpUl.txt  SimulationPrototype/af3UdpUl.txt SimulationPrototype/beUdpUl.txt > SimulationPrototype/paste_UDP_UL_QOS.txt


load '../00.styles/gnuplot-palettes/set1.pal'

set datafile missing '0'

line_width=2

# xyborder.cfg
set style line 101 lc rgb '#000000' lt 1 lw 1
set border 3 front ls 101
set tics nomirror out scale 0.75
set format '%g'

set xtics scale 2 1

#~ set title "Goodput"
set ylabel "Applicative throughput (Mb/s)"
set xlabel "Time (s)"
set grid xtics ytics
set key default


set multiplot layout 2, 1


set title "Without QoS" font ",14"      
set ylabel "Latency (s)"     
#~ set y2label "BE flows latency (s)"
#~ set y2tics 
set grid xtics ytics 
#~ set yrange [0:0.6]
set xrange [0:18]


availableBw_file="LTEsimulation-no_zero.txt"
file = "paste_UDP_UL_NO-QOS.txt"

plot  availableBw_file  using ($1+0.1):2 with filledcurves x1 lc rgb 0xDCDCDC title "Available BW" axes x1y2,\
      file   using 1:7  with points ls 1 lw line_width title "EF UDP", \
      file  using 1:14  with points ls 2 lw line_width title "AF2 UDP", \
      file  using 1:21  with points ls 3 lw line_width title "AF1 UDP", \
      file   using 1:28  with points ls 4 lw line_width title "BE UDP" axes x1y1      

#~ set y2label " "
file = "paste_UDP_UL_QOS-tst.txt"
set title "With QoS" font ",14"      
plot  availableBw_file  using ($1+0.1):2 with filledcurves x1 lc rgb 0xDCDCDC title "Available BW" axes x1y2,\
      file   using 1:7  with points ls 1 lw line_width title "EF UDP", \
      file  using 1:14  with points ls 2 lw line_width title "AF4 UDP", \
      file  using 1:21  with points ls 3 lw line_width title "AF3 UDP", \
      file   using 1:28  with points ls 4 lw line_width title "BE UDP" axes x1y1     


unset multiplot      


