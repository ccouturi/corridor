
reset

input_file = 'pltQoS.gp'
output_file = 'QoS-UDP.png'

input_file2 = 'pltQoS_latency.gp'
output_file2 = 'QoS-UDP_latency.png'

input_file3 = 'pltQoS_availableBW.gp'
output_file3 = 'QoS_availableBW.png'

input_file4 = 'pltQoS-TCP.gp'
output_file4 = 'QoS-TCP.png'


png_sizeX = 1600
#~ png_sizeX = 1900
png_sizeY = 1000


########################################################################
# Figure 1
reset

## Trace à l'écran
load input_file

## trace dans fichier PNG
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file

load input_file

set term pop
########################################################################





########################################################################
# Figure 2
reset

## Trace à l'écran
load input_file2

## trace dans fichier PNG
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file2

load input_file2

set term pop
########################################################################




########################################################################
# Figure 3
reset
png_sizeX = 1600
png_sizeY = 300


## Trace à l'écran
load input_file3

## trace dans fichier PNG
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file3

load input_file3

set term pop
########################################################################




########################################################################
# Figure 4
reset
png_sizeX = 1600
png_sizeY = 1000


## Trace à l'écran
load input_file4

## trace dans fichier PNG
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file4

load input_file4

set term pop
########################################################################

