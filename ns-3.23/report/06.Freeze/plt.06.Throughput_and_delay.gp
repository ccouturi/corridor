# notes:
# cat out.txt | grep  "node 4" | grep  "CC: Received seq =" |awk '{print $2 " " $10}' > RxSeq.txt 
#  cat out.txt | grep  "node 1"  | grep  "CC:: ReceivedAck #" |awk '{print $2 " " $9}' > RxAck.txt
#  cat out.txt | grep  "node 1"  | grep  "CC:SeqSent=" |awk '{print $2 " " $7}' > TxSent.txt
#

# cp tcpDl.txt report/06.Freeze/tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_1--MaxWndSize_0--start_0.txt 
# cp throughput.dat report/06.Freeze/throughput--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_1--MaxWndSize_0--start_0.txt

reset

########################################################################
# Configuration
set termopt enhanced    # turn on enhanced text mode (for subscript)

# Load colormap and styles
load '../00.styles/gnuplot-palettes/set1.pal'
load '../00.styles/gnuplot-configs/xyborder.cfg'
#~ load 'report/00.styles/gnuplot-palettes/set1.pal'
#~ load 'report/00.styles/gnuplot-configs/xyborder.cfg'



########################################################################
# Input parameters
if (!exists("freeze")) freeze=0
if (!exists("wndSize")) wndSize=0

########################################################################
# fichiers d'entree et remplacement des - par des "0"
#set datafile missing '-'
filter = 'sed -e "s/-/0/g" '

tcpDl1='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_0--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt"
throughput1='< '.filter."throughput--pwr_30--fad_0--atten_3.5--tunnel_0--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt"
file1_title='Without interruption'

tcpDl2='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
throughput2='< '.filter."throughput--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
file2_title='With interruption'

#~ file3='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_1--MaxWndSize_0--start_0.txt"
#~ file3_title='FreezeTCP'
#~ 
#~ file4='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_1--MaxWndSize_3000--start_0.txt"
#~ file4_title='FreezeTCP + window constraint (3000)'



########################################################################
# Trace
set multiplot layout 2, 1 #title "TCP and UDP goodput" font ",14"


## Rectangles représentant les pertes
set style rect fc lt -1 fs solid 0.15 noborder
set obj rect from 7.9, graph 0 to 8.1, graph 1

## grid
set xtics scale 2 1
set mxtics 2
set grid xtics ytics mxtics
show grid

set xrange [0:12]
set xlabel "Time (s)" font ",12"

## key
set key font ",12"

## Trace 1: Goodput for Rx application
set title "Throughput at Tx application layer" font ",14"
set ylabel "Throughput (Mb/s)" font ",12"


plot throughput1 using 1:2 with lines lw 2 title file1_title axes x1y1,\
     throughput2 using 1:2 with lines lw 2 title file2_title axes x1y1,\
#~ 
#~ plot tcpDl1 using 1:2 with lines lw 2 title file1_title axes x1y1,\
     #~ tcpDl2 using 1:2 with lines lw 2 title file2_title axes x1y1,\

#~ plot file1 using 1:2 with lines lw 2 title file1_title axes x1y1,\
     #~ file2 using 1:2 with lines lw 2 title file2_title axes x1y1,\
     
     #~ file3 using 1:2 with lines lw 2 title file3_title axes x1y1,\
     #~ file4 using 1:2 with lines lw 2 title file4_title axes x1y1,\
     #~ 'cwnd_test.dat' using 1:2 with lines title 'Congestion Window' axes x1y2,\
     #~ 'ssTresh.dat' using 1:2 with lines title 'Slow Start Threshold' axes x1y2,\
     #~ 'RWND.txt' using 1:2 with points  title 'RWND' axes x1y2
     #~ 'inFlight.txt' using 1:2 with lines lt rgb 'blue' title 'inFlight' axes x1y2,\
     #~ 'ActualWnd.txt' using 1:2 with lines lt rgb 'pink' title 'ActualWnd' axes x1y2


## Trace 2:  Delay actually seen by sink
set title "Transmission Delay" font ",14"
set ylabel "Delay (s)" font ",12"

plot tcpDl1 using 1:7 with lines lw 2 title file2_title axes x1y1,\
     tcpDl2 using 1:7 with lines lw 2 title file2_title axes x1y1,\
     #~ file3 using 1:7 with lines lw 2 title file3_title axes x1y1,\
     #~ file4 using 1:7 with lines lw 2 title file4_title axes x1y1,\

#~ plot 'tcpDl.txt' using 1:7 with points title 'tcpDl delay',\
     #~ 'RxAck.txt' using 1:2 with points title 'Rx Ack' axes x1y2,\
     #~ 'TxSent.txt' using 1:2 with points title 'TxSeq' axes x1y2,\
     #~ 'RxSeq.txt' using 1:2 with points title 'RxSeq' axes x1y2
     

unset multiplot
