
reset


png_sizeX = 2000
png_sizeY = 1000

########################################################################
#-------------------------- SANS FREEZE --------------------------------
freeze= 0
wndSize=0

########################################################################
# Trace 1
input_file = 'plt.06.Throughput_and_delay.gp'
output_file = 'Throughput_and_delay--Reference.png'

load 'plotToScreenAndPng.gp'



########################################################################
# Trace 2
input_file = 'plt.06.Cwnd_and_seq.gp'
output_file = 'Cwnd_and_seq--Reference.png'

load 'plotToScreenAndPng.gp'




########################################################################
#-------------------------- AVEC FREEZE --------------------------------
freeze= 1
wndSize=0

########################################################################
# Trace 3
input_file = 'plt.06.Throughput_and_delay.gp'
output_file = 'Throughput_and_delay--Freeze.png'

load 'plotToScreenAndPng.gp'



########################################################################
# Trace 4
input_file = 'plt.06.Cwnd_and_seq.gp'
output_file = 'Cwnd_and_seq--Freeze.png'

load 'plotToScreenAndPng.gp'



########################################################################
#------------------- AVEC FREEZE et WND=3000 ---------------------------
freeze= 1
wndSize=3000

########################################################################
# Trace 5
input_file = 'plt.06.Throughput_and_delay.gp'
output_file = 'Throughput_and_delay--Freeze_and_wnd_3000.png'

load 'plotToScreenAndPng.gp'


########################################################################
# Trace 6
input_file = 'plt.06.Cwnd_and_seq.gp'
output_file = 'Cwnd_and_seq--Freeze_and_wnd_3000.png'

load 'plotToScreenAndPng.gp'



