# notes:
#  cat out.txt | grep  "node 4" | grep  "CC: Received seq =" |awk '{print $2 " " $10}' > RxSeq.txt 
#  cat out.txt | grep  "node 1"  | grep  "CC:: ReceivedAck #" |awk '{print $2 " " $9}' > RxAck.txt
#  cat out.txt | grep  "node 1"  | grep  "CC:SeqSent=" |awk '{print $2 " " $7}' > TxSent.txt
# cp RxSeq.txt    report/06.Freeze/RxSeq--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt 
# cp RxAck.txt    report/06.Freeze/RxAck--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt #
# cp TxSent.txt    report/06.Freeze/TxSent--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt 
# cp cwnd_test.dat report/06.Freeze/cwnd--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt 
# cp ssTresh.dat   report/06.Freeze/ssTresh--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt 
# cp cwnd_test.dat report/06.Freeze/CWND--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_0--MaxWndSize_0--start_0.txt 

# cp tcpDl.txt report/06.Freeze/tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_1--MaxWndSize_0--start_0.txt 
# cp throughput.dat report/06.Freeze/throughput--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_1--MaxWndSize_0--start_0.txt

reset

########################################################################
# Input parameters
if (!exists("freeze")) freeze=0
if (!exists("wndSize")) wndSize=0

########################################################################
# Configuration
set termopt enhanced    # turn on enhanced text mode (for subscript)

# Load colormap and styles
load '../00.styles/gnuplot-palettes/set1.pal'
load '../00.styles/gnuplot-configs/xyborder.cfg'
#~ load 'report/00.styles/gnuplot-palettes/set1.pal'
#~ load 'report/00.styles/gnuplot-configs/xyborder.cfg'


########################################################################
# fichiers d'entree et remplacement des - par des "0"
#set datafile missing '-'
filter = 'sed -e "s/-/0/g" '


tcpDl='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
throughput='< '.filter."throughput--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
RxAck='< '.filter."RxAck--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
TxSent='< '.filter."TxSent--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
cwnd='< '.filter."cwnd--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
ssTresh='< '.filter."ssTresh--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"



########################################################################
# Trace
set multiplot layout 2, 1 #title "TCP and UDP goodput" font ",14"


## Rectangles représentant les pertes
set style rect fc lt -1 fs solid 0.15 noborder
set obj rect from 7.9, graph 0 to 8.1, graph 1

## grid
set xtics scale 2 1
set mxtics 2
set grid xtics ytics mxtics
show grid

set xrange [7.5:11.5]
set xlabel "Time (s)" font ",12"
set y2tics 

## key
set key font ",12"

## Trace 1: 

freeze=0
#~ freeze=1
wndSize=0
tcpDl='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
throughput='< '.filter."throughput--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
RxAck='< '.filter."RxAck--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
TxSent='< '.filter."TxSent--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
cwnd='< '.filter."cwnd--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
ssTresh='< '.filter."ssTresh--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"


set title "Without Freeze" font ",14"
set ylabel "Contention window size (bytes)" font ",12"
set y2label "Sequence numbers" font ",12"
plot cwnd   using 1:2 with lines lw 2 title 'Contention Window' axes x1y1,\
     RxAck  using 1:2 with points pt 3 title 'Rx Ack' axes x1y2,\
     TxSent using 1:2 with points pt 2 title 'TxSeq' axes x1y2,\
     
     
## Trace 2: 

#~ freeze=0
freeze=1
wndSize=3000
tcpDl='< '.filter."tcpDl--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
throughput='< '.filter."throughput--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
RxAck='< '.filter."RxAck--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
TxSent='< '.filter."TxSent--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
cwnd='< '.filter."cwnd--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"
ssTresh='< '.filter."ssTresh--pwr_30--fad_0--atten_3.5--tunnel_1--speed_0--dist_1000--freeze_".freeze."--MaxWndSize_".wndSize."--start_0.txt"


set title "With Freeze" font ",14"
set ylabel "Contention window size (bytes)" font ",12"
set y2label "Sequence numbers" font ",12"
plot cwnd   using 1:2 with lines lw 2 title 'Contention Window' axes x1y1,\
     RxAck  using 1:2 with points pt 3 title 'Rx Ack' axes x1y2,\
     TxSent using 1:2 with points pt 2 title 'Tx Seq number' axes x1y2,\
     
     
     

unset multiplot
