
########################################################################
# Inputs
########################################################################
# input_file: Gnuplot script
# output_file: PNG file to save

if (!exists("png_sizeX")) freeze=2000
if (!exists("png_sizeY")) png_sizeY=1000


########################################################################
# Trace

# Trace to screen
load input_file

# trace dans fichier PNG
set term push
set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file

load input_file

set term pop
