
reset

input_file = 'plt.03.ThroughputReference.gp'
output_file = 'ThroughputReferenceWithWithoutFading.png';
png_sizeX = 1600
png_sizeY = 1000

########################################################################
# Trace à l'écran
load input_file


########################################################################
# trace dans fichier PNG
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file


load input_file

set term pop
########################################################################



