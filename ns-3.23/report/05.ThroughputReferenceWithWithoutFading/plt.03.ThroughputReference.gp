reset

########################################################################
# Configuration
set termopt enhanced    # turn on enhanced text mode (for subscript)

# Load colormap and styles
load '../00.styles/gnuplot-palettes/set1.pal'
load '../00.styles/gnuplot-configs/xyborder.cfg'


########################################################################
# fichiers d'entree et remplacement des - par des "0"
#set datafile missing '-'
filter = 'sed -e "s/-/0/g" '

UDP_DL_with_fading='< '.filter.'udpDl_withFading.txt'
UDP_DL_without_fading='< '.filter.'udpDl_withoutFading.txt'



########################################################################
# Trace

#set title "TCP and UDP goodput" font ",14"
set multiplot layout 3, 1 


#RSRP

set ylabel "RSRP (dBm)"
set xlabel "Distance to eNB_{1} (m)"
set xrange [-250:1500]
set xtics 100
set mxtics 2
set grid xtics ytics mxtics 
set key default


set title "RSRP" font ",14"
plot "DlRsrpSinrStats_withFading.txt" using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines title 'RSRP (with fading)', \
     "DlRsrpSinrStats_withoutFading.txt" using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines title 'Ref RSRP (without fading)'
     #~ "../../output/ref/DlRsrpSinrStats.fading_none-TxPwr_30-path_loss_3.5.txt" using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines title 'Ref RSRP (witout fading)'


#plot Goodput actually seen by sink
set title "Maximum througput"  font ",14"
set ylabel "Throughput (Mb/s)"
#set y2tics 
#set xtics scale 2 1

set yrange [0:4.5]
set grid xtics ytics
#~ plot 'udpDl_withFading.txt' using ((($1)-2.5)*100):2 with lines title 'UDP DL (with fading)' axes x1y1,\
     #~ 'udpDl_withoutFading.txt' using ((($1)-2.5)*100):2 with lines  title 'UDP DL (without fading)'axes x1y1
plot UDP_DL_with_fading using ((($1)-2.5)*100):2 with lines title 'UDP DL (with fading)' axes x1y1,\
     UDP_DL_without_fading using ((($1)-2.5)*100):2 with lines  title 'UDP DL (without fading)'axes x1y1
set autoscale y


#plot Dealy actually seen by sink
set title "End to end delay"  font ",14"
set ylabel "Delay (s)"
set grid xtics ytics
plot UDP_DL_with_fading using ((($1)-2.5)*100):7 with lines title 'UDP DL delay (with fading)',\
     UDP_DL_without_fading using ((($1)-2.5)*100):7 with lines title 'UDP DL delay (without fading)'


unset multiplot
