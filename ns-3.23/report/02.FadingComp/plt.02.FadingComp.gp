reset

########################################################################
# Configuration
set termopt enhanced    # turn on enhanced text mode (for subscript)

# Load colormap and styles
load '../00.styles/gnuplot-palettes/set1.pal'
load '../00.styles/gnuplot-configs/xyborder.cfg'


src_file = "paste_EPA3kmh.txt"
title_str = "EPA_3kmh";


########################################################################
# Trace

#set title "Comparison of fading channels" font ",14"

set multiplot layout 2, 1 

set xrange [0:1]

set yrange [-15:15]
set ylabel "RSRP variation (dBm)"
set xlabel "Time (s)"
set grid xtics ytics
set key default

#~ src_file = "paste_EPA3kmh.txt"
#~ title_str = "Extended Pedestrian A (EPA)3kmh"
#~ plot src_file using 1:(10*log10(($5)*1000))-(10*log10(($11)*1000))  with lines title title_str
#~ 
#~ src_file = "paste_EPA300kmh-2160MHz.txt"
#~ title_str = "Extended Pedestrian A (EPA) 300kmh"
#~ plot src_file using 1:(10*log10(($5)*1000))-(10*log10(($11)*1000))  with lines title title_str


#~ src_file = "paste_ETU3kmh.txt"
#~ title_str = "Extended Typical Urban model (ETU) 3kmh"
#~ plot src_file using 1:(10*log10(($5)*1000))-(10*log10(($11)*1000))  with lines title title_str
#~ 
#~ src_file = "paste_ETU300kmh-2160MHz.txt"
#~ title_str = "Extended Typical Urban model (ETU) 300kmh"
#~ plot src_file using 1:(10*log10(($5)*1000))-(10*log10(($11)*1000))  with lines title title_str

 
src_file = "paste_EVA60kmh.txt"
title_str = "Extended Vehicular A (EVA) 60kmh"
plot src_file using 1:(10*log10(($5)*1000))-(10*log10(($11)*1000))  with lines title title_str

src_file = "paste_EVA300kmh-2160MHz.txt"
title_str = "Extended Vehicular A (EVA) 300kmh"
plot src_file using 1:(10*log10(($5)*1000))-(10*log10(($11)*1000))  with lines title title_str

unset multiplot

