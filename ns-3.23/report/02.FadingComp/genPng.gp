
reset

input_file = 'plt.02.FadingComp.gp'
output_file = 'fadingComparison.png';
png_sizeX = 1200
png_sizeY = 800

########################################################################
# Trace à l'écran
load input_file


########################################################################
# trace dans fichier PNG
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file


load input_file

set term pop
########################################################################



