reset

########################################################################
# Configuration
set termopt enhanced    # turn on enhanced text mode (for subscript)

# Load colormap and styles
load '../00.styles/gnuplot-palettes/set1.pal'
load '../00.styles/gnuplot-configs/xyborder.cfg'



########################################################################
# Trace

#set title "TCP and UDP goodput" font ",14"
set multiplot layout 3, 1 


#RSRP

set ylabel "RSRP (dBm)"
set xlabel "Distance to eNB_{1} (m)"
set xrange [-250:1500]
set xtics 100
set mxtics 2
set grid xtics ytics mxtics 
set key default


set title "RSRP" font ",14"
plot "DlRsrpSinrStats.txt" using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines title 'RSRP', \
     "../../output/ref/DlRsrpSinrStats.fading_none-TxPwr_30-path_loss_3.5.txt" using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines title 'Ref RSRP (witout fading)'


#plot Goodput actually seen by sink
set title "Maximum througput"  font ",14"
set ylabel "Throughput (Mb/s)"
#set y2tics 
#set xtics scale 2 1

set yrange [0:4.5]
set grid xtics ytics
plot 'udpDl.txt' using ((($1)-2.5)*100):2 with lines title 'UDP DL' axes x1y1,\
     'udpUl.txt' using ((($1)-2.5)*100):2 with lines  title 'UDP UL'axes x1y1
unset yrange


#plot Dealy actually seen by sink
set title "End to end delay"  font ",14"
set ylabel "Delay (s)"
set grid xtics ytics
plot 'udpDl.txt' using ((($1)-2.5)*100):7 with lines title 'UDP DL delay',\
     'udpUl.txt' using ((($1)-2.5)*100):7 with lines title 'UDP UL delay'



unset multiplot
