
reset

output_file = 'RSRP.png'
png_sizeX = 1200
png_sizeY = 600


########################################################################
# Configuration
set termopt enhanced    # turn on enhanced text mode (for subscript)

# Load colormap and styles
load '../00.styles/gnuplot-palettes/set1.pal'
load '../00.styles/gnuplot-configs/xyborder.cfg'


########################################################################
# RSRP
#set title "Friis Propagation model"
set ylabel "RSRP (dBm)"
set xlabel "Distance to eNB_{1} (m)"
set grid xtics ytics

set xrange [-250:1600]

set key default
plot "DlRsrpSinrStats.fading_none-TxPwr_30-path_loss_2.txt"   using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines lc 1 title 'atten: 2  ', \
     "DlRsrpSinrStats.fading_none-TxPwr_30-path_loss_3.txt"   using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines lc 2 title 'atten: 3  ', \
     "DlRsrpSinrStats.fading_none-TxPwr_30-path_loss_3.5.txt" using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines lc 3 title 'atten: 3.5', \
     "DlRsrpSinrStats.fading_none-TxPwr_30-path_loss_4.txt"   using ((($1)-2.5)*100):(10*log10(($5)*1000))  with lines lc 4 title 'atten: 4  '
     

########################################################################
# Enregistrement dans le fichier
set term push

set terminal png size png_sizeX, png_sizeY enhanced font "Helvetica"
set termopt enhanced    # turn on enhanced text mode (for subscript)
set output output_file
replot

set term pop
########################################################################



