/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-generator.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TryTcpGenerator");


void stopTcp (Ptr<TrafficGenerator> app)
{
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tFreezing TCP" << std::endl; 
    p->Freeze();
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
  
}
void restoreTcp (Ptr<TrafficGenerator> app)
{
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tUnFreezing TCP" << std::endl; 
    p->EndFreeze();
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
}




static void CwndChange (Ptr<OutputStreamWrapper> stream, uint32_t oldCwnd, uint32_t newCwnd)
{
  
  //std::cout << Simulator::Now().GetSeconds() << "\t" << newCwnd << std::endl;
  
  *stream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newCwnd << std::endl;//stream = the oject pointed by stream(in this case it's a pointer too !!!!!!!!!)
}

void GetSocket (Ptr<PacketSink> ps)

{
std::cout << "entree dans GetSocket"<< std::endl;
Ptr< Socket > soc= ps->GetListeningSocket();
Ptr< TcpFreeze > tf= soc->GetObject <TcpFreeze>();
std::cout << "Runtime Socket\t"<< soc<<"Tcp Freeze socket\t"<< tf << std::endl;
tf->Freeze();
}

/*void test (Ptr<PacketSink> ps)

{
std::cout << "entree dans test"<< std::endl;
Ptr< Socket > soc= ps->GetListeningSocket();
Ptr< TcpFreeze > tf= soc->GetObject <TcpFreeze>();
std::cout<< "f_wnd\t"<< tf->f_wnd<< std::endl;
}*/


void FreezeTcp (Ptr<TcpFreeze> p)

{
std::cout << "entree dans freeze tcp"<< p << std::endl;
p-> Freeze();
}


void StopFreezeTcp (Ptr<TcpFreeze> p)

{
std::cout << "Unfreez"<< p << std::endl;
p-> EndFreeze();
}

int main (int argc, char *argv[])
{
  //~ LogLevel logLevel = (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LEVEL_ALL);
  //~ LogComponentEnable ("TcpSocketBase", logLevel);
  //~ LogComponentEnable ("TcpReno", logLevel);
  
  Time::SetResolution (Time::NS);

  uint16_t sinkPort = 8000;
//************************************************************cmd variables*****************************************************************//
  CommandLine cmd;
  cmd.AddValue ("Port", "Sink port", sinkPort);
  cmd.Parse (argc, argv);

//*****************************************************Point-To-Point model with IP Conf*****************************************************//
  Config::SetDefault("ns3::TcpL4Protocol::SocketType", StringValue ("ns3::TcpFreeze"));//to use TcpFreeze as the Tcp type/TypeIdValue (TcpReno::GetTypeId())
  
  NodeContainer nodes;
  nodes.Create (2);

  

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  NetDeviceContainer devices;
  devices = pointToPoint.Install (nodes);

  InternetStackHelper stack;
  stack.Install (nodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer ipInterfaces = address.Assign (devices);

//****************************************************Installing TCP sink on node 1**********************************************************//

/*  
  Address sinkAddress (InetSocketAddress (ipInterfaces.GetAddress(0), sinkPort)); //saving the sink address to use later
  PacketSinkHelper sink ("ns3::TcpSocketFactory", sinkAddress);//determines which socket factory to use by the sinkhelper
  ApplicationContainer sinkApps = sink.Install (nodes.Get(0));//install the PacketSink on the Nodes 0
  sinkApps.Start (Seconds (0.));
  sinkApps.Stop (Seconds (10.));
  
  Ptr<PacketSink> ps = sinkApps.Get(0)->GetObject<PacketSink>(); 
  std::cout<< "Packet Sink\t"<< ps<<  std::endl;
  Simulator::Schedule (Seconds (3),&GetSocket,ps);
  //Simulator::Schedule (Seconds (4),&test,ps);
  //Simulator::Schedule (Seconds (5),&test,ps);
*/
  /*TypeId type = TypeId::LookupByName ("ns3::TcpSocketFactory");
  Ptr<Socket> recvSink = Socket::CreateSocket (nodes.Get(0), TcpSocketFactory::GetTypeId ());  // node 0, receiver
  InetSocketAddress sinkAddress = InetSocketAddress (ipInterfaces.GetAddress(0), sinkPort);
  recvSink->Bind (sinkAddress);
  recvSink->Listen();*/
  


//************************************************TCP Traffic Generator using SimpleSource **************************************************//
/*
  //TypeId tid = TypeId::LookupByName ("ns3::TcpFreeze");
  Ptr<Socket> ns3TcpSocket = Socket::CreateSocket(nodes.Get(1),TcpSocketFactory::GetTypeId ());//create TCP socket and install it on node 1
  Ptr<SimpleSource> app = CreateObject<SimpleSource> ();//Create an app of type simple source
  app->Setup(ns3TcpSocket, sinkAddress, 1040, 1000000, DataRate ("10Mbps"));//setting the sending application and the address of the sink
  nodes.Get(1)->AddApplication (app);//installing the TCP application on Node 1
  app->SetStartTime (Seconds (1.));
  app->SetStopTime (Seconds (9.));
*/

  TrafficGenerator tcpApp;
  tcpApp.Setup ("ns3::TcpSocketFactory", nodes.Get(1), nodes.Get(0), ipInterfaces.GetAddress(0), sinkPort,
                       300, DataRate("1Mbps")); 
  tcpApp.SetStartTime (Seconds (1.));
  tcpApp.SetStopTime (Seconds (9.));

  tcpApp.RecordTraffic ( "tcpDl.txt", MilliSeconds(100) );

  //~ Simulator::Schedule ( Seconds(5.), &stopTcp, &tcpApp);
  //~ Simulator::Schedule ( Seconds(6.), &restoreTcp, &tcpApp);



//********************************************************Traces and Pcaps***************************************************************//
  //~ AsciiTraceHelper ascii;
  //~ pointToPoint.EnableAsciiAll (ascii.CreateFileStream ("Generator.tr"));//***

  pointToPoint.EnablePcapAll("Tcp-Freeze-test");//0=source/1=client generating Pcaps

   
 /* 
  AsciiTraceHelper asciiTraceHelper;
    Ptr<OutputStreamWrapper> stream = asciiTraceHelper.CreateFileStream("TCP_CWND_test.dat");
    ns3TcpSocket->TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, stream));
*/


//***************************************************************************************************************************************//
 
 
 //Ptr<TcpFreeze> tf3 = (Ptr<TcpFreeze>) (ns3TcpSocket);
 //Ptr<TcpFreeze> tf = (TcpFreeze*) (&sink);

 //Ptr<TcpNewReno> nr =  ns3TcpSocket->GetObject<TcpNewReno>();


 //Ptr<PacketSink> ttt =  sinkApps.Get(0)->GetObject<PacketSink>();
 //Ptr<TcpFreeze> tf = ns3TcpSocket->GetObject<TcpFreeze>();
 
 //std::cout<< "socket:\t"<<recvSink <<"\tsocket:\t"<<tf << std::endl;
 //std::cout<< "Socket\t"<< ns3TcpSocket << "\t entree dans New Reno tcp:\t"<< nr << "\tTcp Freeze:\t" << tf << "\t"<<  std::endl;
 //Simulator::Schedule (Seconds (3),&FreezeTcp,tf);
 //Simulator::Schedule (Seconds (4),&StopFreezeTcp,tf);




  Simulator::Stop(Seconds(11));
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
