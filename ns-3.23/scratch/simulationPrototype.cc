/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Melina Marangos <melina.marangou@telecom-bretagne.eu>
 */

// Network topology
// //
// //
// //  +------------+ 
// //  |   Host 0   |--|
// //  [------------]  |
// //                  |  +------------+     +------------+
// //                  +--|            |     |            |    +------------+     
// //                     |  Router 1  |-----|  Router 2  |----|   Host 2   |
// //                  +--|            |     |            |    [------------]  
// //                  |  [------------]     [------------]
// //  +------------+  |
// //  |   Host 1   |--|
// //  [------------]
// // - n0 - R1 subnet (2001:1::/64);
// // - n1 - R1 subnet (2001:2::/64);
// // - R1 - R2 subnet (2001:3::/64);
// // - R1 - n2 subnet (2001:4::/64);
// //

#include <fstream>
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/radvd.h"
#include "ns3/radvd-interface.h"
#include "ns3/radvd-prefix.h"

#include "ns3/flow-monitor-module.h"
#include "ns3/netanim-module.h"
#include "ns3/gnuplot.h"
#include "ns3/object-factory.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <list>

#include <iostream>
#include <vector>
#include <string>
#include <boost/format.hpp>

#include "ns3/delay-jitter-estimation.h" // for TraficGenerator

using namespace std;
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("simulationPrototype");


void ChangeAF3Params ( vector< Ptr<DiffServFlow> > myFlowVector )
{
 
  enum{drop = 256 };
  enum{ AF11 = 40, AF12 = 48, AF13 = 56, AF21 = 72, AF22 = 80, AF23 = 88, AF31 = 104, AF32 = 112, AF33 = 120, AF41 = 136, AF42 = 144, AF43 = 152, EF = 184, BE = 0};
  
std::cout << "----------------> trying to change AF3 parameters\n";
  
  ConformanceSpec cSpec3;
  cSpec3.initialCodePoint = AF31;
//Modif CC
  //~ cSpec3.nonConformantActionI = AF32;
  //~ cSpec3.nonConformantActionII = AF33;
cSpec3.nonConformantActionI = drop;
cSpec3.nonConformantActionII = drop;
// END Modif CC

  MeterSpec mSpec3;  // -> AF3
  mSpec3.meterID = "TRTCM"; 
  mSpec3.cIR = 1000000;
  mSpec3.cBS = 2000;
  mSpec3.eBS = 1500;
  //~ mSpec3.pIR = 2500000;
mSpec3.pIR = 256000; // flow rate is 512kb/s => limiting at only 256kb/s
  mSpec3.pBS = 1500;


 
  for (std::vector< Ptr<DiffServFlow> >::iterator it = myFlowVector.begin() ; it != myFlowVector.end(); ++it)
    {
      if ( (*it)->GetFlowId ()==3 )
      {
          Ptr<DiffServSla> sla3Ptr = CreateObject<DiffServSla>(003,cSpec3,mSpec3);
          (*it)->SetSla(sla3Ptr);
      }
    }
}

void setMaxTcpWndSize (Ptr<TrafficGenerator> app, uint32_t size)
{
  std::cout <<  "-------------ENTERING setWndSize; app= " << app << " sinkSock=" << app->GetSinkConnectedSocket (0) << std::endl; 
  
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  std::cout <<  "-------------p= " << p << std::endl; 
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tSetting TCP maxWndSize: " << size << std::endl; 
    p->SetAttribute ("MaxAdvertisedWindowSize", UintegerValue (size));  
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
  
}



void stopTcp (Ptr<TrafficGenerator> app)
{
  std::cout <<  "-------------ENTERING STOP; app= " << app << " sinkSock=" << app->GetSinkConnectedSocket (0) << std::endl; 
  
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  std::cout <<  "-------------p= " << p << std::endl; 
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tFreezing TCP" << std::endl; 
    p->Freeze();
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
  
}
void restoreTcp (Ptr<TrafficGenerator> app)
{
  std::cout <<  "-------------ENTERING restoreTcp; app= " << app << std::endl; 
  
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tUnFreezing TCP" << std::endl; 
    p->EndFreeze();
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
}




struct inputData
{
  double timeInterval;
  double throughput;
  double avgPacketDelay;
};


std::list<struct inputData> InputDataFromFile(const char* filePath);
void ChangeDataRate (Ptr<PointToPointNetDevice> net_dev_r1, Time samplePeriod, std::list<struct inputData> dataList, Ptr<DiffServQueue> q1);
void ModifyQueueWeights(int weight_AF1 , int weight_AF2 , int weight_AF3 , int weight_AF4 , int weight_BE , Ptr<DiffServQueue> q1); 
void ModifyQueueSize(int size_AF1 , int size_AF2 , int size_AF3 , int size_AF4 , int size_EF , int size_BE , Ptr<DiffServQueue> q1);
void ModifyQueueSize(int size, string queue, Ptr<DiffServQueue> q1);  
void ModifyConformantAction(int level, Ptr<DiffServQueue> q1);
std::string Protocol2String (unsigned int protocol);
void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Time samplePeriod , Ptr<OutputStreamWrapper> stream, Ptr<OutputStreamWrapper> stream_loss);

bool modifyPolicy = false;
bool modifyQueueSize = false;
bool modifyQueueWeights = false;

int main (int argc, char** argv)
{
  //StatCollector6::SetDelayLossThreshold(0.001);
  //StatCollector6::EnableEndToEndStatCollection();
  //StatCollector6::EnableQueueStatCollection();

  enum{drop = 256 };

  enum{ AF11 = 40, AF12 = 48, AF13 = 56, AF21 = 72, AF22 = 80, AF23 = 88, AF31 = 104, AF32 = 112, AF33 = 120, AF41 = 136, AF42 = 144, AF43 = 152, EF = 184, BE = 0};
  bool verbose = false;
  bool qos = true;
  bool degradation=true;
  double simTime = 20;
  int samplePeriod = 100;
  int channelDataRate=10000000; //10Mbps
  int channelDelay=4; //ms
  
  bool freeze = false;
  uint32_t maxAdvertisedWindowSize = 0;


  CommandLine cmd;
  cmd.AddValue ("verbose", "turn on log components", verbose);
  cmd.AddValue ("simtime", "set simulation time", simTime);
  cmd.AddValue ("qos", "turn off QoS", qos);
  cmd.AddValue ("channelDataRate", "modily data rate in bps", channelDataRate);
  cmd.AddValue ("channelDelay", "modify channel delay in ms", channelDelay);
  cmd.AddValue ("degradation", "fluctuations in channel delay in ms", degradation);
  cmd.AddValue ("samplePeriod", "set the samplePeriod", samplePeriod);
  cmd.AddValue ("modifyPolicy", "modify the qos policies at runtime", modifyPolicy);
  cmd.AddValue ("modifyQueueSize", "modify the queue sizes", modifyQueueSize);
  cmd.AddValue ("modifyQueueWeights", "modify the queue weights", modifyQueueWeights);
  cmd.AddValue("freeze", "activate FreezeTCP (default=0 => non activated)", freeze);
  cmd.AddValue("maxAdvertisedWindowSize", "maximum advertised window size for FreezeTCP socket (default=0 => non constraint)", maxAdvertisedWindowSize);

  cmd.Parse (argc, argv);

  if (verbose)
  {
    //LogComponentEnable ("Ipv6L3Protocol", LOG_LEVEL_INFO);
    //LogComponentEnable ("Ipv6RawSocketImpl", LOG_LEVEL_ALL);
    //LogComponentEnable ("Icmpv6L4Protocol", LOG_LEVEL_INFO);
    //LogComponentEnable ("Ipv6StaticRouting", LOG_LEVEL_ALL);
    //LogComponentEnable ("Ipv6Interface", LOG_LEVEL_INFO);
    //LogComponentEnable ("RadvdApplication", LOG_LEVEL_ALL);
    // LogComponentEnable ("Ping6Application", LOG_LEVEL_INFO);
    LogComponentEnable("DiffServQueue", LOG_LEVEL_ALL);
    LogComponentEnable("WRED", LOG_LEVEL_ALL);
    //LogComponentEnable("UdpL4Protocol", LOG_LEVEL_ERROR);
    //LogComponentEnable("TcpL4Protocol", LOG_LEVEL_ERROR);
    //LogComponentEnable("StatCollector6", LOG_LEVEL_ALL);
  }
  
  //~ LogComponentEnable ("TrafficGenerator", LOG_LEVEL_ALL);
  
  // Specific configurations
  Config::SetDefault ("ns3::TcpL4Protocol::SocketType", StringValue ("ns3::TcpFreeze"));
  




  NS_LOG_INFO ("Create nodes.");
  Ptr<Node> n0 = CreateObject<Node> ();
  Ptr<Node> n1 = CreateObject<Node> ();
  Ptr<Node> r1 = CreateObject<Node> ();
  Ptr<Node> r2 = CreateObject<Node> ();
  Ptr<Node> n2 = CreateObject<Node> ();

  NodeContainer net1 (n0, r1);
  NodeContainer net2 (n1, r1);
  NodeContainer net3 (r1, r2);
  NodeContainer net4 (r2, n2);
  NodeContainer all (n0, n1, r1, r2, n2);

  NS_LOG_INFO ("Create IPv6 Internet Stack");
  InternetStackHelper internetv6;
  internetv6.Install (all);

  NS_LOG_INFO ("Create channels.");
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("10Mb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.004)));
  NetDeviceContainer d1 = p2ph.Install (net1); /* n0 - R1 */
  NetDeviceContainer d2 = p2ph.Install (net2); /* n1 - R1 */
  NetDeviceContainer d3 = p2ph.Install (net3); /* R1 - R2 */
  NetDeviceContainer d4 = p2ph.Install (net4); /* R2 - n2 */

  //Setting the DiffServ queue to R1, other devices implement DropTail
  Ptr<PointToPointNetDevice> net_dev_r1 = DynamicCast<PointToPointNetDevice> (d3.Get(0));
  net_dev_r1->SetDataRate(DataRate ("5Mb/s"));
  

  //Simulator::Schedule (Seconds(65.0), Config::Set, "/ChannelList/2/$ns3::CsmaChannel/Delay", TimeValue (MilliSeconds(4.0)));
  //Simulator::Schedule (Seconds(10.0), Config::Set, "/ChannelList/*/$ns3::CsmaChannel/DataRate", DataRateValue (5000000));

  Ptr<DiffServQueue> q1 = CreateObject<DiffServQueue> ();

  //enabling QoS on the MR
  if (qos)
  {
    net_dev_r1->SetQueue(q1);
  }

/***********************Topology Configuration******************************/

  NS_LOG_INFO ("Create networks and assign IPv6 Addresses.");
  Ipv6AddressHelper ipv6;

  /* net1 */
  ipv6.SetBase (Ipv6Address ("2001:1::"), Ipv6Prefix (64));
  NetDeviceContainer tmp;
  tmp.Add (d1.Get (0)); /* n0 */
  Ipv6InterfaceContainer iic1 = ipv6.AssignWithoutAddress (tmp); /* n0 interface */

  NetDeviceContainer tmp2;
  tmp2.Add (d1.Get (1)); /* R1 */
  Ipv6InterfaceContainer iicr1 = ipv6.Assign (tmp2); /* R1 interface to the first subnet is just statically assigned */
  iicr1.SetForwarding (0, true);
  iicr1.SetDefaultRouteInAllNodes (0);
  iic1.Add (iicr1);

  /* net2 */
  ipv6.SetBase (Ipv6Address ("2001:2::"), Ipv6Prefix (64));
  NetDeviceContainer tmp3;
  tmp3.Add (d2.Get (0)); /* n1 */
  Ipv6InterfaceContainer iic2 = ipv6.AssignWithoutAddress (tmp3); /* n1 interface */

  NetDeviceContainer tmp4;
  tmp4.Add (d2.Get (1)); /* R1 */
  Ipv6InterfaceContainer iicr2 = ipv6.Assign (tmp4); /* R1 interface to the fourth subnet is just statically assigned */
  iicr2.SetForwarding (0, true);
  iicr2.SetDefaultRouteInAllNodes (0);
  iic2.Add (iicr2);

  /* net3 R1 - R2 */
  ipv6.SetBase (Ipv6Address ("2001:3::"), Ipv6Prefix (64));
  Ipv6InterfaceContainer iic3 = ipv6.Assign (d3);
  iic3.SetForwarding (0, true);
  iic3.SetDefaultRouteInAllNodes (0);
  iic3.SetForwarding (1, true);
  iic3.SetDefaultRouteInAllNodes (1);

  /* net4 R2 - n2 */
  ipv6.SetBase (Ipv6Address ("2001:4::"), Ipv6Prefix (64));
  NetDeviceContainer tmp5;
  tmp5.Add (d4.Get (0)); /* R2 */
  Ipv6InterfaceContainer iicr4 = ipv6.Assign (tmp5); /* R interface */
  iicr4.SetForwarding (0, true);
  iicr4.SetDefaultRouteInAllNodes (0);

  NetDeviceContainer tmp6;
  tmp6.Add (d4.Get (1)); /* n2 */
  Ipv6InterfaceContainer iic4 = ipv6.AssignWithoutAddress (tmp6); 
  iic4.Add (iicr4);

  /** radvd configuration */
  RadvdHelper radvdHelper;
  /* R interface (n0 - R1) */
  radvdHelper.AddAnnouncedPrefix(iic1.GetInterfaceIndex (1), Ipv6Address("2001:1::0"), 64);
  radvdHelper.AddAnnouncedPrefix(iic3.GetInterfaceIndex (0), Ipv6Address("2001:3::0"), 64);
  RadvdHelper radvdHelper2;
  /* R interface (n1 - R1) */
  radvdHelper.AddAnnouncedPrefix(iic2.GetInterfaceIndex (1), Ipv6Address("2001:2::0"), 64);
  radvdHelper.AddAnnouncedPrefix(iic3.GetInterfaceIndex (0), Ipv6Address("2001:3::0"), 64);
  /* R interface (R2 - n2) */
  RadvdHelper radvdHelper3;
  radvdHelper3.AddAnnouncedPrefix(iic3.GetInterfaceIndex (1), Ipv6Address("2001:3::0"), 64);
  radvdHelper3.AddAnnouncedPrefix(iic4.GetInterfaceIndex (1), Ipv6Address("2001:4::0"), 64);

  ApplicationContainer radvdApps = radvdHelper.Install (r1);
  radvdApps.Add(radvdHelper2.Install (r1));
  radvdApps.Add(radvdHelper3.Install (r2));
  radvdApps.Start (Seconds (0.0));
  radvdApps.Stop (Seconds (simTime));
  /** radvd configuration */

/***********************END OF Topology Configuration******************************/

/**********************************WRED Queue implementation*********************************************************/

  //Conformance and Meter Specs

  ConformanceSpec cSpec1;
  cSpec1.initialCodePoint = AF11;
  cSpec1.nonConformantActionI = AF12;
  cSpec1.nonConformantActionII = drop;

  ConformanceSpec cSpec2;
  cSpec2.initialCodePoint = AF21;
  cSpec2.nonConformantActionI = AF22;
  cSpec2.nonConformantActionII = AF23;

  ConformanceSpec cSpec3;
  cSpec3.initialCodePoint = AF31;
  cSpec3.nonConformantActionI = AF32;
  cSpec3.nonConformantActionII = AF33;

  ConformanceSpec cSpec4;
  cSpec4.initialCodePoint = AF41;
  cSpec4.nonConformantActionI = AF42;
  cSpec4.nonConformantActionII = AF43;

  ConformanceSpec cSpec5;
  cSpec5.initialCodePoint = EF;
  //~ cSpec5.nonConformantActionI = BE;
  //~ cSpec5.nonConformantActionII = BE;
  cSpec5.nonConformantActionI = EF; // Modified by CC (formely: BE)
  cSpec5.nonConformantActionII = EF;  // Modified by CC (formely: BE)

  MeterSpec mSpec1;  // -> AF1
  mSpec1.meterID = "TRTCM"; // Two Rate Three Color Marker: if not P bucket-> red; else if not C bucket -> yellow; else -> green
  mSpec1.cIR = 500000;
  mSpec1.cBS = 2000;
  mSpec1.eBS = 0;
  mSpec1.pIR = 1000000; // 1Mb/s
  mSpec1.pBS = 2000;

  MeterSpec mSpec2;  // -> AF2
  mSpec2.meterID = "SRTCM"; // Single Rate Three Color Marker: Bucket C?-> Green; else bucket E-> yellow; else -> red
  mSpec2.cIR = 5000000; // 5Mb/s
  mSpec2.cBS = 2000;
  mSpec2.eBS = 1500;
  mSpec2.pIR = 2500000;
  mSpec2.pBS = 1500;

  MeterSpec mSpec3;  // -> AF3
  mSpec3.meterID = "TRTCM"; 
  mSpec3.cIR = 1000000;
  mSpec3.cBS = 2000;
  mSpec3.eBS = 1500;
  mSpec3.pIR = 2500000; // 2.5Mb/s for 512kb/s (=> overprovisionning)
  mSpec3.pBS = 1500;

  MeterSpec mSpec4;  // -> AF4
  mSpec4.meterID = "TRTCM";
  mSpec4.cIR = 1000000;
  mSpec4.cBS = 2000;
  mSpec4.eBS = 1500;
  mSpec4.pIR = 3500000;
  mSpec4.pBS = 1500;

  MeterSpec mSpec5;  // -> EF
  mSpec5.meterID = "TokenBucket"; // bucket filled at cIR up to cBS?
  mSpec5.cIR = 1000000; // 1Mb/s
  mSpec5.cBS = 2000;    // bucket size = 2000 bytes => Allowed burst = 16kb
  mSpec5.eBS = 1500;
  mSpec5.pIR = 3500000;
  mSpec5.pBS = 1500;


  //Flow Setup

  Ptr<DiffServFlow> flow1Ptr = CreateObject<DiffServFlow> (001,"2001:1::200:ff:fe00:1","2001:4::200:ff:fe00:8",0,5001);//AF1 n0
  Ptr<DiffServFlow> flow2Ptr = CreateObject<DiffServFlow> (002,"2001:2::200:ff:fe00:3","2001:4::200:ff:fe00:8",0,5002);//AF2 n1
  Ptr<DiffServFlow> flow3Ptr = CreateObject<DiffServFlow> (003,"2001:1::200:ff:fe00:1","2001:4::200:ff:fe00:8",0,5003);//AF3 n0
  Ptr<DiffServFlow> flow4Ptr = CreateObject<DiffServFlow> (004,"2001:2::200:ff:fe00:3","2001:4::200:ff:fe00:8",0,5004);//AF4 n1
  Ptr<DiffServFlow> flow5Ptr = CreateObject<DiffServFlow> (005,"2001:1::200:ff:fe00:1","2001:4::200:ff:fe00:8",0,5005);//EF n0

  //SLA Setup

  Ptr<DiffServSla> sla1Ptr = CreateObject<DiffServSla>(001,cSpec1,mSpec1);
  Ptr<DiffServSla> sla2Ptr = CreateObject<DiffServSla>(002,cSpec2,mSpec2);
  Ptr<DiffServSla> sla3Ptr = CreateObject<DiffServSla>(003,cSpec3,mSpec3);
  Ptr<DiffServSla> sla4Ptr = CreateObject<DiffServSla>(004,cSpec4,mSpec4);
  Ptr<DiffServSla> sla5Ptr = CreateObject<DiffServSla>(005,cSpec5,mSpec5);

  flow1Ptr->SetSla(sla1Ptr);
  flow2Ptr->SetSla(sla2Ptr);
  flow3Ptr->SetSla(sla3Ptr);
  flow4Ptr->SetSla(sla4Ptr);
  flow5Ptr->SetSla(sla5Ptr);

  vector< Ptr<DiffServFlow> > myFlowVector;
  myFlowVector.push_back(flow1Ptr);
  myFlowVector.push_back(flow2Ptr);
  myFlowVector.push_back(flow3Ptr);
  myFlowVector.push_back(flow4Ptr);
  myFlowVector.push_back(flow5Ptr);


  DiffServQueue::SetDiffServFlows(myFlowVector);
  q1->SetQueueMode("Edge");


  //Meters setup

  Ptr<DiffServMeter> STB = Create<TokenBucket>();
  Ptr<DiffServMeter> srTCM = Create<SRTCM>();
  Ptr<DiffServMeter> trTCM = Create<TRTCM>();

  vector< Ptr<DiffServMeter> > myMeterVector;
  myMeterVector.push_back(STB);
  myMeterVector.push_back(srTCM);
  myMeterVector.push_back(trTCM);

  DiffServQueue::SetDiffServMeters(myMeterVector);

  //AQM setup

  Ptr<WRED> AQM1Ptr = Create<WRED>();
  /** Values for WRED: 
   * exponentialWeight, 
   * minThresholdRed, minThresholdYellow, minThresholdGreen, 
   * maxThresholdRed, maxThresholdYellow, maxThresholdGreen, 
   * maxProbabilityRed, maxProbabilityYellow,maxProbabilityGreen */
          
  AQM1Ptr->SetAF1WRED(7, 10,50,100,  10,100,60,  10,100,30); 
  AQM1Ptr->SetAF2WRED(7, 10,50,100,  10,100,60,  50,100,30); 
  AQM1Ptr->SetAF3WRED(7, 10,50,100,  10,100,60,  50,100,50); 
  AQM1Ptr->SetAF4WRED(7, 10,50,100,  10,100,60,  50,100,50); 

  //void SetAF1WRED(int exp, int redMinTh,int redMaxTh, int redMaxDrop, int yellowMinTh, int yellowMaxTh, int yellowMaxDrop, int greenMinTh, int greenMaxTh, int greenMaxDrop); 

// debut modif CC
//~ Simulator::Schedule ( Seconds(8.0), &ChangeAF3Params, myFlowVector); 
// FIN modif CC

  vector< Ptr<DiffServAQM> > myAQMVector;
  myAQMVector.push_back(AQM1Ptr);

// debut modif CC
  //~ q1->SetDiffServAQM (myAQMVector,"WRED","WRED","WRED","WRED");
  // Le commentaire précédent passe les queues en droptail
q1->m_dropHead = true;
// FIN modif CC

  //WRR,Queue size, EF limiter
  q1->SetWRRWeights(3,3,5,7,1);//AF1, AF2, AF3, AF4, BE
  q1->SetQueueSize(10,10,10,10,10,10);//AF1,AF2,AF3,AF4,EF,BE

/**********************************END OF WRED Queue implementation*********************************************************/

  /****************************** TRAFFIC GENERATION*********************************************************/

  Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
  startTimeSeconds->SetAttribute ("Min", DoubleValue (0.501));
  startTimeSeconds->SetAttribute ("Max", DoubleValue (0.510));
  Time startTimeUdp = Seconds (startTimeSeconds->GetValue ());
  uint32_t udpPqtSize = 1024; // default 1024 / vary between 12 and 1500
  Time startTimeTcp = Seconds (0.503);

  /********************** UPLINK**************************/

  //Control of traffic  
  //~ bool beTcpUl = false; //1x1Mb/s TCP, both ways
  //~ bool beUdpUl = true; //1x512kb/s UDP, both ways 
  //~ bool af1UdpUl = false;
  //~ bool af2UdpUl = false;
  //~ bool af3UdpUl = true; //2x64kb/s UDP, both ways (2x64 or 1x128 ?)
  //~ bool af4UdpUl = true; //4x512kb/s UDP, both ways (4x512 or 1x2048 ?)
  //~ bool efUdpUl = true; //250kbps
//~ 
  //~ bool beTcpDl = false; //1x1Mb/s TCP, both ways
  //~ bool beUdpDl = true; //1x512kb/s UDP, both ways 
  //~ bool af1UdpDl = false;
  //~ bool af2UdpDl = false;
  //~ bool af3UdpDl = true; //2x64kb/s UDP, both ways (2x64 or 1x128 ?)
  //~ bool af4UdpDl = true; //4x512kb/s UDP, both ways (4x512 or 1x2048 ?)
  //~ bool efUdpDl = true; //250kbps



  bool af1UdpUl = false;
  bool af2UdpUl = false;
  bool af1UdpDl = false;
  bool af2UdpDl = false;
  
  //~ bool beTcpUl = true; //1x1Mb/s TCP, both ways
  //~ bool beUdpUl = false; //1x512kb/s UDP, both ways 
bool beTcpUl = false; //1x1Mb/s TCP, both ways
bool beUdpUl = true; //1x512kb/s UDP, both ways 
  
  bool af3UdpUl = true; //2x64kb/s UDP, both ways (2x64 or 1x128 ?)
  bool af4UdpUl = true; //4x512kb/s UDP, both ways (4x512 or 1x2048 ?)
  bool efUdpUl = true; //250kbps

  bool beTcpDl = true; //1x1Mb/s TCP, both ways
  bool beUdpDl = false; //1x512kb/s UDP, both ways 
  bool af3UdpDl = true; //2x64kb/s UDP, both ways (2x64 or 1x128 ?)
  bool af4UdpDl = true; //4x512kb/s UDP, both ways (4x512 or 1x2048 ?)
  bool efUdpDl = true; //250kbps



  /**
   * SCENARIO 1
  EF: 250kb/s UDP, both ways for Train Control
  AF3: 2x64kb/s UDP, both ways (2x64 or 1x128 ?) for VoIP
  AF4: 4x512kb/s UDP, both ways (4x512 or 1x2048 ?) for CCTV
  BE1: 1x512kb/s UDP, both ways 
  BE2: 1x1Mb/s TCP, both ways
  */

  /**
   * SCENARIO 2
  EF: 256kb/s UDP, both ways for Train Control
  AF3: 512kb/s UDP, both ways for CCTV
  AF4: 64b/s UDP, both ways for VoIP
  BE1: 2Mb/s UDP, both ways 
  */
    
  uint16_t ulport1 = 5001; //af1 n0
  uint16_t ulport2 = 5002; //af2 n1
  uint16_t ulport3 = 5003; //af3 n0
  uint16_t ulport4 = 5004; //af4 n1
  uint16_t ulport5 = 5005; //ef n0
  uint16_t ulport6 = 5006; //be n1

    
  // BE TCP trafic (tcpPort)
  TrafficGenerator beTcpUlApp;
  if (beTcpUl)
    {
      //~ beTcpUlApp.Setup ("ns3::TcpSocketFactory", n1, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport6,
                             //~ 512, DataRate("1Mbps")); 
      beTcpUlApp.SetupBulk ("ns3::TcpSocketFactory", n1, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport6,
                             512, 1500000); 
      beTcpUlApp.SetStartTime (startTimeTcp);   
      beTcpUlApp.RecordTraffic ( "SimulationPrototype/beTcpUl.txt", MilliSeconds(samplePeriod) );
      
      
      if (maxAdvertisedWindowSize != 0)
        {
          //~ Simulator::Schedule ( Seconds(5.5), &setMaxTcpWndSize, &beTcpUlApp, maxAdvertisedWindowSize);
          //~ Simulator::Schedule ( Seconds(10.1), &setMaxTcpWndSize, &beTcpUlApp, 0);

//~ Config::SetDefault ("ns3::TcpFreeze::MaxAdvertisedWindowSize", UintegerValue (maxAdvertisedWindowSize));    

Simulator::Schedule ( Seconds(0.6), &setMaxTcpWndSize, &beTcpUlApp, maxAdvertisedWindowSize);

        }
      if (freeze)
        {
          Simulator::Schedule ( Seconds(5.5), &stopTcp, &beTcpUlApp);
          Simulator::Schedule ( Seconds(9.5), &restoreTcp, &beTcpUlApp);
        }
    }
    
  // BE UDP trafic
  TrafficGenerator beUdpUlApp;
  if (beUdpUl)
    {
      beUdpUlApp.Setup ("ns3::UdpSocketFactory", n1, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport6,
                             udpPqtSize, DataRate("2Mbps"));
      startTimeUdp = Seconds (startTimeSeconds->GetValue ()); 
      beUdpUlApp.SetStartTime (startTimeUdp);   
      beUdpUlApp.RecordTraffic ( "SimulationPrototype/beUdpUl.txt", MilliSeconds(samplePeriod) );
    }
    
   // AF1 UDP UL (from n0 to n2)
  TrafficGenerator af1UdpUlApp;
  if (af1UdpUl)
    {
      af1UdpUlApp.Setup ("ns3::UdpSocketFactory", n0, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport1,
                             udpPqtSize, DataRate("2048kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af1UdpUlApp.SetStartTime (startTimeUdp);
      af1UdpUlApp.RecordTraffic ( "SimulationPrototype/af1UdpUl.txt", MilliSeconds(samplePeriod) );
    }
    
  // AF2 UDP UL (from n1 to n2)
  TrafficGenerator af2UdpUlApp;
  if (af2UdpUl)
    {
      af2UdpUlApp.Setup ("ns3::UdpSocketFactory", n1, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport2,
                             udpPqtSize, DataRate("2048kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af2UdpUlApp.SetStartTime (startTimeUdp);
      af2UdpUlApp.RecordTraffic ( "SimulationPrototype/af2UdpUl.txt", MilliSeconds(samplePeriod) );
    }
    
  // AF3 UDP UL (from n0 to n2)
  TrafficGenerator af3UdpUlApp;
  if (af3UdpUl)
    {
      af3UdpUlApp.Setup ("ns3::UdpSocketFactory", n0, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport3,
                             udpPqtSize, DataRate("512kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af3UdpUlApp.SetStartTime (startTimeUdp);
      af3UdpUlApp.RecordTraffic ( "SimulationPrototype/af3UdpUl.txt", MilliSeconds(samplePeriod) );
    }
    
     // AF4 UDP UL (from n1 to n2)
  TrafficGenerator af4UdpUlApp;
  if (af4UdpUl)
    {
      af4UdpUlApp.Setup ("ns3::UdpSocketFactory", n1, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport4,
                             //~ udpPqtSize, DataRate("64kbps")); 
                             udpPqtSize, DataRate("128kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af4UdpUlApp.SetStartTime (startTimeUdp);
      af4UdpUlApp.RecordTraffic ( "SimulationPrototype/af4UdpUl.txt", MilliSeconds(samplePeriod) );
    }

  // EF UDP UL (from n0 to n2)
  TrafficGenerator efUdpUlApp;
  if (efUdpUl)
    {
      efUdpUlApp.Setup ("ns3::UdpSocketFactory", n0, n2, Ipv6Address("2001:4::200:ff:fe00:8"), ulport5,
                             udpPqtSize, DataRate("256kbps"));
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());
      efUdpUlApp.SetStartTime (startTimeUdp);   
      efUdpUlApp.RecordTraffic ( "SimulationPrototype/efUdpUl.txt", MilliSeconds(samplePeriod) );
    }
  
  /********************** DOWNLINK**************************/
  uint16_t dlport1 = 4001; //af1 n0
  uint16_t dlport2 = 4002; //af2 n1
  uint16_t dlport3 = 4003; //af3 n0
  uint16_t dlport4 = 4004; //af4 n1
  uint16_t dlport5 = 4005; //ef n0
  uint16_t dlport6 = 4006; //be n1
    
  // BE TCP trafic (tcpPort)
  TrafficGenerator beTcpDlApp;
  if (beTcpDl)
    {
      beTcpDlApp.Setup ("ns3::TcpSocketFactory", n2, n1, Ipv6Address("2001:2::200:ff:fe00:3"), dlport6,
                             512, DataRate("1Mbps")); 
      beTcpDlApp.SetStartTime (startTimeTcp);   
      beTcpDlApp.RecordTraffic ( "SimulationPrototype/beTcpDl.txt", MilliSeconds(samplePeriod) );
    }
    
  // BE UDP trafic
  TrafficGenerator beUdpDlApp;
  if (beUdpDl)
    {
      beUdpDlApp.Setup ("ns3::UdpSocketFactory", n2, n1, Ipv6Address("2001:2::200:ff:fe00:3"), dlport6,
                             udpPqtSize, DataRate("2Mbps"));
      startTimeUdp = Seconds (startTimeSeconds->GetValue ()); 
      beUdpDlApp.SetStartTime (startTimeUdp);   
      beUdpDlApp.RecordTraffic ( "SimulationPrototype/beUdpDl.txt", MilliSeconds(samplePeriod) );
    }
    
   // AF1 UDP DL (from n0 to n2)
  TrafficGenerator af1UdpDlApp;
  if (af1UdpDl)
    {
      af1UdpDlApp.Setup ("ns3::UdpSocketFactory", n2, n0, Ipv6Address("2001:1::200:ff:fe00:1"), dlport1,
                             udpPqtSize, DataRate("2048kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af1UdpDlApp.SetStartTime (startTimeUdp);
      af1UdpDlApp.RecordTraffic ( "SimulationPrototype/af1UdpDl.txt", MilliSeconds(samplePeriod) );
    }
    
  // AF2 UDP DL (from n1 to n2)
  TrafficGenerator af2UdpDlApp;
  if (af2UdpDl)
    {
      af2UdpDlApp.Setup ("ns3::UdpSocketFactory", n2, n1, Ipv6Address("2001:2::200:ff:fe00:3"), dlport2,
                             udpPqtSize, DataRate("2048kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af2UdpDlApp.SetStartTime (startTimeUdp);
      af2UdpDlApp.RecordTraffic ( "SimulationPrototype/af2UdpDl.txt", MilliSeconds(samplePeriod) );
    }
    
  // AF3 UDP DL (from n0 to n2)
  TrafficGenerator af3UdpDlApp;
  if (af3UdpDl)
    {
      af3UdpDlApp.Setup ("ns3::UdpSocketFactory", n2, n0, Ipv6Address("2001:1::200:ff:fe00:1"), dlport3,
                             udpPqtSize, DataRate("512kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af3UdpDlApp.SetStartTime (startTimeUdp);
      af3UdpDlApp.RecordTraffic ( "SimulationPrototype/af3UdpDl.txt", MilliSeconds(samplePeriod) );
    }
    
     // AF4 UDP DL (from n1 to n2)
  TrafficGenerator af4UdpDlApp;
  if (af4UdpDl)
    {
      af4UdpDlApp.Setup ("ns3::UdpSocketFactory", n2, n1, Ipv6Address("2001:2::200:ff:fe00:3"), dlport4,
                             udpPqtSize, DataRate("64kbps")); 
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());                       
      af4UdpDlApp.SetStartTime (startTimeUdp);
      af4UdpDlApp.RecordTraffic ( "SimulationPrototype/af4UdpDl.txt", MilliSeconds(samplePeriod) );
    }

  // EF UDP DL (from n0 to n2)
  TrafficGenerator efUdpDlApp;
  if (efUdpDl)
    {
      efUdpDlApp.Setup ("ns3::UdpSocketFactory", n2, n0, Ipv6Address("2001:1::200:ff:fe00:1"), dlport5,
                             udpPqtSize, DataRate("256kbps"));
      startTimeUdp = Seconds (startTimeSeconds->GetValue ());
      efUdpDlApp.SetStartTime (startTimeUdp);   
      efUdpDlApp.RecordTraffic ( "SimulationPrototype/efUdpDl.txt", MilliSeconds(samplePeriod) );
    }    
  /******************************END OF TRAFFIC GENERATION*********************************************************/

  AsciiTraceHelper ascii;
  p2ph.EnableAsciiAll (ascii.CreateFileStream ("SimulationPrototype/simulationTraces.tr"));
  p2ph.EnablePcapAll (std::string ("SimulationPrototype/simulationNode"), true);

  // Flow monitor
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor;

  monitor = flowmon.InstallAll ();

  AsciiTraceHelper asciiTraceHelper;
  Ptr<OutputStreamWrapper> thoughputStream = asciiTraceHelper.CreateFileStream("SimulationPrototype/throughput.dat");
  Ptr<OutputStreamWrapper> lostPacketsStream = asciiTraceHelper.CreateFileStream("SimulationPrototype/lost_packets.dat");
  ThroughputMonitor (&flowmon, monitor, MilliSeconds(samplePeriod), thoughputStream,lostPacketsStream); 
  q1->RecordQueueData (MilliSeconds(samplePeriod), "SimulationPrototype/Queue_stats.txt");

  if(modifyPolicy)
  {
    //~ Simulator::Schedule(Seconds(4.5), &ModifyConformantAction, 1, q1);
    //~ Simulator::Schedule(Seconds(6), &ModifyConformantAction, 2, q1);
    //~ Simulator::Schedule(Seconds(8.5), &ModifyConformantAction, 1, q1);
    //~ Simulator::Schedule(Seconds(10), &ModifyConformantAction, 0, q1);
  }

  if(modifyQueueSize)
  {
    //~ Simulator::Schedule(Seconds(6), &ModifyQueueSize, 200, "AF4", q1);
    //~ Simulator::Schedule(Seconds(8.5), &ModifyQueueSize, 100, "AF4", q1);
  }

  if(modifyQueueWeights)
  {

  }

  if(degradation)
  {
    std::list<struct inputData> inputDataList = InputDataFromFile ("SimulationPrototype/LTEsimulation.txt");
    ChangeDataRate (net_dev_r1, MilliSeconds(samplePeriod), inputDataList, q1); 
  }
//~ std::cout << "init : "  << net_dev_r1 << std::endl;

  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

  //StatCollector6::OutputEndToEndStats();
  //StatCollector6::OutputQueueStats();

  monitor->CheckForLostPackets ();
  Ptr<Ipv6FlowClassifier> classifier = DynamicCast<Ipv6FlowClassifier> (flowmon.GetClassifier6 ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
  {
    Ipv6FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);

    std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ":" <<t.destinationPort<<" :"<< Protocol2String(t.protocol)<< ")\n";
    std::cout << " Tx Bytes: " << i->second.txBytes << "\n";
    std::cout << " Rx Bytes: " << i->second.rxBytes << "\n";
    std::cout << " Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds() - i->second.timeFirstTxPacket.GetSeconds())/1024/1024 << " Mbps\n";
  }

  monitor->SerializeToXmlFile("SimulationPrototype/flowmon.xml", true, true);

  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");

  return 0;
}

void ChangeDataRate (Ptr<PointToPointNetDevice> net_dev_r1, Time samplePeriod, std::list<struct inputData> dataList, Ptr<DiffServQueue> q1)
{
  static int nbpass=0;
  static std::list<struct inputData> list;
  static std::list<struct inputData>::iterator it;
  static DataRate dr;

  if(nbpass==0)
  {
    std::cout << "changing data rate" << std::endl;
    list=dataList;
    it = list.begin();  
  }
    /**while (it != inputDataList.end()) 
      {
      std::cout << it->timeInterval << " "<< it->throughput << " "<< it->avgPacketDelay << std::endl;
      it++;
      } */  
    
  if (it != list.end())
  {
    if ((it->throughput)==0 and nbpass==0)
    {
      dr=(DataRate("1000000bps")); 
    }
    else if (!(it->throughput)==0)
    {
      dr=(DataRate((it->throughput)*1024*1024)); 
    }
    
    std::cout << Simulator::Now ().GetSeconds () << "\t :" << dr << std::endl;
    //~ std::cout << Simulator::Now ().GetSeconds () << "\t :" << dr << "\t" << net_dev_r1 << std::endl;
    net_dev_r1->SetDataRate(dr);
    
    if(modifyPolicy)
    {
      it++;
      //~ double temp_rate=it->throughput;
      double temp_rate = dr.GetBitRate () / (1024.0*1024.0);
      //~ if(temp_rate > 2.)
      if(temp_rate > 1.)
      { 
        ModifyConformantAction(0, q1);
        if(modifyQueueSize){
          ModifyQueueSize(10 , 10 , 10 , 10 ,10 , 10 , q1); 
          }
      } //good condition
      //~ else if ((temp_rate <= 2.) && (temp_rate > 1.))
      else if (temp_rate > 0.5)
      { 
        ModifyConformantAction(1, q1);
        if(modifyQueueSize){
          ModifyQueueSize(5 , 5 , 5 , 5 ,10 , 10 , q1);
        }
      } //degraded condition
      //~ else if (temp_rate <= 1.)
      else
      { 
        ModifyConformantAction(2, q1);
        if(modifyQueueSize){
          ModifyQueueSize(5 , 5 , 5 , 5 ,10 , 10 , q1); 
        }
      } //critical condition
      it--;
    }
    
    //std::cout << it->timeInterval << " "<< it->throughput << " "<< it->avgPacketDelay << std::endl;
    Simulator::Schedule ( samplePeriod, &ChangeDataRate, net_dev_r1, samplePeriod, dataList, q1);
    it++;
    nbpass++;
    }
}
 
std::list<struct inputData> InputDataFromFile(const char* filePath)
{
  std::cout << "InputDataFromFile" << std::endl;
  static std::list<struct inputData> inputList;
    std::list<struct inputData>::iterator it;
    ifstream infile;
    infile.open(filePath);   

    std::string line;
  while (std::getline(infile, line))
  {
      std::stringstream ss(line);
      double a, b, c, d, e, f, g;
      if (ss >> a >> b >> c >> d >> e >> f >> g)
      {
        // Add a, b, c etc to their respective arrays
        struct inputData newItem;
        newItem.timeInterval = a;
        newItem.throughput = b;
        newItem.avgPacketDelay  = g;
        inputList.push_back (newItem);
        //~ std::cout << "data t=" << a << "\td=" << b << "\tdelay=" << g <<std::endl;
      }
    }
  return inputList;
  /**it = inputList.begin();  
  while (it != inputList.end()) 
    {
    std::cout << it->timeInterval << " "<< it->throughput << " "<< it->avgPacketDelay << std::endl;
    it++;
    } */
  }

void ModifyQueueSize(int size_AF1 , int size_AF2 , int size_AF3 , int size_AF4 , int size_EF , int size_BE , Ptr<DiffServQueue> q1)
{
  q1->SetQueueSize(size_AF1 , size_AF2 , size_AF3 , size_AF4 , size_EF , size_BE);
}

void ModifyQueueSize(int size, string queue, Ptr<DiffServQueue> q1)//"AF1", etc
{
  q1->SetSingleQueueSize(size, queue);
}

void ModifyQueueWeights(int weight_AF1 , int weight_AF2 , int weight_AF3 , int weight_AF4 , int weight_BE , Ptr<DiffServQueue> q1)
{
  q1->SetWRRWeights(weight_AF1,weight_AF2,weight_AF3,weight_AF4,weight_BE);//AF1, AF2, AF3 , AF4 , BE
}

void ModifyConformantAction(int level, Ptr<DiffServQueue> q1)
{
  q1->ModifyQosPolicy(level);
}

struct througputData
{
  unsigned int flowId;
  double lastTimeStamp;
  double lastRxBytes;
  double avgThrough;
  int lastLostPackets;
};

std::string Protocol2String (unsigned int protocol)
{
  std::string ret;
  switch (protocol)
  {
    case 6:
      ret = "TCP";
      break;
    case 17:
      ret = "UDP";
      break;
    default:
      char buffer[10];
      sprintf (buffer, "%u", protocol);
      ret = buffer;        
      break;
  }
  return ret;
}

void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Time samplePeriod, Ptr<OutputStreamWrapper> stream, Ptr<OutputStreamWrapper> stream_loss)
{
  static int nbPass = 0;
  static std::list<struct througputData> througputList;

  std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
  Ptr<Ipv6FlowClassifier> classing = DynamicCast<Ipv6FlowClassifier> (fmhelper->GetClassifier6());


  if (nbPass==1) // Print header in file
  {
    *stream->GetStream() << "time\t";
    *stream_loss->GetStream() << "time\t";
    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
    {
      Ipv6FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
      *stream->GetStream() << fiveTuple.sourceAddress << ":" << fiveTuple.sourcePort << " -> " << fiveTuple.destinationAddress << ":" << fiveTuple.destinationPort << " (" << Protocol2String (fiveTuple.protocol) << ")" << "\t";
    *stream_loss->GetStream() << fiveTuple.sourceAddress << ":" << fiveTuple.sourcePort << " -> " << fiveTuple.destinationAddress << ":" << fiveTuple.destinationPort << " (" << Protocol2String (fiveTuple.protocol) << ")" << "\t";

    }  
    *stream->GetStream() << std::endl;
    *stream_loss->GetStream() << std::endl;

  }

  if (nbPass!=0)
  {
    *stream->GetStream() << setprecision (2) << fixed << (double)Simulator::Now().GetSeconds(); 
    *stream_loss->GetStream() << setprecision (2) << fixed << (double)Simulator::Now().GetSeconds(); 
  }     
  
  
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
  {
    bool found = false;
    double currentThroughput;
    std::list<struct througputData>::iterator it; 
    
    //~ std::cout << "...................\nTraitement du Flow #" << stats->first << "\n";
    it = througputList.begin();
    while (it != througputList.end()) // Parse trafic list to find corresponding one
    {
      //~ std::cout << "Comparaison avec #" << it->flowId << "\n";
      if (it->flowId == stats->first)
      {
    currentThroughput = ( (stats->second.rxBytes - it->lastRxBytes) * 8.0d / ( Simulator::Now ().GetSeconds () - it->lastTimeStamp ) /1000/1000 );        it->avgThrough= (it->avgThrough+currentThroughput);
        
        *stream->GetStream() << "\t" << setprecision (8) << fixed << currentThroughput;
        *stream_loss->GetStream() << "\t" << stats->second.lostPackets- it->lastLostPackets;
        it->lastLostPackets=stats->second.lostPackets;
    
        
        //~ Ipv6FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
        //~ std::cout << "Found Flow " << stats->first  << " (" << fiveTuple.sourceAddress << ":" << fiveTuple.sourcePort << " -> " << fiveTuple.destinationAddress << ":" << fiveTuple.destinationPort << ") protocol: " << (int)fiveTuple.protocol << "\n";
        //~ std::cout << "    +----> " << currentThroughput << "\n";
    
        it->lastTimeStamp = stats->second.timeLastTxPacket.GetSeconds();
        it->lastRxBytes  = stats->second.rxBytes;
        found = true;
        break;
      }
      it++;
  }
    
    if (found != true) // if trafic was not found then add it to the list
    {
      struct througputData newItem;
      newItem.flowId = stats->first;
      newItem.lastTimeStamp = Simulator::Now ().GetSeconds ();
      newItem.lastRxBytes  = stats->second.rxBytes;
      newItem.lastLostPackets= stats->second.lostPackets;
      througputList.push_back (newItem);
      //~ std::cout << "NEW FLOW DETECTED " << stats->first  << " (" << fiveTuple.sourceAddress << ":" << fiveTuple.sourcePort << " -> " << fiveTuple.destinationAddress << ":" << fiveTuple.destinationPort << ") protocol: " << (int)fiveTuple.protocol << "\n";
        //~ currentThroughput = ( (stats->second.rxBytes - it->lastRxBytes) * 8.0d / ( Simulator::Now ().GetSeconds () - it->lastTimeStamp ) /1024/1024 );
        //~ std::cout << "    +----> " << currentThroughput << "\n";
    }

  } // END for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)


  if (nbPass!=0) // Add end line to file
  {
    {
    *stream->GetStream() << std::endl;  
    *stream_loss->GetStream() << std::endl;  
  }
  }
    
  Simulator::Schedule (samplePeriod, &ThroughputMonitor, fmhelper, flowMon, samplePeriod, stream, stream_loss);

  nbPass++;
}

/************************************************************ Garbage **********************************************************************/

/*****************************************Old Throughput method*****************************************************************************************
void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Time samplePeriod, Ptr<OutputStreamWrapper> stream, tuple_list previousValues)
{
double localThrou=0;
static int nbPass = 0;
tuple_list::iterator it;
    
std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
Ptr<Ipv6FlowClassifier> classing = DynamicCast<Ipv6FlowClassifier> (fmhelper->GetClassifier6());


if ((double)Simulator::Now().GetSeconds()==1.) // Print header in file
{
*stream->GetStream() << "time\t";
for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
{
Ipv6FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);

std::string protocolString;
switch (fiveTuple.protocol)
{
case 6:
protocolString = "TCP";
break;
case 17:
protocolString = "UDP";
break;
default:
char buffer[10];
sprintf (buffer, "%d", (int)fiveTuple.protocol);
protocolString = buffer;        
break;
}
*stream->GetStream() << fiveTuple.sourceAddress << ":" << fiveTuple.sourcePort << " -> " << fiveTuple.destinationAddress << ":" << fiveTuple.destinationPort << " (" << protocolString << ")" << "\t";
}  
*stream->GetStream() << std::endl;
}

if ((double)Simulator::Now().GetSeconds()>=1.)
{
*stream->GetStream() << setprecision (1) << fixed << (double)Simulator::Now().GetSeconds();      
//} modif

for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
{

if ((double)Simulator::Now().GetSeconds()==1.)
{
  previousValues.push_back(tuple<double, double>(stats->second.rxBytes,stats->second.timeFirstTxPacket.GetSeconds()) );
  }

it = previousValues.begin();  
localThrou = ((stats->second.rxBytes-(it->get<0>())) * 8.0 / ( stats->second.timeLastRxPacket.GetSeconds()-(it->get<1>()) )/1024/1024);

it= previousValues.erase(it);
it=previousValues.insert(it, tuple<double, double>(stats->second.rxBytes,stats->second.timeLastTxPacket.GetSeconds()) );
it++;
//localThrou = (stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds())/1024/1024);

*stream->GetStream() << "\t" << setprecision (8) << fixed <<(double)localThrou;
}
}

// if ((double)Simulator::Now().GetSeconds()>=1.)
if ((double)Simulator::Now().GetSeconds()>=1.)
{
*stream->GetStream() << std::endl;  
}

Simulator::Schedule (samplePeriod, &ThroughputMonitor, fmhelper, flowMon, samplePeriod, stream, previousValues);

{
flowMon->SerializeToXmlFile ("SimulationPrototype/ThroughputMonitor.xml", true, true);
}

nbPass++;
}
*****************************************Old Throughput method*****************************************************************************************/

/******************************Ping6 from node 0 to 1******************************
//Create a Ping6 application to send ICMPv6 echo request from n0 to n2 via R 
uint32_t packetSize = 1024;
uint32_t maxPacketCount = 1000;
Time interPacketInterval = Seconds (.1);
Ping6Helper ping6; 

//ping6.SetLocal (iic1.GetAddress (0, 1)); 
ping6.SetRemote (Ipv6Address ("2001:4::200:ff:fe00:8")); should be n2 address after autoconfiguration 
ping6.SetIfIndex (iic4.GetInterfaceIndex (0));

ping6.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
ping6.SetAttribute ("Interval", TimeValue (interPacketInterval));
ping6.SetAttribute ("PacketSize", UintegerValue (packetSize));

ApplicationContainer apps = ping6.Install (net2.Get (0));
apps.Start (Seconds (2.0));
apps.Stop (Seconds (30.0));
******************************Ping6 from node 0 to 1*******************************/

/******************************CSMA*************************************************
CsmaHelper csma;
csma.SetChannelAttribute ("DataRate", DataRateValue (10000000));//10Mbps
csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (channelDelay)));
NetDeviceContainer d1 = csma.Install (net1); // n0 - R1 
NetDeviceContainer d2 = csma.Install (net2); // n1 - R1

CsmaHelper csma2;
csma2.SetChannelAttribute ("DataRate", DataRateValue (channelDataRate));//10Mbps by default
csma2.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (channelDelay)));
NetDeviceContainer d3 = csma2.Install (net3); // R1 - R2

NetDeviceContainer d4 = csma.Install (net4); // R2 - n2
******************************END OF CSMA************************************************/

/****************************************************TCP traffic *********************************************************
//FOR BE CLASS
//TCP connection from n0 to n2
uint16_t dlport = 5004;
Address remote (Inet6SocketAddress ("2001:4::200:ff:fe00:8", dlport)); // interface of n2

PacketSinkHelper packetSinkHelper ("ns3::TcpSocketFactory", Inet6SocketAddress (Ipv6Address::GetAny (), dlport));
ApplicationContainer sinkApps = packetSinkHelper.Install (net4.Get(1)); //n2 as sink
sinkApps.Start (Seconds (0.5));
sinkApps.Stop (Seconds (simTime));

Ptr<Socket> ns3TcpSocket = Socket::CreateSocket (net1.Get(0), TcpSocketFactory::GetTypeId ()); //source at n0

// Create TCP application at n0
Ptr<SimpleSource> app = CreateObject<SimpleSource> ();
app->Setup (ns3TcpSocket, remote, 1040, 10000000, DataRate ("1Mbps"));
net1.Get(0)->AddApplication (app);
app->SetStartTime (Seconds (0.52));
app->SetStopTime (Seconds (simTime));

******************************TCP traffic from node 2 to 1*********************************************************
//FOR AF4 CLASS
//TCP connection from n1 to n2
uint16_t dlulport2 = 5002;
Address remote2 (Inet6SocketAddress ("2001:4::200:ff:fe00:8", dlulport2)); // interface of n2

PacketSinkHelper packetSinkHelper2 ("ns3::TcpSocketFactory", Inet6SocketAddress (Ipv6Address::GetAny (), dlulport2));
ApplicationContainer sinkApps2 = packetSinkHelper2.Install (net4.Get(1)); //n2 as sink
//sinkApps2.Start (Seconds (0.1));
//sinkApps2.Stop (Seconds (simTime));

Ptr<Socket> ns3TcpSocket2 = Socket::CreateSocket (net2.Get(0), TcpSocketFactory::GetTypeId ()); //source at n1

// Create TCP application at n1
Ptr<SimpleSource> app2 = CreateObject<SimpleSource> ();
app2->Setup (ns3TcpSocket2, remote2, 1040, 10000, DataRate ("1Mbps"));
net2.Get(0)->AddApplication (app2);
//app2->SetStartTime (Seconds (0.2));
//app2->SetStopTime (Seconds (simTime));

******************************UDP traffic from node 0 and 2 -> 1*********************************************************
uint32_t pqtSize = 1024; // default 1024 / vary between 12 and 1500
Time pqtInterval = MilliSeconds (10); // default: 10ms
uint32_t maxPacketCount = 1000000000;

// UL (from n0 to n2) //FOR AF3 CLASS
// Create one udpServer application on node one.
//
uint16_t ulport1 = 5002; // Need different port numbers to ensure there is no conflict//af4
uint16_t ulport2 = 5001;//af3
uint16_t ulport3 = 5003;//ef
uint16_t ulport4 = 5004;//be

UdpServerHelper server1 (ulport1);
UdpServerHelper server2 (ulport2);
UdpServerHelper server3 (ulport3);
UdpServerHelper server4 (ulport4);
ApplicationContainer apps;
apps = server1.Install (n2);
apps = server2.Install (n2);
apps = server3.Install (n2);
apps = server4.Install (n2);
apps.Start (Seconds (0.5));
apps.Stop (Seconds (simTime));
//
// Create one UdpClient application to send UDP datagrams from node zero to
// node one.
//
UdpClientHelper client1 (Ipv6Address("2001:4::200:ff:fe00:8"), ulport1);
UdpClientHelper client2 (Ipv6Address("2001:4::200:ff:fe00:8"), ulport2);
UdpClientHelper client3 (Ipv6Address("2001:4::200:ff:fe00:8"), ulport3);
UdpClientHelper client4 (Ipv6Address("2001:4::200:ff:fe00:8"), ulport4);
client1.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
client1.SetAttribute ("Interval", TimeValue (pqtInterval));
client1.SetAttribute ("PacketSize", UintegerValue (pqtSize));
client2.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
client2.SetAttribute ("Interval", TimeValue (pqtInterval));
client2.SetAttribute ("PacketSize", UintegerValue (pqtSize));
client3.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
client3.SetAttribute ("Interval", TimeValue (MilliSeconds (30)));
client3.SetAttribute ("PacketSize", UintegerValue (pqtSize));
client4.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
client4.SetAttribute ("Interval", TimeValue (pqtInterval));
client4.SetAttribute ("PacketSize", UintegerValue (pqtSize));
apps = client3.Install (n1);//250kbps
apps = client1.Install (n0);//1Mbps
//apps = client4.Install (n1);
//apps = client2.Install (n0);

apps.Start (startTimeUdp);
apps.Stop (Seconds (simTime));

***********************************END OF TCP traffic*****************************************************/
