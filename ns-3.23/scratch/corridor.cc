/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Telecom Bretagne
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Couturier <christophe.couturier@telecom-bretagne.eu>
 */

// For Python
//#include <cstdlib> 
//#include <Python.h>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/netanim-module.h"
#include "ns3/gnuplot.h"

#include "ns3/cognitive-manager.h"

#include <ns3/radio-environment-map-helper.h>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <list>

#include <ns3/lte-rrc-sap.h>
#include <ns3/lte-common.h>

#include <boost/format.hpp>

#include "ns3/evolved-friis-propagation-loss-model.h"


#include "ns3/traffic-generator.h"

using namespace ns3;


NS_LOG_COMPONENT_DEFINE ("corridor");

//~ void
//~ tstCallback (Ptr<OutputStreamWrapper> stream, uint32_t oldNb, uint32_t newNb)
//~ {
  //~ NS_LOG_DEBUG ("RxMsgChangeCallback: old=" << oldNb << "  new=" << newNb); 
  //~ *stream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newNb << std::endl;
//~ }


//~ void PacketDropCB (std::string context, Ptr<Packet> p)
//~ {
  //~ std::cout << context << "PACKET DROP!!!!" << std::endl;
//~ }


 //~ 
//~ static void NextRxSeqCallback ( Ptr<OutputStreamWrapper> stream, SequenceNumber32 oldSeq, SequenceNumber32 newSeq)
//~ {
  //~ std::cout << "OK \n";
  //~ *stream->GetStream() << Simulator::Now().GetSeconds() << "\t" << oldSeq << std::endl;
  //~ *stream->GetStream() << Simulator::Now().GetSeconds() << "\t" << newSeq << std::endl;
//~ }
//~ void startRxSeqRecording(Ptr<TrafficGenerator> app)
//~ {
  //~ Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  //~ if (p!=0)
  //~ {
    //~ AsciiTraceHelper asciiTraceHelper; 
    //~ Ptr<OutputStreamWrapper> rxSeqStream = asciiTraceHelper.CreateFileStream ("RxSeq.txt");
    //~ p->TraceConnectWithoutContext ("StatLastRxSeq", MakeBoundCallback (&NextRxSeqCallback, rxSeqStream));
    //~ std::cout << "Connection result: " << "\n";
  //~ }
  //~ else
  //~ {
    //~ std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    //~ std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  //~ }
//~ }

void setMaxTcpWndSize (Ptr<TrafficGenerator> app, uint32_t size)
{
  std::cout <<  "-------------ENTERING setWndSize; app= " << app << " sinkSock=" << app->GetSinkConnectedSocket (0) << std::endl; 
  
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  std::cout <<  "-------------p= " << p << std::endl; 
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tSetting TCP maxWndSize: " << size << std::endl; 
    p->SetAttribute ("MaxAdvertisedWindowSize", UintegerValue (size));  
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
  
}



void stopTcp (Ptr<TrafficGenerator> app)
{
  std::cout <<  "-------------ENTERING STOP; app= " << app << " sinkSock=" << app->GetSinkConnectedSocket (0) << std::endl; 
  
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  std::cout <<  "-------------p= " << p << std::endl; 
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tFreezing TCP" << std::endl; 
    p->Freeze();
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
  
}
void restoreTcp (Ptr<TrafficGenerator> app)
{
  std::cout <<  "-------------ENTERING restoreTcp; app= " << app << std::endl; 
  
  Ptr<TcpFreeze> p = app->GetSinkConnectedSocket (0)->GetObject <TcpFreeze>();
  if (p!=0)
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tUnFreezing TCP" << std::endl; 
    p->EndFreeze();
  }
  else
  {
    std::cout << Simulator::Now ().GetSeconds () << "\tERROR while getting TcpFreeze Socket (app: " << app << "  tcpFreeze: " << p << ")" << std::endl; 
    std::cout <<  "\t\t\tsocket is probably not a TcpFreeze one!!!!" << std::endl; 
  }
}


void ChangeChannelAtten (Ptr<LteHelper> lteHelper, double loss)
{
  std::cout << "Trying to change propag model" << std::endl;

  Ptr<EvolvedFriisPropagationLossModel> dlPlm = lteHelper->m_downlinkPathlossModel->GetObject<EvolvedFriisPropagationLossModel> ();
  Ptr<EvolvedFriisPropagationLossModel> ulPlm = lteHelper->m_uplinkPathlossModel->GetObject<EvolvedFriisPropagationLossModel> ();
  if (!dlPlm || !ulPlm)
    {
      std::cout << "WARNING: impossible to change channel caracteristics. May not be an EvolvedFriisPropagationLossModel" << std::endl;
      return;
    }
  dlPlm->SetSystemLoss (loss);
  ulPlm->SetSystemLoss (loss);
  //~ Config::Set ("/ChannelList/[*]/$ns3::EvolvedFriisSpectrumPropagationLossModel/attenuationfactor", DoubleValue (5.1));  
}


void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Time samplePeriod , Ptr<OutputStreamWrapper> stream);

/*void RxTrace (std::string context, Ptr<const Packet> p)
{  std::cout << context << " RX p: " << *p << std::endl;}

void TxTrace (std::string context, Ptr<const Packet> p)
{  std::cout << context << " TX p: " << *p << std::endl;}
*/

//~ uint8_t measId;
//~ void RecvMeasurementReportCallback (std::string context,
       //~ uint64_t imsi, uint16_t cellId, uint16_t rnti,
       //~ LteRrcSap::MeasurementReport measReport)
//~ {
  //~ LteRrcSap::MeasResults res = measReport.measResults;
  //~ double rsrp = EutranMeasurementMapping::RsrpRange2Dbm (res.rsrpResult);
  //~ double rsrq = EutranMeasurementMapping::RsrqRange2Db (res.rsrqResult);
  //~ 
 //~ 
  //~ if (res.measId==measId)
  //~ {
    //~ NS_LOG_INFO (Simulator::Now ().GetSeconds () 
              //~ << "\timsi: "<< imsi 
              //~ << "\tcellID: " << cellId 
              //~ << "\thaveMeasResultNeighCells: " << res.haveMeasResultNeighCells
              //~ << "\trnti:" << rnti
              //~ << "\trsrp:" << rsrp << "dBm"
              //~ << "\trsrq:" << rsrq << "dB"
              //~ );
  //~ } //END if (res.measId==measId)
//~ }



static void DlPhyError(void)
{
  std::cout << Simulator::Now ().GetSeconds () << "\t" << " !!!!!!!! ERREUR Rx DL PHY !!!!!!!!! " << std::endl;
}

// Calback to generate legacy Cognitive Manager Database (cID / RSRP RSRQ ) : meas_test.dat
static void ReportCallback (Ptr<OutputStreamWrapper> stream, Ptr<MobilityModel> mrPosition, uint16_t rnti, uint16_t cellId, double rsrp, double rsrq, bool servingCell)
{
  char ts[128];
  sprintf (ts, "%.6f", Simulator::Now ().GetSeconds ());
  
  char posX[128];
  Vector pos = mrPosition->GetPosition ();
  sprintf (posX, "%.6f", pos.x);
  
  char speedX[128];
  Vector speed = mrPosition->GetVelocity ();
  sprintf (speedX, "%.6f", speed.x);

  *stream->GetStream() << ts << "\t" << posX << "\t" << speedX << "\t" << rnti << "\t" << cellId << "\t" << rsrp << "\t" << rsrq << "\t" << servingCell << std::endl;
}


// Calback to generate additionnal Cognitive Manager Database (MCS) : dlSched.dat
// cf. https://groups.google.com/forum/#!searchin/ns-3-users/trace$20MCS/ns-3-users/bdTw72FfQew/Hsx_VQq16sMJ
void PrintDLScheduler (Ptr<OutputStreamWrapper> stream, Ptr<MobilityModel> mrPosition, uint32_t frameNum, uint32_t subFrameNum, uint16_t rnti, uint8_t mcs1, uint16_t sizeTb1, uint8_t mcs2, uint16_t sizeTb2)
{
  char ts[128];
  sprintf (ts, "%.6f", Simulator::Now ().GetSeconds ());

  char posX[128];
  Vector pos = mrPosition->GetPosition ();
  sprintf (posX, "%.6f", pos.x);
  
  char speedX[128];
  Vector speed = mrPosition->GetVelocity ();
  sprintf (speedX, "%.6f", speed.x);
  
  NS_LOG_INFO (Simulator::Now ().GetSeconds () 
              << "\t--->MAC-DL" << "\t" 
              << "xpos = " << posX << "\t" 
              << "xspeed = " << speedX << "\t" 
              << "frameNum: "<< frameNum << "\t" 
              << "subFrameNum: "<< subFrameNum << "\t" 
              << "rnti: " << rnti << "\t" 
              << "mcs1: " << (int)mcs1 << "\t" 
              << "sizeTb1:" << sizeTb1 << "\t" 
              << "mcs2:" << (int)mcs2 << "\t" 
              << "sizeTb2:" << sizeTb2 
              );
  *stream->GetStream() 
          << ts << "\t" 
          << posX << "\t" 
          << speedX << "\t" 
          << frameNum << "\t" 
          << subFrameNum << "\t" 
          << rnti << "\t" 
          << (int)mcs1 << "\t" 
          << sizeTb1 << "\t" 
          << (int)mcs2 << "\t" 
          << sizeTb2 
          << std::endl;              
}




int main (int argc, char *argv[])
{
  // _______________ LOG SYSTEM CONFIGURATION _______________
  //LOG_ERROR, LOG_WARN, LOG_DEBUG, LOG_INFO, LOG_FUNCTION, LOG_LOGIC, LOG_ALL

  LogLevel logLevel = (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LEVEL_ALL);

  //~ LogComponentEnable ("LteHelper", logLevel);
  LogComponentEnable ("EpcHelper", logLevel);
  //LogComponentEnable ("EpcEnbApplication", logLevel);
  //LogComponentEnable ("EpcX2", logLevel);
  //LogComponentEnable ("EpcSgwPgwApplication", logLevel);

  //LogComponentEnable ("LteEnbRrc", logLevel);
  //LogComponentEnable ("LteEnbNetDevice", logLevel);
  //LogComponentEnable ("LteEnbPhy", logLevel);
  //LogComponentEnable ("LteUeRrc", logLevel);
  //LogComponentEnable ("LteUeNetDevice", logLevel);
  //LogComponentEnable ("LteUePhy", logLevel);
  //LogComponentEnable ("LteSpectrumPhy", logLevel);
  
  
  //LogComponentEnable ("InternetStackHelper", logLevel);
  //LogComponentEnable ("Ipv4StaticRoutingHelper", logLevel);
  
  //~ LogComponentEnable ("Config", LOG_LEVEL_DEBUG);

  LogComponentEnable ("TcpSocketBase", (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LOGIC | LOG_INFO) );
  //~ LogComponentEnable ("TcpReno", (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LOGIC | LOG_INFO) );
  LogComponentEnable ("TcpNewReno", (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LOGIC | LOG_INFO) );
  LogComponentEnable ("TcpFreeze", (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LOGIC | LOG_INFO) );

  LogComponentEnable ("corridor", LOG_LEVEL_DEBUG);
  //~ LogComponentEnable ("corridor", LOG_LEVEL_INFO);
  LogComponentEnable ("CognitiveManager", LOG_LEVEL_DEBUG);
  //~ LogComponentEnable ("CognitiveManager", LOG_LEVEL_INFO);

  //~ LogComponentEnable ("TrafficGenerator", (LogLevel)(LOG_PREFIX_FUNC | LOG_PREFIX_TIME | LOG_LEVEL_DEBUG | LOG_LOGIC | LOG_INFO));

  //~ LogComponentEnable ("EvolvedFriisPropagationLossModel", LOG_DEBUG);

 
  NS_LOG_INFO ("Corridor logging system activated");


//~ Ipv4Address tstAdd4 = Ipv4Address ("12.13.14.15");
//~ Ipv6Address tstAdd6 = Ipv6Address ("2001:2::0");
//~ Address add4 = tstAdd4.ConvertTo ();
//~ Address add6 = tstAdd6.ConvertTo ();
//~ 
//~ std::cout << "Add IPv4: " << tstAdd4 << "\ttype: " << add4.GetType () << "\tIpv4Address::GetType: " << Ipv4Address::GetType () << "\n";
//~ std::cout << "Add IPv6: " << tstAdd4 << "\ttype: " << add6.GetType () << "\tIpv6Address::GetType: " << Ipv6Address::GetType () << "\n";


  
  // _______________ DECLARATIONS AND INTIALISATIONS _______________
  int i=0;

  uint16_t numberOfMRs = 1; // no more for the moment
  uint16_t numberOfEnbs = 2;
  uint16_t numberOfMNNs = 3;

  double distance = 1000.0;             // m  // distance between eNB
  double xMobileStartPos = -distance/4; // m
  //~ double xMobileStartPos = 0; // m
  double yForRemoteHost = 0;            // m
  double yForPgw = 100.0;               // m
  double yForeNB = 500.0;               // m
  double yForUe = yForeNB + 20.0;       // m
  double yForMNN = yForUe + 20;         // m
  double distance_between_MNNs = 10;    // m
//  double yForWifiAp = yForeNB-20;     // m
  

  double speed = 100;                   // m/s
//  double simTime = 3.0 * distance / speed;    // 3000 m / 20 m/s = 150 secs
  double simTime = 20;

  double enbTxPowerDbm = 30.0; // dBm 
  double ueTxPowerDbm = 23.0; // dBm - typical default value is 23dBm

  uint16_t fading = 0;
  double  attenuationFactor = 2.0d;
  
  bool tunnel = false;
  bool freeze = false;
  uint32_t maxAdvertisedWindowSize = 0;
  
  bool second_trip_case = 0;
  float antenna_distance = 50; // m

  float futureExtent_seconds = antenna_distance / (float)speed ;
  //int futureExtent_index = floor(futureExtent_seconds / 0.200); // UE internal measurements every 200 ms
  
  char dataBasePath[] = "cogDbBase.db";
  char macDataBasePath[] = "cogDbMac.db";
  Time cognitiveDatabasePeriod = MilliSeconds (100);
  
  
  // Command line arguments
  CommandLine cmd;
  cmd.AddValue("simTime", "Total duration of the simulation (in seconds)", simTime);
  cmd.AddValue("speed", "Speed of the UE (default = 100 m/s)", speed);
  cmd.AddValue("enbTxPowerDbm", "TX power [dBm] used by eNBs (default = 30.0)", enbTxPowerDbm);
  cmd.AddValue("ueTxPowerDbm", "maximum TX power [dBm] used by UE (default = 23.0)", ueTxPowerDbm);
  cmd.AddValue("second_trip_case", "BooleanValue to replay the measurements or not (default = 0)", second_trip_case);
  cmd.AddValue("antenna_distance", "Distance between the antennas on the train (default = 50 m)", antenna_distance);
  cmd.AddValue("fading", "Integer to specify if there is a fading (and which) or not (default = 0)", fading);
  cmd.AddValue("tunnel", "BooleanValue to specify if there is a tunnel or not or not (default = 0)", tunnel);
  cmd.AddValue("attenuation_factor", "Double  to specify the exponent of the distance in attenuation calculation (default = 2.0)", attenuationFactor);
  cmd.AddValue("distance", "Distance between the eNodeBs (default = 1000 m)", distance);
  //~ cmd.AddValue("start_pos", "start position of the train (default = distance/4)", xMobileStartPos);
  cmd.AddValue("start_pos", "start position of the train (default = 0)", xMobileStartPos);
  cmd.AddValue("freeze", "activate FreezeTCP (default=0 => non activated)", freeze);
  cmd.AddValue("MaxAdvertisedWindowSize", "maximum advertised window size for FreezeTCP socket, This value will be multiplied by 4!!! (default=0 => non constraint)", maxAdvertisedWindowSize);
   
//TODO: add a command line value for
//              - database path(es)
//              - variability

  cmd.Parse(argc, argv);  

  //~ ConfigStore config;
  //~ config.ConfigureDefaults ();



  // Set Default parameters

  Config::SetDefault ("ns3::UdpClient::Interval", TimeValue (MilliSeconds(10)));
  Config::SetDefault ("ns3::UdpClient::MaxPackets", UintegerValue (1000000));

//TODO: Move ns3::LteHelper::UseIdealRrc to false to use LteRrcProtocolReal instead of LteUeRrcProtocolReal
  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (true)); // RP : existe aussi UseIdeallRrc !!!!!!!!!!!!!!!!!!

  // Set TCP algorithm (default = ns3::TcpNewReno) , others: TcpTahoe, TcpRfc793, TcpWestwood
  Config::SetDefault ("ns3::TcpL4Protocol::SocketType", StringValue ("ns3::TcpFreeze"));
  //~ Config::SetDefault ("ns3::TcpL4Protocol::SocketType", StringValue ("ns3::TcpWestwood"));

//~ 
// Pour éviter de saturer le buffer de TCP, ce qui a pour conséquence de planter l'émetteur à la reprise d'un Freze
//~ Config::SetDefault ("ns3::TcpSocket::SndBufSize", UintegerValue(4294967295)); // default = 131072
// Pour éviter pertes au L2 (limite le pic lors de reprise du trafic et génère un timeOut qui implique un timeout de 1s après la reprise)
//~ Config::SetDefault ("ns3::DropTailQueue::MaxPackets", UintegerValue(10000000)); // default = 100 // limite à 200kb/s -> 121
//~ Config::SetDefault ("ns3::DropTailQueue::MaxBytes", UintegerValue(4294967295)); // default = 6553500

//~ Config::SetDefault ("ns3::TcpNewReno::LimitedTransmit", BooleanValue(true)); // default = false

    //~ Config::SetDefault ("ns3::TcpFreeze::MaxAdvertisedWindowSize", UintegerValue (maxAdvertisedWindowSize));  


  // _____________________________________________
  // _________________Internet____________________
  // _____________________________________________

  // LTE Helpers
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  //Ptr<EpcHelper> epcHelper = CreateObject<EpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
  lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold",
                                            UintegerValue (30));
  lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset",
                                            UintegerValue (1));

  // Get the P-GW
  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create the Internet: PtP link between Remote Host and PGW
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("1Gb/s"))); // 100Gb/s
  //~ p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("10Mb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);

  // Routing of the Internet Host (towards the LTE network)
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1); // interface 0 is localhost, 1 is the p2p device toward the pgw



  // Create eNB and embedded (MR + MNN) nodes
  NodeContainer mrNodes;
  NodeContainer enbNodes;
  NodeContainer mnnNodes;
  enbNodes.Create (numberOfEnbs);
  mrNodes.Create (numberOfMRs);
  mnnNodes.Create (numberOfMNNs);

  Ptr<Node> mr = mrNodes.Get(0);
  
  
  // _____________________________________________
  // __________________Mobility models____________
  // _____________________________________________
//TODO: check the ns3::SteadyStateRandomWaypointMobilityModel

  // Install Mobility Model in eNBs
  Ptr<ListPositionAllocator> enbPositionAlloc = CreateObject<ListPositionAllocator> ();
  for (i = 0; i < numberOfEnbs; i++)
  {
    Vector enbPosition (distance * (i), yForeNB, 0);
    enbPositionAlloc->Add (enbPosition);
  }
  MobilityHelper enbMobility;
  enbMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  enbMobility.SetPositionAllocator(enbPositionAlloc);
  enbMobility.Install(enbNodes);
  
  // Install Mobility Model in P-GW
  // Attention : ne supporte qu'un seul Wifi AP pour l'instant
  MobilityHelper pgwMobility;
  pgwMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  pgwMobility.Install(pgw);
  pgw->GetObject<MobilityModel> ()->SetPosition (Vector (distance /2, yForPgw, 0));

  // Install Mobility Model in remoteHost
  MobilityHelper remoteHostMobility;
  remoteHostMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  remoteHostMobility.Install(remoteHost);
  remoteHost->GetObject<MobilityModel> ()->SetPosition (Vector (distance /2, yForRemoteHost, 0));
  
  // Install Mobility Model in MR
  // Attention : ne supporte qu'un seul MR pour l'instant
  MobilityHelper mrMobility;
  mrMobility.SetMobilityModel ("ns3::ConstantVelocityMobilityModel");
  mrMobility.Install(mrNodes);
  mrNodes.Get (0)->GetObject<MobilityModel> ()->SetPosition (Vector (xMobileStartPos, yForUe, 0));
  mrNodes.Get (0)->GetObject<ConstantVelocityMobilityModel> ()->SetVelocity (Vector (speed, 0, 0));

  // Install Mobility Model in MNN
  MobilityHelper mnnMobility;
  mnnMobility.SetMobilityModel ("ns3::ConstantVelocityMobilityModel");
  mnnMobility.Install(mnnNodes);
  for (i=0; i<numberOfMNNs ; i++)
  {
    mnnNodes.Get (i)->GetObject<MobilityModel> ()->SetPosition (Vector (xMobileStartPos + i*distance_between_MNNs, yForMNN, 0));
    mnnNodes.Get (i)->GetObject<ConstantVelocityMobilityModel> ()->SetVelocity (Vector (speed, 0, 0));
  }  
  
  
  // _____________________________________________
  // ___________________WIFI______________________
  // _____________________________________________
  // Wifi channel
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ());

  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

  // STA mac Wifi
  NqosWifiMacHelper mac = NqosWifiMacHelper::Default ();
  Ssid ssid = Ssid ("Mobile-Router-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));
  
  // installation of the wifi stack on the MNN
  NetDeviceContainer mnnWifiDevices;
  mnnWifiDevices = wifi.Install (phy, mac, mnnNodes);

  // AP mac Wifi
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));
  
  // installation of the MR
  NetDeviceContainer mrWifiDevices;
  mrWifiDevices = wifi.Install (phy, mac, mrNodes); 
  
  // Create the trainWifiDevices NetDeviceContainer
  NetDeviceContainer trainWifiDevices;
  trainWifiDevices.Add(mrWifiDevices);
  trainWifiDevices.Add(mnnWifiDevices);
  
  // Install the IP stack on the MNNs
  internet.Install (mnnNodes);

  // Install the IP stack on the MR
  internet.Install (mrNodes);

  // Assign IP addresses to the train internal network
  Ipv4InterfaceContainer trainIpIfaces;
  ipv4h.SetBase ("10.1.1.0", "255.255.255.0");  
  trainIpIfaces = ipv4h.Assign(trainWifiDevices);
  

  
  // Set the default gateway for the MNNs
  for (i=0; i<numberOfMNNs ; i++)
  {
    Ptr<Node> mnn = mnnNodes.Get(i);
    Ptr<Ipv4StaticRouting> mnnStaticRouting = ipv4RoutingHelper.GetStaticRouting (mnn->GetObject<Ipv4> ());
    mnnStaticRouting->SetDefaultRoute (trainIpIfaces.GetAddress(0), 1); // interface 0 is the MR itself, interface 1 is the Wifi device of the MR  
  }



  // _____________________________________________
  // ___________LTE PATH LOSS MODEL_______________
  // _____________________________________________
  // default value: ns3::FriisPropagationLossModel
  //        others:  ns3::Cost231PropagationLossModel, ns3::BuildingsPropagationLossModel, ns3::FixedRssLossModel ...
  //        doc at: file:///home/ccouturi/boulot/telecom.Bretagne/recherche/corridor/dev/ns-3/corridor/trunk/ns-3.21/doc/html/classns3_1_1_fixed_rss_loss_model.html
  //~ lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisPropagationLossModel")); // other models: ns3::Cost231PropagationLossModel; ns3::FixedRssLossModel
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::EvolvedFriisPropagationLossModel")); 
  Config::SetDefault ("ns3::EvolvedFriisPropagationLossModel::attenuationfactor", DoubleValue (attenuationFactor));

  // plan signal drop at a given time to simulate a tunnel
  if (tunnel)
    {
      Simulator::Schedule ( Seconds(7.9), &ChangeChannelAtten, lteHelper, 10000000000); // 1000 -> 30dB loss 
      Simulator::Schedule ( Seconds(8.1), &ChangeChannelAtten, lteHelper, 1);
    }


  
  // _____________________________________________
  // ______________LTE FADING MODEL_______________
  // _____________________________________________
  if (fading)
  {
    // Set Fading Model with a file
    // The file has been generated with the fading-trace-generator.m
    lteHelper->SetAttribute ("FadingModel", StringValue ("ns3::TraceFadingLossModel"));

    if (fading<10 && fading<20)
      {
      switch (fading)
        {
        case 1:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/fading_trace_EPA_3kmph.fad"));
           std::cout << "Applying fading model: " << "fading_trace_EPA_3kmph" << std::endl;
          break;
        case 2:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/fading_trace_ETU_3kmph.fad"));
          std::cout << "Applying fading model: " << "fading_trace_ETU_3kmph" << std::endl;          
          break;
        case 3:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/fading_trace_EVA_60kmph.fad"));
          std::cout << "Applying fading model: " << "fading_trace_EVA_60kmph" << std::endl; 
          break;

        case 5:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/fading_trace_EPA_300kmph-2160MHz.fad"));
          std::cout << "Applying fading model: " << "EPA_300kmph-2160MHz" << std::endl;
          break;
        case 6:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/fading_trace_ETU_300kmph-2160MHz.fad"));
          std::cout << "Applying fading model: " << "ETU_300kmph-2160MHz" << std::endl;          
          break;
        case 7:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/fading_trace_EVA_300kmph-2160MHz.fad"));
          std::cout << "Applying fading model: " << "EVA_300kmph-2160MHz" << std::endl; 
          break;
        }
        
      lteHelper->SetFadingModelAttribute ("TraceLength", TimeValue (Seconds (10.0))); // default = 10 seconds length trace
      lteHelper->SetFadingModelAttribute ("SamplesNum", UintegerValue (10000));       // default = 10,000 samples
      lteHelper->SetFadingModelAttribute ("WindowSize", TimeValue (Seconds (0.5)));    // default = 0.5 seconds for window size
      lteHelper->SetFadingModelAttribute ("RbNum", UintegerValue (100));          // default = 100 RB
      }// END if (fading<10)

    if (fading>=10)
    {
      switch (fading)
      {
        // 2.6GHz
        case 11:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/run_1-carrier_1-3x2.fad"));
          std::cout << "Applying fading model: " << "run_1-carrier_1-3x2" << std::endl;
          lteHelper->SetFadingModelAttribute ("TraceLength", TimeValue (Seconds (465.0))); // default = 10 seconds length trace
          lteHelper->SetFadingModelAttribute ("SamplesNum", UintegerValue (77500));       // default = 10,000 samples
          lteHelper->SetFadingModelAttribute ("WindowSize", TimeValue (Seconds (0.5)));    // default = 0.5 seconds for window size
          lteHelper->SetFadingModelAttribute ("RbNum", UintegerValue (100));          // default = 100 RB
          break;
        case 12:
          //~ lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/run_2-carrier_1-3x2.fad"));
          //~ std::cout << "Applying fading model: " << "run_2-carrier_1-3x2" << std::endl;
          //~ lteHelper->SetFadingModelAttribute ("TraceLength", TimeValue (Seconds (498.0))); // default = 10 seconds length trace
          //~ lteHelper->SetFadingModelAttribute ("SamplesNum", UintegerValue (83000));       // default = 10,000 samples
          //~ lteHelper->SetFadingModelAttribute ("WindowSize", TimeValue (Seconds (100)));    // default = 0.5 seconds for window size
          //~ lteHelper->SetFadingModelAttribute ("RbNum", UintegerValue (100));          // default = 100 RB
          //~ break;

          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/run_2-carrier_1-3x2-new08.fad"));
          std::cout << "Applying fading model: " << "run_2-carrier_1-3x2---NEWW!!!!" << std::endl;
          lteHelper->SetFadingModelAttribute ("TraceLength", TimeValue (Seconds (20.0))); // default = 10 seconds length trace
          lteHelper->SetFadingModelAttribute ("SamplesNum", UintegerValue (20000));       // default = 10,000 samples
          lteHelper->SetFadingModelAttribute ("WindowSize", TimeValue (Seconds (20.0)));    // default = 0.5 seconds for window size
          lteHelper->SetFadingModelAttribute ("RbNum", UintegerValue (94));          // default = 100 RB
          //~ lteHelper->SetFadingModelAttribute ("RbNum", UintegerValue (90));          // default = 100 RB
          break;

        // 800MHz
        case 101:
          lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue ("src/lte/model/fading-traces/800MHz_1_1-1x1.fad"));
          std::cout << "Applying fading model: " << "fading-traces/800MHz_1_1-1x1" << std::endl;
          lteHelper->SetFadingModelAttribute ("TraceLength", TimeValue (Seconds (66.0))); // default = 10 seconds length trace
          lteHelper->SetFadingModelAttribute ("SamplesNum", UintegerValue (66000));       // default = 10,000 samples
          lteHelper->SetFadingModelAttribute ("WindowSize", TimeValue (Seconds (0.5)));    // default = 0.5 seconds for window size
          lteHelper->SetFadingModelAttribute ("RbNum", UintegerValue (100));          // default = 100 RB
          break;
 
        }
      }//END if (fading>=10 && fading<20)
    
  }// END if (fading)
  
  
  // _____________________________________________
  // ___________________LTE_______________________
  // _____________________________________________
   
  // Install LTE Devices in eNBs
  NetDeviceContainer enbLteDevs;
  
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (enbTxPowerDbm)); // default value = 30dBm
  //~ Config::SetDefault ("ns3::LteEnbPhy::NoiseFigure", DoubleValue (5.0)); // default value = 5dB
  //~ Config::SetDefault ("ns3::LteUePhy::NoiseFigure", DoubleValue (9.0));  // default value = 9dB
//~ Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (false)); // default value = true
//~ Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue (23.0)); // default value = 10dBm in therory but actually behave like if 23dBm (as default Pcmax parameter value of LteUePowerControl)

Config::SetDefault ("ns3::LteUePowerControl::Pcmax", DoubleValue (ueTxPowerDbm)); // default value = 23dBm 


  
  //~ Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (25)); // BW in RB: default value = 25
  //~ Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (25)); // BW in RB: default value = 25
  Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (6)); // BW in RB: default value = 25
  Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (6)); // BW in RB: default value = 25

    // Frequency Management (for BW spec look at Tables 7.1.7.1-1 and 7.1.7.2.1-1 of 3GPP TS 36.213)
    // eNB 1
  //~ lteHelper->SetEnbDeviceAttribute("DlEarfcn", UintegerValue (100) );    // default DL Earfcn = 100
  //~ lteHelper->SetEnbDeviceAttribute("UlEarfcn", UintegerValue (18100) );  // default UL Earfcn = 18100
  //~ lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (6));  // 6 RB -> 1.4MHz
  //~ lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (25)); // 25RB -> 5MHz
  enbLteDevs.Add( lteHelper->InstallEnbDevice (enbNodes.Get (0) ) );
    // eNB 2
  //~ lteHelper->SetEnbDeviceAttribute("DlEarfcn", UintegerValue (2750) );   // BAND 7: 2.62GHZ
  //~ lteHelper->SetEnbDeviceAttribute("UlEarfcn", UintegerValue (21449) );  // BAND 7: 2.57GHZ
  //~ lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (6) );
  //~ lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (6) );
  enbLteDevs.Add( lteHelper->InstallEnbDevice (enbNodes.Get (1) ) );

    // traces
  uint8_t ulBw, dlBw;  
  double dlFrq, ulFrq;
  Ptr<LteEnbNetDevice> enbdev1 = enbLteDevs.Get(0)->GetObject<LteEnbNetDevice> ();
  Ptr<LteEnbNetDevice> enbdev2 = enbLteDevs.Get(1)->GetObject<LteEnbNetDevice> ();
  dlFrq = LteSpectrumValueHelper::GetCarrierFrequency (enbdev1->GetDlEarfcn ());
  ulFrq = LteSpectrumValueHelper::GetCarrierFrequency (enbdev1->GetUlEarfcn ());
  ulBw = enbdev1->GetUlBandwidth ();
  dlBw = enbdev1->GetDlBandwidth ();
  NS_LOG_DEBUG ("FREQUENCE ENB1: "<< std::endl
             << "     DL frq: " << dlFrq << " (Earfcn=" << enbdev1->GetDlEarfcn () << ") " << "BW :" << (int)dlBw << std::endl
             << "     UL frq: " << ulFrq << " (Earfcn=" << enbdev1->GetUlEarfcn () << ") " << "BW :" << (int)ulBw );
  dlFrq = LteSpectrumValueHelper::GetCarrierFrequency (enbdev2->GetDlEarfcn ());
  ulFrq = LteSpectrumValueHelper::GetCarrierFrequency (enbdev2->GetUlEarfcn ());
  ulBw = enbdev2->GetUlBandwidth ();
  dlBw = enbdev2->GetDlBandwidth ();
  NS_LOG_DEBUG ("FREQUENCE ENB2:" << std::endl
             << "     DL frq: " << dlFrq << " (Earfcn=" << enbdev2->GetDlEarfcn () << ") " << "BW :" << (int)dlBw << std::endl
             << "     UL frq: " << ulFrq << " (Earfcn=" << enbdev2->GetUlEarfcn () << ") " << "BW :" << (int)ulBw );

  
  // Install LTE Devices in the MR
  //By default the eNB and UE use IsotropicAntennaModel with 0dBi gain
  //lteHelper->SetUeAntennaModelType ("ns3::CosineAntennaModel");
  //lteHelper->SetUeAntennaModelAttribute ("Orientation", DoubleValue (0));
  //lteHelper->SetUeAntennaModelAttribute ("Beamwidth",   DoubleValue (100));
  //lteHelper->SetUeAntennaModelAttribute ("MaxGain",     DoubleValue (13.5));
  NetDeviceContainer mrLteDevs = lteHelper->InstallUeDevice (mrNodes);
  // Assign IP address to the MR's LTE interface
  Ipv4InterfaceContainer mrIpIfaces;
  mrIpIfaces = epcHelper->AssignUeIpv4Address (NetDeviceContainer (mrLteDevs));
  // Attach the MR to the first eNodeB
  lteHelper->Attach (mrLteDevs.Get(0), enbLteDevs.Get(0));
  // Set the default gateway for the MR
  Ptr<Ipv4StaticRouting> mrStaticRouting = ipv4RoutingHelper.GetStaticRouting (mr->GetObject<Ipv4> ());
  mrStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 2); // 2 is the index of the LTE interface of the MR
  

  // Add X2 inteface
  lteHelper->AddX2Interface (enbNodes);


 


  // _____________________________________________
  // ______________TRAFFIC GENRATORS______________
  // _____________________________________________
//  //~ bool bulk = true;
//  bool bulk = false;

  bool dlTcp = true;
  //~ bool dlTcp = false;
  //~ bool ulTcp = true;
  bool ulTcp = false;
  
  //~ bool dlUdp = true;
  bool dlUdp = false;
  //~ bool ulUdp = true;
  bool ulUdp = false;
  
  uint16_t tcpDlPort = 30000;
  uint16_t tcpUlPort = 40000;
  //~ uint32_t tcpPqtSize = 512;
  uint32_t tcpPqtSize = 1024;
  Time startTimeTcp = Seconds (0.02);

  uint16_t udpDlPort = 10000;
  uint16_t udpUlPort = 20000;
  uint32_t udpPqtSize = 1024;        // default 1024 / vary between 12 and 1500
  //~ DataRate udpDataRate = DataRate("800kbps");
  //~ DataRate udpDataRate = DataRate("20Mbps"); // reference scenario was made with datarate of 20Mb/s
  DataRate udpDataRate = DataRate("6Mbps");
  
  Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
  startTimeSeconds->SetAttribute ("Min", DoubleValue (0));
  startTimeSeconds->SetAttribute ("Max", DoubleValue (0.010));
  Time startTimeUdp = Seconds (startTimeSeconds->GetValue ());


  //~ if (bulk)
  //~ {
    //~ uint32_t maxBytes = 100000000;
    //~ BulkSendHelper source ("ns3::TcpSocketFactory",
                           //~ InetSocketAddress (mrIpIfaces.GetAddress (0), tcpDlPort));
    //~ // Set the amount of data to send in bytes.  Zero is unlimited.
    //~ source.SetAttribute ("MaxBytes", UintegerValue (maxBytes));
    //~ ApplicationContainer sourceApps = source.Install (remoteHost);
    //~ sourceApps.Start (Seconds (1.0));
    //~ sourceApps.Stop (Seconds (20.0));
//~ 
    //~ PacketSinkHelper sink ("ns3::TcpSocketFactory",
                           //~ InetSocketAddress (Ipv4Address::GetAny (), tcpDlPort));
    //~ ApplicationContainer sinkApps = sink.Install (mr);
    //~ sinkApps.Start (Seconds (0.0));
    //~ sinkApps.Stop (Seconds (20.0));
  //~ }
  
  // TCP traffic from Remote Host to Mobile Router (tcpDlPort)
  TrafficGenerator tcpDlApp;
  if (dlTcp)
    {
      //~ tcpDlApp.Setup ("ns3::TcpSocketFactory", remoteHost, mr, mrIpIfaces.GetAddress (0), tcpDlPort,
                       //~ tcpPqtSize, DataRate("2Mbps"));  //1100kbps
//~ // tcpDlApp.Setup ("ns3::TcpSocketFactory", remoteHost, pgw, Ipv4Address ("1.0.0.1"), tcpDlPort,
                             //~ // tcpPqtSize, DataRate("100kbps")); //2Mbps
      tcpDlApp.SetupBulk ("ns3::TcpSocketFactory", remoteHost, mr, mrIpIfaces.GetAddress (0), tcpDlPort,
                       tcpPqtSize, 4000000); 
      tcpDlApp.SetStartTime (startTimeTcp);   
      tcpDlApp.RecordTraffic ( "tcpDl.txt", MilliSeconds(50) );
     
      // Command FreezeTCP and MaxWindowSize at given times
      if (maxAdvertisedWindowSize != 0)
        {
          Simulator::Schedule ( Seconds(7.8), &setMaxTcpWndSize, &tcpDlApp, maxAdvertisedWindowSize);
          Simulator::Schedule ( Seconds(8.2), &setMaxTcpWndSize, &tcpDlApp, 0);
        }
      if (freeze)
        {
          Simulator::Schedule ( Seconds(7.8), &stopTcp, &tcpDlApp);
          Simulator::Schedule ( Seconds(8.1), &restoreTcp, &tcpDlApp);
        }
    }
    

  // TCP traffic from Mobile Router to Remote Host (tcpUlPort)
  TrafficGenerator tcpUlApp;  
  if (ulTcp)
    {
      tcpUlApp.Setup ("ns3::TcpSocketFactory", mr, remoteHost, internetIpIfaces.GetAddress (1), tcpUlPort,
                             tcpPqtSize, DataRate("2Mbps")); 
      tcpUlApp.SetStartTime (startTimeTcp);   
      tcpUlApp.RecordTraffic ( "tcpUl.txt", MilliSeconds(100) );      
    }



  // UDP DL (from RemoteHost to MR)
  TrafficGenerator udpDlApp;
  if (dlUdp)
    {
      udpDlApp.Setup ("ns3::UdpSocketFactory", remoteHost, mr, mrIpIfaces.GetAddress (0), udpDlPort,
                             udpPqtSize, udpDataRate); // Create a client on the Remote Host that will pool MR:dlPort
      udpDlApp.SetStartTime (startTimeUdp);   
      udpDlApp.RecordTraffic ( "udpDl.txt", MilliSeconds(100) );
    }

  // UDP UL (from MR to RemoteHost)
  TrafficGenerator udpUlApp;
  if (ulUdp)
    {
      udpUlApp.Setup ("ns3::UdpSocketFactory", mr, remoteHost, internetIpIfaces.GetAddress (1), udpUlPort,
                             udpPqtSize, udpDataRate); // Create a client on the MR that will pool Node 1:dlPort (Node 1 = Remote Host)
      udpUlApp.SetStartTime (startTimeUdp);
      udpUlApp.RecordTraffic ( "udpUl.txt", MilliSeconds(100) );
    }
 

  
  Ptr<EpcTft> tft = Create<EpcTft> ();
  EpcTft::PacketFilter dlpf;
  //~ dlpf.localPortStart = dlPort;
  //~ dlpf.localPortEnd = dlPort;
  //~ dlpf.localPortStart = tcpDlPort;
  //~ dlpf.localPortEnd = tcpDlPort;
  tft->Add (dlpf); 

  //~ EpcTft::PacketFilter ulpf;
  //~ ulpf.remotePortStart = 40000;
  //~ ulpf.remotePortEnd = 50000;
  //~ tft->Add (ulpf);


//~ EpcTft::PacketFilter dlpfMine;
//~ dlpfMine.localPortStart = 0;
//~ dlpfMine.localPortEnd = 65535;
//~ dlpfMine.remotePortStart = 0;
//~ dlpfMine.remotePortEnd = 65535;
//~ dlpfMine.localAddress = Ipv4Address ("10.1.1.0");
//~ dlpfMine.localMask = Ipv4Mask ("255.255.255.0");
//~ dlpfMine.remoteAddress = Ipv4Address ("1.0.0.0");
//~ dlpfMine.remoteMask = Ipv4Mask("255.255.255.0");
//~ tft->Add (dlpfMine); 

  
  EpsBearer bearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
  //~ EpsBearer bearer (EpsBearer::GBR_GAMING);
  
  
  //GbrQosInformation qos;
  //qos.gbrDl = 1024000; // Downlink GBR
  //qos.gbrUl = 1024000; // Uplink GBR
  //qos.mbrDl = 3052000; // Downlink MBR
  //qos.mbrUl = 3052000; // Uplink MBR
  //EpsBearer bearer (EpsBearer::GBR_GAMING, qos); //GBR_CONV_VIDEO;  GBR_NON_CONV_VIDEO
  ////~ EpsBearer bearer (EpsBearer::GBR_NON_CONV_VIDEO, qos); //GBR_CONV_VIDEO;  GBR_NON_CONV_VIDEO
  
  
  lteHelper->ActivateDedicatedEpsBearer (mrLteDevs.Get(0), bearer, tft);

// TODO: verifier différence entre 
//         lteHelper->ActivateDedicatedEpsBearer
//      et PointToPointEpcHelper::ActivateEpsBearer

  
  
  // _____________________________________________
  // _____________COGNITIVE MANAGER_______________
  // _____________________________________________
    // Receiver socket on MR
  TypeId tidTcp = TypeId::LookupByName ("ns3::TcpSocketFactory");
  Ptr<Socket> destination = Socket::CreateSocket(mr, tidTcp);
  InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny(), tcpDlPort);
    // Application declaration and installation on MR
  Ptr<CognitiveManager> appdest = CreateObject<CognitiveManager> ();
  appdest->Setup (mr->GetObject<MobilityModel> (), destination, local, second_trip_case, futureExtent_seconds, dataBasePath, lteHelper, cognitiveDatabasePeriod, macDataBasePath);
  mr->AddApplication (appdest);
  appdest->SetStartTime (Seconds (0.01));

  
  // _____________________________________________
  // ________ DATABASE GENERATION FOR CM _________
  // _____________________________________________
  if (second_trip_case==0)
  {
    //~ // cID / RSRP / RSRQ Traces
    Ptr<LtePhy> uePhy = mrLteDevs.Get (0)->GetObject<LteUeNetDevice> ()->GetPhy ()->GetObject<LtePhy> ();
    AsciiTraceHelper asciiTraceHelper2;
    
    Ptr<OutputStreamWrapper> stream2 = asciiTraceHelper2.CreateFileStream (dataBasePath);
    uePhy->SetAttribute ("UeMeasurementsFilterPeriod", TimeValue (cognitiveDatabasePeriod)); //default: 200ms
    uePhy->TraceConnectWithoutContext ("ReportUeMeasurements", MakeBoundCallback(&ReportCallback, stream2, mr->GetObject<MobilityModel> ()));
    
    // MCS traces
    AsciiTraceHelper asciiTraceHelperDlSched;
    Ptr<OutputStreamWrapper> streamDlSched = asciiTraceHelperDlSched.CreateFileStream (macDataBasePath);
    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::LteEnbNetDevice/LteEnbMac/DlScheduling", MakeBoundCallback(&PrintDLScheduler, streamDlSched, mr->GetObject<MobilityModel> ()) );

  } // END if (second_trip_case)
  
  
  // _____________________________________________
  // __________________ TRACES ___________________
  // _____________________________________________
  
  // ___________________ CWND TRACES ___________________
  if (dlTcp)
    {
      tcpDlApp.TraceCwnd ("cwnd_test.dat");
      tcpDlApp.TraceSSThreshold ("ssTresh.dat");
    }

  // _____________ RTT & RTO trace on TCP traffic _____________
  if (dlTcp)
    {
      tcpDlApp.TraceRtt ("RTT.dat");
      tcpDlApp.TraceRto ("RTO.dat");
      tcpDlApp.TraceRxMsg ( "tcpDl-RxNb.txt" );     
      
tcpDlApp.TraceRwnd ("RWND.txt");
tcpDlApp.TraceActualWnd ("ActualWnd.txt");
tcpDlApp.TraceInFlight ("inFlight.txt");
tcpDlApp.TraceTxSequence ("TxSeq.txt"); 
tcpDlApp.TraceHighestTxSequence ("TxSeqHigh.txt"); 

//~ tcpDlApp.TraceRxSequence ("RxSeq.txt"); 
//~ Simulator::Schedule ( Seconds(1.0), &startRxSeqRecording, &tcpDlApp);

    }  
    
//~ Config::Connect ("/NodeList/[i]/DeviceList/[i]/$ns3::PointToPointNetDevice/TxQueue", MakeCallback(&PacketDropCB));
//~ Config::Connect ("/NodeList/[i]/DeviceList/[i]/$ns3::PointToPointNetDevice/MacTxDrop", MakeCallback(&PacketDropCB));
    
    
  // _____________ PHY errors trace callback _____________
  Config::Connect ("/NodeList/4/DeviceList/0/Phy/DlPhyReception", MakeCallback(&DlPhyError));

  // ________________ Handover callback ________________
  //Config::Connect("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart", MakeCallback (&NotifyHandoverStartUe));
  
  
  // ________________ PCAP tracing ________________
  p2ph.EnablePcapAll("Node"); //Node0=Pgw;  Node1=Remote host;  Node2 et Node3=enbs;  Node 4 = MR
  phy.EnablePcap ("wifi", mrWifiDevices.Get (0));//generate Pcap file of the wifi interface of the MR/ node 4 int 0

  // ________________ ASCII tracing ________________
  //~ AsciiTraceHelper myAsciiTraceHelperIpv4;
  //~ internet.EnableAsciiIpv4All (myAsciiTraceHelperIpv4.CreateFileStream ("testIpv4.tr"));


  // ________________ LTE automated traces ________________
  lteHelper->EnableDlRxPhyTraces ();
  lteHelper->EnableUlRxPhyTraces ();
  lteHelper->EnableMacTraces ();
  lteHelper->EnableRlcTraces ();
  lteHelper->EnablePdcpTraces ();
  lteHelper->EnablePhyTraces ();
  //~ Ptr<RadioBearerStatsCalculator> rlcStats = lteHelper->GetRlcStats ();
  //~ rlcStats->SetAttribute ("EpochDuration", TimeValue (Seconds (1.0)));  // period for the generation of UlRlcStats.txt and DlRlcStats.txt
  //~ Ptr<RadioBearerStatsCalculator> pdcpStats = lteHelper->GetPdcpStats ();
  //~ pdcpStats->SetAttribute ("EpochDuration", TimeValue (Seconds (1.0))); // period for the generation of UlPdcpStats.txt and DlPdcpStats.txt

 

  //~ // ______ PHY parameters tracing_______
  //~ //   cf. http://lena.cttc.es/manual/lte-design.html#measurement-reporting-triggering
  //~ //   and http://www.nsnam.org/docs/models/html/lte-user.html#configure-ue-measurements
  //~ LteRrcSap::ReportConfigEutra config;
  //~ config.eventId = LteRrcSap::ReportConfigEutra::EVENT_A1;
  //~ config.threshold1.choice = LteRrcSap::ThresholdEutra::THRESHOLD_RSRP;
  //~ //config.threshold1.range = 41;
  //~ config.threshold1.range = 0; // changed to 0 to get a periodic report
  //~ config.triggerQuantity = LteRrcSap::ReportConfigEutra::RSRP;
  //~ config.reportInterval = LteRrcSap::ReportConfigEutra::MS120; // report every 120ms (default MS480)
//~ 
  //~ for (int enbNum = 0; enbNum < numberOfEnbs; enbNum++)
  //~ {
    //~ Ptr< Node > node = enbNodes.Get (enbNum);
    //~ Ptr< NetDevice > nDev = node->GetDevice (0);
    //~ Ptr<LteEnbNetDevice> enbDev = nDev->GetObject<LteEnbNetDevice> ();
    //~ Ptr<LteEnbRrc> enbRrc = enbDev->GetRrc ();
    //~ 
    //~ measId = enbRrc->AddUeMeasReportConfig (config);
//~ 
    //~ //enbRrc->TraceConnect ("RecvMeasurementReport", "context", MakeCallback (&RecvMeasurementReportCallback));
  //~ }                  
  //~ Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/RecvMeasurementReport",
               //~ MakeCallback (&RecvMeasurementReportCallback));                    



  // _____________________________________________
  // _____________Gnuplot parameters______________
  // _____________________________________________
  //~ std::string fileNameWithNoExtension = "FlowVSThroughput_";
  //~ std::string graphicsFileName        = fileNameWithNoExtension + ".png";
  //~ std::string plotFileName            = fileNameWithNoExtension + ".plt";
  //~ std::string plotTitle               = "Flow vs Throughput";
  //~ std::string dataTitle               = "Throughput";
//~ 
  //~ // Instantiate the plot and set its title.
  //~ Gnuplot gnuplot (graphicsFileName);
  //~ gnuplot.SetTitle (plotTitle);
//~ 
  //~ // Make the graphics file, which the plot file will be when it
  //~ // is used with Gnuplot, be a PNG file.
  //~ gnuplot.SetTerminal ("png");
//~ 
  //~ // Set the labels for each axis.
  //~ gnuplot.SetLegend ("Flow", "Throughput");
//~ 
  //~ Gnuplot2dDataset dataset;
  //~ dataset.SetTitle (dataTitle);
  //~ dataset.SetStyle (Gnuplot2dDataset::LINES_POINTS);






  // _____________________________________________
  // _______________ FLOWMONITOR__________________
  // _____________________________________________
    // Declare & Initialise FlowMonitor
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor;
  //monitor = flowmon.InstallAll ();
  monitor = flowmon.Install (remoteHost);
  monitor = flowmon.Install (mrNodes);
  monitor = flowmon.Install (enbNodes);
  monitor = flowmon.Install (mnnNodes);


  AsciiTraceHelper asciiTraceHelper;
  Ptr<OutputStreamWrapper> thoughputStream = asciiTraceHelper.CreateFileStream("throughput.dat");

    // Call the flow monitor function
  ThroughputMonitor (&flowmon, monitor, Seconds(0.1), thoughputStream); 



  //~ config.ConfigureAttributes ();

  // _____________________________________________
  // ___________ START/STOP SIMULATION ___________
  // _____________________________________________
  
    // Set stop time
  Simulator::Stop (Seconds (simTime));
  
    //generate XML for NetAnim output
  //~ AnimationInterface anim ("anim.xml"); 
  //~ anim.EnablePacketMetadata (true); // Optional
  //~ anim.EnableIpv4RouteTracking ("anim-routingtable.xml", Seconds (0), Seconds (simTime), Seconds (0.25)); //Optional  

    // Start simulation
  Simulator::Run ();

  // GtkConfigStore config;
  // config.ConfigureAttributes();


  // _____________________________________________
  // _________dump Flowmonitor stats _____________
  // _____________________________________________
/*
  monitor->CheckForLostPackets ();

  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
  {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
    std::cout << "Flow " << i->first  << " (" << t.sourceAddress << ":" << t.sourcePort << " -> " << t.destinationAddress << ":" << t.destinationPort << ") protocol: " << (int)t.protocol << "\n";
    std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
    std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
    std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds() - i->second.timeFirstTxPacket.GetSeconds())/1024/1024  << " Mbps\n";
   }
*/
  //~ monitor->SerializeToXmlFile("flowmon.xml", true, true);



  // _____________________________________________
  // ____________Gnuplot ...continued_____________
  // _____________________________________________
  //~ gnuplot.AddDataset (dataset);
  //~ // Open the plot file.
  //~ std::ofstream plotFile (plotFileName.c_str());
  //~ // Write the plot file.
  //~ gnuplot.GenerateOutput (plotFile);
  //~ // Close the plot file.
  //~ plotFile.close ();


  Simulator::Destroy();
  return 0;
}


struct througputData
{
  unsigned int flowId;
  double lastTimeStamp;
  uint32_t lastRxBytes;
};

std::string Protocol2String (unsigned int protocol)
{
  std::string ret;
  switch (protocol)
  {
    case 6:
      ret = "TCP";
      break;
    case 17:
      ret = "UDP";
      break;
    default:
      char buffer[10];
      sprintf (buffer, "%u", protocol);
      ret = buffer;        
      break;
  }
  return ret;
}


void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon, Time samplePeriod, Ptr<OutputStreamWrapper> stream)
{
  static int nbPass = 0;
  static std::list<struct througputData> througputList;

  std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
  Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier());


  if (nbPass==1) // Print header in file
  {
    *stream->GetStream() << "time\t";
    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
    {
      Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
      *stream->GetStream() << fiveTuple.sourceAddress << ":" << fiveTuple.sourcePort << " -> " << fiveTuple.destinationAddress << ":" << fiveTuple.destinationPort << " (" << Protocol2String (fiveTuple.protocol) << ")" << "\t";
    }  
    *stream->GetStream() << std::endl;
  }

  if (nbPass!=0)
  {
    *stream->GetStream() << (double)Simulator::Now().GetSeconds();      
  }
  
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
  {
    bool found = false;
    double currentThroughput;
    std::list<struct througputData>::iterator it; 
    
    //~ std::cout << "...................\nTraitement du Flow #" << stats->first << "\n";
    it = througputList.begin();
    while (it != througputList.end()) // Parse traffic list to find corresponding one
    {
      //~ std::cout << "Comparaison avec #" << it->flowId << "\n";
      if (it->flowId == stats->first)
      {
        currentThroughput = ( (stats->second.rxBytes - it->lastRxBytes) * 8.0d / ( Simulator::Now ().GetSeconds () - it->lastTimeStamp ) /1000/1000 );
        *stream->GetStream() << "\t" << currentThroughput;    // write to file

        it->lastTimeStamp = Simulator::Now ().GetSeconds ();
        it->lastRxBytes  = stats->second.rxBytes;
        found = true;
        break;
      }
      it++;
    } 
    
    if (found != true) // if traffic was not found then add it to the list
    {
      struct througputData newItem;
      newItem.flowId = stats->first;
      newItem.lastTimeStamp = Simulator::Now ().GetSeconds ();
      newItem.lastRxBytes  = stats->second.rxBytes;
      througputList.push_back (newItem);
    }

  } // END for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)

  if (nbPass!=0) // Add end line to file
  {
    *stream->GetStream() << std::endl;  
  }
    
  Simulator::Schedule (samplePeriod, &ThroughputMonitor, fmhelper, flowMon, samplePeriod, stream);

  //~if(flowToXml)
  //~{
  //~  flowMon->SerializeToXmlFile ("ThroughputMonitor.xml", true, true);
  //~}

  nbPass++;
}




// ******************************************************************
// *                            POUBELLE
// ******************************************************************
/************************* RADIO ENVIRONMENT MAP **********************

  Ptr<RadioEnvironmentMapHelper> remHelper = CreateObject<RadioEnvironmentMapHelper> ();
  remHelper->SetAttribute ("ChannelPath", StringValue ("/ChannelList/1")); 
  remHelper->SetAttribute ("OutputFile", StringValue ("rem.out"));
  remHelper->SetAttribute ("XMin", DoubleValue (-300.0));
  remHelper->SetAttribute ("XMax", DoubleValue (11500.0));
  remHelper->SetAttribute ("XRes", UintegerValue (200));
  remHelper->SetAttribute ("YMin", DoubleValue (-300.0));
  remHelper->SetAttribute ("YMax", DoubleValue (300.0));
  remHelper->SetAttribute ("YRes", UintegerValue (200));
  remHelper->SetAttribute ("Z", DoubleValue (0.0));
  remHelper->Install ();
*/



/************************  Nodes IP address list **********************

//  Ipv4Address loopback("127.0.0.1");
  for (NodeList::Iterator iter = NodeList::Begin();
       iter != NodeList::End();
       iter++) {
    Ptr<Node> node = (*iter);

    Ptr<Ipv4> ipv4 = node->GetObject<Ipv4>();
    NS_ASSERT(ipv4 != 0);
    
    for (uint32_t index = 0; index < ipv4->GetNInterfaces(); index++) {
      uint32_t    addressIndex=0;
      Ipv4InterfaceAddress ipv4Address = ipv4->GetAddress(index , addressIndex);

//      if (ipv4Address != loopback) {
        std::cout << " node " << node->GetId() <<
          " ip " << ipv4Address << std::endl;
//      }
      // end iterating over interfaces
    }

    // end iterate over nodes
  }
*/

/*******************  Manual Hand Over request  **********************
  lteHelper->HandoverRequest (Seconds (0.100), mrLteDevs.Get (0), enbLteDevs.Get (0), enbLteDevs.Get (1));
*/


/*****************  Try to list objects in path  ********************
void PlotAllObjectsPath (std::string basePath)
{
  Config::MatchContainer mc = Config::LookupMatches (basePath + "*");
    
std::cout << "**************base: " << basePath << "\tNbMatches = " << mc.GetN () << std::endl; 

  int j=0;
  if (mc.GetN () != 0 )
  {
    for (Config::MatchContainer::Iterator mci = mc.Begin (); mci != mc.End (); ++mci, ++j)
    {
//      std::cout << "      " << j << ": " << mc.GetMatchedPath (j) << std::endl; 
      PlotAllObjectsPath (mc.GetMatchedPath (j));
    }
  } else {
    std::cout << basePath << std::endl; 
  }
}
*/


/***********************************************************************
void NotifyHandoverStartEnb (
      std::string context, uint64_t imsi, uint16_t cellid, uint16_t rnti, uint16_t targetCellId)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": start handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << " to CellId " << targetCellId
            << std::endl;
}
*/
